package Ablaze.renderer;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import Ablaze.model.ModelWritingDesk;
import Ablaze.tileentity.TileEntityWritingDesk;

public class ItemRendererWritingDesk implements IItemRenderer {

	private ModelWritingDesk model;
	private TileEntitySpecialRenderer render;
	
	public ItemRendererWritingDesk(TileEntitySpecialRenderer renderer){
		this.model = new ModelWritingDesk();
		this.render = renderer;
	}
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		this.render.renderTileEntityAt(new TileEntityWritingDesk(), 0.0D, 0.0D, 0.0D, 0.0F);
	}

}
