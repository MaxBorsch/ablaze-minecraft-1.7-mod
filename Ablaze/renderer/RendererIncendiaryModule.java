package Ablaze.renderer;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import Ablaze.Init;
import Ablaze.model.ModelIncendiaryModule;

public class RendererIncendiaryModule extends TileEntitySpecialRenderer {
	
	private ModelIncendiaryModule model;
	private static final ResourceLocation texture = new ResourceLocation(Init.modid, "textures/models/block/GlowGenerator.png");
	
	public RendererIncendiaryModule(){
		this.model = new ModelIncendiaryModule();
	}
	
	@Override
	public void renderTileEntityAt(TileEntity tileentity, double x, double y,double z, float f) {
	GL11.glPushMatrix();
	GL11.glTranslatef((float) x + 0.55F, (float)y - .8F, (float)z + 0.55F);
	this.bindTexture(texture);
	GL11.glPushMatrix();
	model.renderModel(0.0625F);
	GL11.glPopMatrix();
	GL11.glPopMatrix();
	}

}
