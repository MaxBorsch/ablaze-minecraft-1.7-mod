package Ablaze.renderer;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import Ablaze.model.ModelWeldingFrame;
import Ablaze.tileentity.TileEntityWeldingFrame;

public class ItemRendererWeldingFrame implements IItemRenderer {

	private ModelWeldingFrame model;
	private TileEntitySpecialRenderer render;
	
	public ItemRendererWeldingFrame(TileEntitySpecialRenderer renderer){
		this.model = new ModelWeldingFrame();
		this.render = renderer;
	}
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		this.render.renderTileEntityAt(new TileEntityWeldingFrame(), 0.0D, 0.0D, 0.0D, 0.0F);
	}

}
