package Ablaze.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import Ablaze.Init;
import Ablaze.container.ContainerWritingDesk;
import Ablaze.tileentity.TileEntityWritingDesk;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class GuiWritingDesk extends GuiContainer {

	public TileEntityWritingDesk WritingDesk;
	public static final ResourceLocation Bkg = new ResourceLocation(Init.modid, "textures/gui/writingdesk.png");
	
	public GuiWritingDesk(InventoryPlayer inventory, TileEntityWritingDesk entity) {
		super(new ContainerWritingDesk(inventory, entity));
		
		this.WritingDesk = entity;
		this.xSize = 176;
		this.ySize = 166;
	}

	public void drawGuiContainerForegroundLayer(int par1, int par2){
		String name = this.WritingDesk.getInventoryName();
		
		this.fontRendererObj.drawString(name, this.xSize/2 - this.fontRendererObj.getStringWidth(name)/2, 6, 4210752);
	}
	
	public void drawGuiContainerBackgroundLayer(float f, int i, int j) {
		GL11.glColor4f(1F, 1F, 1F, 1F);
		
		Minecraft.getMinecraft().getTextureManager().bindTexture(Bkg);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
	
		int k2 = WritingDesk.getCookProgressScaled(24);
		drawTexturedModalRect(guiLeft+79, guiTop+34, 176, 14, k2, 16);
	
	}

}
