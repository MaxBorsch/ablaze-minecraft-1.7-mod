package Ablaze.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import Ablaze.Init;
import Ablaze.container.ContainerTinkerTable;
import Ablaze.tileentity.TileEntityTinkerTable;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class GuiTinkerTable extends GuiContainer {

	public TileEntityTinkerTable TinkerTable;
	public static final ResourceLocation Bkg = new ResourceLocation(Init.modid, "textures/gui/tinkertable.png");
	
	public GuiTinkerTable(InventoryPlayer inventory, TileEntityTinkerTable entity) {
		super(new ContainerTinkerTable(inventory, entity));
		this.TinkerTable = entity;
		this.xSize = 176;
		this.ySize = 166;
	}

	public void drawGuiContainerForegroundLayer(int par1, int par2){
	}
	
	public void drawGuiContainerBackgroundLayer(float f, int i, int j) {
		GL11.glColor4f(1F, 1F, 1F, 1F);
		
		Minecraft.getMinecraft().getTextureManager().bindTexture(Bkg);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
	
			if (TinkerTable.useMenuLeft == false){
				drawTexturedModalRect(guiLeft+44, guiTop+7, 176, 0, 39, 40);
			}
			if (TinkerTable.useMenuRight == false){
				drawTexturedModalRect(guiLeft+99, guiTop+7, 215, 0, 39, 59);
			}

	}
}
