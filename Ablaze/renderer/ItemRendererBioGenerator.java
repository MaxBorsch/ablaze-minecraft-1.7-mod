package Ablaze.renderer;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import Ablaze.model.ModelBioGenerator;
import Ablaze.tileentity.TileEntityBioGenerator;

public class ItemRendererBioGenerator implements IItemRenderer {

	private ModelBioGenerator model;
	private TileEntitySpecialRenderer render;
	
	public ItemRendererBioGenerator(TileEntitySpecialRenderer renderer){
		this.model = new ModelBioGenerator();
		this.render = renderer;
	}
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		this.render.renderTileEntityAt(new TileEntityBioGenerator(), 0.0D, 0.0D, 0.0D, 0.0F);
	}

}
