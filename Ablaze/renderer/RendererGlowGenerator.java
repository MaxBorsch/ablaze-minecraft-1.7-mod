package Ablaze.renderer;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import Ablaze.Init;
import Ablaze.model.ModelGlowGenerator;
import Ablaze.tileentity.TileEntityGlowGenerator;
import cpw.mods.fml.client.FMLClientHandler;

public class RendererGlowGenerator extends TileEntitySpecialRenderer {

	private ModelGlowGenerator model;
	public static final ResourceLocation TEXTURE_ON = new ResourceLocation(Init.modid, "textures/models/block/GlowGenerator_active.png");
	public static final ResourceLocation TEXTURE_OFF = new ResourceLocation(Init.modid, "textures/models/block/GlowGenerator.png");
	
	public RendererGlowGenerator(){
		this.model = new ModelGlowGenerator();
	}
	
	@Override
	public void renderTileEntityAt(TileEntity tileentity, double x, double y,double z, float f) {
	TileEntityGlowGenerator te = (TileEntityGlowGenerator) tileentity;
		
		if (te.lastCount>0)
		{
			FMLClientHandler.instance().getClient().renderEngine.bindTexture(TEXTURE_ON);
		}
		else
		{
			FMLClientHandler.instance().getClient().renderEngine.bindTexture(TEXTURE_OFF);
		}	
		
	GL11.glPushMatrix();
	GL11.glTranslatef((float) x + 0.5F, (float)y + 1.5F, (float)z + 0.5F);
	GL11.glRotatef(180, 0F, 0F, 1F);
	GL11.glPushMatrix();
	model.renderModel(0.0625F);
	GL11.glPopMatrix();
	GL11.glPopMatrix();
	}

}
