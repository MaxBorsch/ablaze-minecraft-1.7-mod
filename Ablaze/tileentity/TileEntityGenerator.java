package Ablaze.tileentity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.ForgeDirection;
import Ablaze.item.ItemPowered;
import Ablaze.item.ItemUpgrade;

public class TileEntityGenerator extends TileEntityEnergyHandler implements ISidedInventory {

	public static final int INVENTORY_SIZE = 4;
    public static final int UPGRADE0_INVENTORY_INDEX = 1;
    public static final int UPGRADE1_INVENTORY_INDEX = 2;
    public static final int UPGRADE2_INVENTORY_INDEX = 3;
    public static final int OUTPUT_INVENTORY_INDEX = 0;
    
    public int maxWork = 50;
	public int work = 0;
	
	private int lastWork = 0;
	private boolean hadWork;
	private ItemStack[] inventory;
	
	
	public TileEntityGenerator() {
		this.maxEnergy = 100;
		
		inventory = new ItemStack[INVENTORY_SIZE];
	}
	
	@Override
	public String getInventoryName() {
		return "Generator";
	}
	
	public int getSizeInventory(){
		return this.inventory.length;
	}

	@Override
	public ItemStack getStackInSlot(int i) {
		return this.inventory[i];
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt){
		super.readFromNBT(nbt);
		
		NBTTagList list = nbt.getTagList("Items", 10);
		inventory = new ItemStack[this.getSizeInventory()];
		
		for (int i=0; i<list.tagCount(); i++){
			NBTTagCompound compound = (NBTTagCompound) list.getCompoundTagAt(i);
			byte b = compound.getByte("Slot");
			
			if (b>=0 && b<inventory.length){
				this.inventory[b] = ItemStack.loadItemStackFromNBT(compound);
			}
		}
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt){
		super.writeToNBT(nbt);
				
		NBTTagList list = new NBTTagList();
		
		for (int i=0; i<inventory.length; i++){
			if (inventory[i]!=null){
				NBTTagCompound compound = new NBTTagCompound();
				compound.setByte("Slot", (byte) i);
				inventory[i].writeToNBT(compound);
				list.appendTag(compound);
			}
		}
		nbt.setTag("Items", list);
	}
	
	@Override
	public boolean allowInput(ForgeDirection side) {
		return false;
	}
	
	@Override
	public ForgeDirection shouldRequest() {
		ForgeDirection s = ForgeDirection.DOWN;
		
		TileEntity tile = this.worldObj.getTileEntity(this.xCoord + s.offsetX, this.yCoord + s.offsetY, this.zCoord + s.offsetZ);
		if (tile != null && shouldConnect(tile)) {
			if (((TileEntityEnergyHandler) tile).isRequesting() && ((TileEntityEnergyHandler) tile).allowInput(s.getOpposite())) {
				return s;
			}
		}
		
		return null;
	}
	
	/**
	 * @return Whether the generator can generate.
	 */
	public boolean canGenerate() {
		return false;
	}
	
	/**
	 * Called when the generator is able to generate.
	 * @return Energy Generated
	 */
	public int Generate() {
		return 0;
	}

	/**
	 * Called to check slots for certain upgrades.
	 * @param slot The inventory slot to check.
	 * @param type The type of upgrade to check for.
	 * @return The tier of the upgrade if it is found. 
	 */
	public int getUpgradeTier(int slot, int type) {
		if (inventory[slot] != null 
		&& (inventory[slot].getItem() instanceof ItemUpgrade) 
		&& ((ItemUpgrade) inventory[slot].getItem()).getType() == type) {
			
			return ((ItemUpgrade) inventory[slot].getItem()).getTier();
		}
		
		return 0;
	}
	
	/**
	 * If the generator has a speed upgrade, returns the tier.
	 * @return The speed upgrade tier.
	 */
	public int getSpeedBonus() {
		int bonus = getUpgradeTier(UPGRADE0_INVENTORY_INDEX, 0);
		bonus += getUpgradeTier(UPGRADE1_INVENTORY_INDEX, 0);
		bonus += getUpgradeTier(UPGRADE2_INVENTORY_INDEX, 0);
		
		return bonus;
	}
	
	/**
	 * If the generator has an efficiency upgrade, returns the tier.
	 * @return The efficiency upgrade tier.
	 */
	public int getEfficiencyBonus() {
		int bonus = getUpgradeTier(UPGRADE0_INVENTORY_INDEX, 1);
		bonus += getUpgradeTier(UPGRADE1_INVENTORY_INDEX, 1);
		bonus += getUpgradeTier(UPGRADE2_INVENTORY_INDEX, 1);
		
		return bonus;
	}
	
	/**
	 * If the generator has a range upgrade, returns the tier.
	 * @return The range upgrade tier.
	 */
	public int getRangeBonus() {
		int bonus = getUpgradeTier(UPGRADE0_INVENTORY_INDEX, 2);
		bonus += getUpgradeTier(UPGRADE1_INVENTORY_INDEX, 2);
		bonus += getUpgradeTier(UPGRADE2_INVENTORY_INDEX, 2);
		
		return bonus;
	}
	
	
	public void updateEntity(){
		if (!(this.worldObj).isRemote){
			boolean flag = false;
			boolean flag2 = false;
			boolean update = false;
			
			if (this.random.nextInt(10) == 0) {
				this.updateConnections();
			}
			
			
			if (this.energy < this.maxEnergy && canGenerate()) {
				if (this.work < this.maxWork) {
					this.work += 1 + (getSpeedBonus());
				} else {
					this.work = 0;
					this.energy += Generate();
				}
			}
			
			if (this.energy > 0) {
				if (inventory[OUTPUT_INVENTORY_INDEX] != null && (inventory[OUTPUT_INVENTORY_INDEX].getItem() instanceof ItemPowered) && inventory[OUTPUT_INVENTORY_INDEX].getItemDamage() > 0){
					inventory[OUTPUT_INVENTORY_INDEX].setItemDamage(inventory[OUTPUT_INVENTORY_INDEX].getItemDamage() - 1);
					this.energy--;
				} else {
					if (this.isProviding()) {
						this.sendEnergy(this.getRequesting());
					}
				}
			}
			
			if (this.work != this.lastWork) {
				flag2 = true;
			}
			
			if (this.hadWork && !flag2) {
				update = true;
			} else if (!this.hadWork && flag2) {
				update = true;
			}
			
			if (update) {
				markDirty();
				this.state = this.work != this.lastWork ? (byte) 1 : (byte) 0;
				this.worldObj.addBlockEvent(this.xCoord, this.yCoord, this.zCoord, this.getBlockType(), 1, this.state);
				worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
			}
			
			this.lastWork = this.work;
			this.hadWork = flag2;
		}
	}
	
	@Override
    public boolean receiveClientEvent(int eventId, int eventData)
    {
        if (eventId == 1)
        {
            this.state = (byte) eventData;
            //this.worldObj.updateAllLightTypes
            this.worldObj.func_147451_t(this.xCoord, this.yCoord, this.zCoord);
            return true;
            
        }
        else 
        {
            return super.receiveClientEvent(eventId, eventData);
        }
    }
	
	@Override
    public ItemStack decrStackSize(int slotIndex, int decrementAmount)
    {
        ItemStack itemStack = getStackInSlot(slotIndex);
        if (itemStack != null)
        {
            if (itemStack.stackSize <= decrementAmount)
            {
                setInventorySlotContents(slotIndex, null);
            }
            else
            {
                itemStack = itemStack.splitStack(decrementAmount);
                if (itemStack.stackSize == 0)
                {
                    setInventorySlotContents(slotIndex, null);
                }
            }
        }

        return itemStack;
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int slotIndex)
    {
        ItemStack itemStack = getStackInSlot(slotIndex);
        if (itemStack != null)
        {
            setInventorySlotContents(slotIndex, null);
        }
        return itemStack;
    }


    @Override
    public void setInventorySlotContents(int slotIndex, ItemStack itemStack)
    {
        inventory[slotIndex] = itemStack;
        if (itemStack != null && itemStack.stackSize > getInventoryStackLimit())
        {
            itemStack.stackSize = getInventoryStackLimit();
        }
    }

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer) {
		return true;
	}
	
	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack) {
		return true;
	}

	@Override
    public int[] getAccessibleSlotsFromSide(int side)
    {
        return side == ForgeDirection.DOWN.ordinal() ? new int[]{OUTPUT_INVENTORY_INDEX} : new int[]{OUTPUT_INVENTORY_INDEX};
    }

	@Override
	public boolean canInsertItem(int i, ItemStack itemstack, int j) {
		return this.isItemValidForSlot(i, itemstack);
	}

	@Override
    public boolean canExtractItem(int slotIndex, ItemStack itemStack, int side)
    {
        return slotIndex == OUTPUT_INVENTORY_INDEX;
    }

	public int getEnergyScaled(int i) {		
		return (int) ((((float) this.energy) / ((float) this.maxEnergy)) * (float) i);
	}

	@Override
	public boolean hasCustomInventoryName() {
		return true;
	}

	@Override
	public void openInventory() {}

	@Override
	public void closeInventory() {}

}
