package Ablaze.block;

import net.minecraft.block.BlockTorch;
import net.minecraft.client.renderer.texture.IIconRegister;
import Ablaze.Init;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockBlazeTorch extends BlockTorch
{
    public BlockBlazeTorch()
    {
        super();
        this.setTickRandomly(true);
        this.setHardness(0.0F);
        this.setLightLevel(1F);
        this.setStepSound(soundTypeWood);
    }

	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
	    this.blockIcon = par1IconRegister.registerIcon(Init.modid + ":" + (this.getUnlocalizedName().substring(5)));

	}
}
