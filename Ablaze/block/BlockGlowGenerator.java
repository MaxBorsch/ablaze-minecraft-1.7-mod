package Ablaze.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import Ablaze.Init;
import Ablaze.tileentity.TileEntityGlowGenerator;
import cpw.mods.fml.common.network.internal.FMLNetworkHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockGlowGenerator extends BlockContainer
{
	private final boolean isActive;
    private Random random = new Random();
	
	private static boolean keepInventory;
	
    public BlockGlowGenerator(boolean par2)
    {
        super(Material.rock);
        this.setBlockBounds(0.05F, 0.0F, 0.05F, 0.95F, 1F, 0.95F);
        this.isActive = par2;
    }
    
    @Override
    public Item getItemDropped(int par1, Random rand, int par2){
		return Item.getItemFromBlock(Init.blockGlowGenIdle);
	}
	
    @SideOnly(Side.CLIENT)
	public void randomDisplayTick(World world, int x, int y, int z, Random r){
	if (this.isActive){
		int direction = world.getBlockMetadata(x, y, z);
		
		float x1 = (float) x + 0.5F;
		float y1 = (float) y + r.nextFloat();
		float z1 = (float) z + 0.5F;
		
		float f = 0.52F;
		float f1 = r.nextFloat() * 0.6F - 0.3F;
		
		float dir2 = r.nextFloat()/12;
		float dir3 = r.nextFloat()/12;
		
			world.spawnParticle("flame", (double)x1+(f*2), (double)y+f, (double)z1, -(f/8), 0, 0);
			world.spawnParticle("flame", (double)x1-(f*2), (double)y+f, (double)z1, (f/8), 0, 0);
			world.spawnParticle("flame", (double)x1, (double)y+f, (double)z1+(f*2), 0, 0, -(f/8));
			world.spawnParticle("flame", (double)x1, (double)y+f, (double)z1-(f*2), 0, 0, (f/8));
			world.spawnParticle("flame", (double)x1, (double)y+f+(f*2), (double)z1, 0, -(f/8), 0);

			world.spawnParticle("smoke", (double)x1, (double)y+f, (double)z1, 0D, 0D, 0D);
	}
	}
	
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ){
		if (!world.isRemote){
			FMLNetworkHandler.openGui(player, Init.instance, Init.guiIdGlowGenerator, world, x, y, z);
		}
		return true;
	}
		
	@Override
    public void breakBlock(World world, int x, int y, int z, Block var3, int var4){
		if (!keepInventory){
			TileEntityGlowGenerator tileentity = (TileEntityGlowGenerator) world.getTileEntity(x, y, z);
			if (tileentity!=null){
				for (int i=0; i<tileentity.getSizeInventory(); i++){
					ItemStack itemstack = tileentity.getStackInSlot(i);
					
					if (itemstack!=null){
						float f = random.nextFloat() * 0.8F + 0.1F;
						float f2 = random.nextFloat() * 0.8F + 0.1F;
						float f3 = random.nextFloat() * 0.8F + 0.1F;
						
						while (itemstack.stackSize > 0){
							int j = random.nextInt(21) + 10;
							
							if (j > itemstack.stackSize){
								j = itemstack.stackSize;
							}
							itemstack.stackSize-= j;
							
							EntityItem item = new EntityItem(world, (double)((float)x + f), (double)((float)y + f2), (double)((float)z + f3), new ItemStack(itemstack.getItem(), j, itemstack.getItemDamage()));
						
						if (itemstack.hasTagCompound()){
							item.getEntityItem().setTagCompound((NBTTagCompound) itemstack.getTagCompound().copy());
						}
						
						float f4 = 0.5F;
						item.motionX = (double)((float)this.random.nextGaussian() * f4);
						item.motionY = (double)((float)this.random.nextGaussian() * f4);
						item.motionZ = (double)((float)this.random.nextGaussian() * f4 + 0.2F);
						
						world.spawnEntityInWorld(item);
						}
					}
				}
			}
		}
		super.breakBlock(world, x, y, z, var3, var4);
	}
    
	public static void updateState(boolean b, World worldObj, int xCoord, int yCoord, int zCoord) {
		TileEntity tileentity = worldObj.getTileEntity(xCoord, yCoord, zCoord);
		keepInventory = true;
		if (b){
			worldObj.setBlock(xCoord, yCoord, zCoord, Init.blockGlowGenActive);
		}else{
			worldObj.setBlock(xCoord, yCoord, zCoord, Init.blockGlowGenIdle);
		}
		keepInventory = false;
				
		if (tileentity != null){
			tileentity.validate();
			worldObj.setTileEntity(xCoord, yCoord, zCoord, tileentity);
		}
	}
	
	@Override
	public boolean hasComparatorInputOverride(){
		return true;
	}
	
	@Override
	public int getComparatorInputOverride(World world, int x, int y, int z, int i){
		return Container.calcRedstoneFromInventory((IInventory) world.getTileEntity(x, y, z));
	}
	
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
	    this.blockIcon = par1IconRegister.registerIcon(Init.modid + ":" + "stone");

	}
	
	public boolean isOpaqueCube(){
		return false;
	}
	
	public boolean renderAsNormalBlock(){
		return false;
	}
	
    public int getRenderType()
    {
        return -1;
    }

	@Override
	public TileEntity createNewTileEntity(World world, int par2) {
		return new TileEntityGlowGenerator();
	}
	
	
	
}
