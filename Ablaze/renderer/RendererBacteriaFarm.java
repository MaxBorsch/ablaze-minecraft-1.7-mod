package Ablaze.renderer;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import Ablaze.Init;
import Ablaze.model.ModelBacteriaFarm;
import Ablaze.tileentity.TileEntityBacteriaFarm;
import cpw.mods.fml.client.FMLClientHandler;

public class RendererBacteriaFarm extends TileEntitySpecialRenderer {

	private ModelBacteriaFarm model;
	
	public static final ResourceLocation TEXTURE_ON = new ResourceLocation(Init.modid, "textures/models/block/BacteriaFarm_active.png");
	public static final ResourceLocation TEXTURE_OFF = new ResourceLocation(Init.modid, "textures/models/block/BacteriaFarm.png");
	
	public RendererBacteriaFarm(){
		this.model = new ModelBacteriaFarm();
	}
	
	@Override
	public void renderTileEntityAt(TileEntity tileentity, double x, double y,double z, float f) {
	
		TileEntityBacteriaFarm te = (TileEntityBacteriaFarm) tileentity;
		
		FMLClientHandler.instance().getClient().renderEngine.bindTexture(te.getState() == 1 ? TEXTURE_ON : TEXTURE_OFF);
		
		GL11.glPushMatrix();
		GL11.glTranslatef((float) x + 0.5F, (float)y + 1.5F, (float)z + 0.5F);
		GL11.glRotatef(180, 0F, 0F, 1F);
		model.renderModel(0.0625F, te.hasJar);
		GL11.glPopMatrix();

	}

}
