package Ablaze.nei;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import Ablaze.Init;
import Ablaze.recipe.ConcentratorRecipeHandler;
import Ablaze.recipe.ExtractorRecipeHandler;
import Ablaze.recipe.InfuserRecipeHandler;
import Ablaze.recipe.RefineryRecipeHandler;
import Ablaze.recipe.WeldingPanelRecipeHandler;
import codechicken.nei.PositionedStack;
import codechicken.nei.api.API;
import codechicken.nei.api.IConfigureNEI;


public class NEIAblazeConfig implements IConfigureNEI {

	private static HashMap<HashMap<Integer, PositionedStack>, PositionedStack> refineryRecipes = new HashMap<HashMap<Integer, PositionedStack>, PositionedStack>();
	private static HashMap<HashMap<Integer, PositionedStack>, PositionedStack> extractorRecipes = new HashMap<HashMap<Integer, PositionedStack>, PositionedStack>();
	private static HashMap<HashMap<Integer, PositionedStack>, PositionedStack> infuserRecipes = new HashMap<HashMap<Integer, PositionedStack>, PositionedStack>();
	private static HashMap<HashMap<Integer, PositionedStack>, PositionedStack> concentratorRecipes = new HashMap<HashMap<Integer, PositionedStack>, PositionedStack>();
	private static HashMap<HashMap<Integer, PositionedStack>, PositionedStack> weldingpanelRecipes = new HashMap<HashMap<Integer, PositionedStack>, PositionedStack>();

	
	@Override
	public void loadConfig() {
		this.registerRecipes();
		API.registerRecipeHandler(new RefineryRecipeHandler());
		API.registerUsageHandler(new RefineryRecipeHandler());
		API.registerRecipeHandler(new ExtractorRecipeHandler());
		API.registerUsageHandler(new ExtractorRecipeHandler());
		API.registerRecipeHandler(new InfuserRecipeHandler());
		API.registerUsageHandler(new InfuserRecipeHandler());
		API.registerRecipeHandler(new ConcentratorRecipeHandler());
		API.registerUsageHandler(new ConcentratorRecipeHandler());
		API.registerRecipeHandler(new WeldingPanelRecipeHandler());
		API.registerUsageHandler(new WeldingPanelRecipeHandler());
	}

	@Override
	public String getName() {
		return "Ablaze NEI Plugin";
	}

	@Override
	public String getVersion() {
		return "1.0.0";
	}
	
	public void registerRecipes()
	{
		this.addRefineryRecipes();
		this.addExtractorRecipes();
		this.addInfuserRecipes();
		this.addConcentratorRecipes();
		this.addWeldingPanelRecipes();

	}
	
	private void addRefineryRecipes()
	{
		HashMap<Integer, PositionedStack> input1 = new HashMap<Integer, PositionedStack>();
		input1.put(0, new PositionedStack(new ItemStack(Items.blaze_powder), 51, 6));
		input1.put(1, new PositionedStack(new ItemStack(Items.coal), 51, 42));
		input1.put(2, new PositionedStack(new ItemStack(Init.itemAsh, 2), 143, 24));
		this.registerRefineryRecipe(input1, new PositionedStack(new ItemStack(Init.itemRefinedBlazeDust), 111, 24));
		
		HashMap<Integer, PositionedStack> input2 = new HashMap<Integer, PositionedStack>();
		input2.put(0, new PositionedStack(new ItemStack(Items.blaze_powder), 51, 6));
		input2.put(1, new PositionedStack(new ItemStack(Items.coal), 51, 42));
		input2.put(2, new PositionedStack(new ItemStack(Init.itemRefinedBlazeDust), 111, 24));
		this.registerRefineryRecipe(input2, new PositionedStack(new ItemStack(Init.itemAsh, 2), 143, 24));
	}
	
	private void addExtractorRecipes()
	{
		HashMap<Integer, PositionedStack> input1 = new HashMap<Integer, PositionedStack>();
		input1.put(0, new PositionedStack(new ItemStack(Items.coal), 51, 42));
		input1.put(1, new PositionedStack(new ItemStack(Items.lava_bucket), 51, 6));
		input1.put(2, new PositionedStack(new ItemStack(Items.glass_bottle), 25, 6));
		this.registerExtractorRecipe(input1, new PositionedStack(new ItemStack(Init.itemBlazingEssence), 111, 24));
		
		HashMap<Integer, PositionedStack> input2 = new HashMap<Integer, PositionedStack>();
		input2.put(0, new PositionedStack(new ItemStack(Items.coal), 51, 42));
		input2.put(1, new PositionedStack(new ItemStack(Items.snowball), 51, 6));
		input2.put(2, new PositionedStack(new ItemStack(Items.glass_bottle), 25, 6));
		this.registerExtractorRecipe(input2, new PositionedStack(new ItemStack(Init.itemChillingEssence), 111, 24));
		
		HashMap<Integer, PositionedStack> input3 = new HashMap<Integer, PositionedStack>();
		input3.put(0, new PositionedStack(new ItemStack(Items.coal), 51, 42));
		input3.put(1, new PositionedStack(new ItemStack(Items.wheat_seeds), 51, 6));
		input3.put(2, new PositionedStack(new ItemStack(Items.glass_bottle), 25, 6));
		this.registerExtractorRecipe(input3, new PositionedStack(new ItemStack(Init.itemBioEssence), 111, 24));
	}
	
	private void addInfuserRecipes()
	{
		HashMap<Integer, PositionedStack> input1 = new HashMap<Integer, PositionedStack>();
		input1.put(0, new PositionedStack(new ItemStack(Init.itemLucidCrystal), 51, 6));
		input1.put(1, new PositionedStack(new ItemStack(Init.itemBlazingEssence), 25, 6));
		input1.put(2, new PositionedStack(new ItemStack(Items.coal), 51, 42));
		this.registerInfuserRecipe(input1, new PositionedStack(new ItemStack(Init.itemBlazingCrystal), 111, 24));
		
		HashMap<Integer, PositionedStack> input2 = new HashMap<Integer, PositionedStack>();
		input2.put(0, new PositionedStack(new ItemStack(Init.itemLucidCrystal), 51, 6));
		input2.put(1, new PositionedStack(new ItemStack(Init.itemChillingEssence), 25, 6));
		input2.put(2, new PositionedStack(new ItemStack(Items.coal), 51, 42));
		this.registerInfuserRecipe(input2, new PositionedStack(new ItemStack(Init.itemChillingCrystal), 111, 24));
		
		HashMap<Integer, PositionedStack> input3 = new HashMap<Integer, PositionedStack>();
		input3.put(0, new PositionedStack(new ItemStack(Init.itemLucidCrystal), 51, 6));
		input3.put(1, new PositionedStack(new ItemStack(Init.itemBioEssence), 25, 6));
		input3.put(2, new PositionedStack(new ItemStack(Items.coal), 51, 42));
		this.registerInfuserRecipe(input3, new PositionedStack(new ItemStack(Init.itemBioCrystal), 111, 24));
	}
	
	private void addConcentratorRecipes()
	{
		HashMap<Integer, PositionedStack> input1 = new HashMap<Integer, PositionedStack>();
		input1.put(0, new PositionedStack(new ItemStack(Init.itemBlazingEssence), 51, 6));
		input1.put(1, new PositionedStack(new ItemStack(Items.coal), 51, 42));
		input1.put(2, new PositionedStack(new ItemStack(Init.itemThermobicPlate), 25, 24));
		this.registerConcentratorRecipe(input1, new PositionedStack(new ItemStack(Init.itemBlazingSingularity), 111, 24));
		
		HashMap<Integer, PositionedStack> input2 = new HashMap<Integer, PositionedStack>();
		input2.put(0, new PositionedStack(new ItemStack(Init.itemChillingEssence), 51, 6));
		input2.put(1, new PositionedStack(new ItemStack(Items.coal), 51, 42));
		input2.put(2, new PositionedStack(new ItemStack(Init.itemThermobicPlate), 25, 24));
		this.registerConcentratorRecipe(input2, new PositionedStack(new ItemStack(Init.itemChillingSingularity), 111, 24));
		
		HashMap<Integer, PositionedStack> input3 = new HashMap<Integer, PositionedStack>();
		input3.put(0, new PositionedStack(new ItemStack(Init.itemBioEssence), 51, 6));
		input3.put(1, new PositionedStack(new ItemStack(Items.coal), 51, 42));
		input3.put(2, new PositionedStack(new ItemStack(Init.itemThermobicPlate), 25, 24));
		this.registerConcentratorRecipe(input3, new PositionedStack(new ItemStack(Init.itemBioSingularity), 111, 24));
	}
	
	private void addWeldingPanelRecipes()
	{
		HashMap<Integer, PositionedStack> input1 = new HashMap<Integer, PositionedStack>();
		input1.put(0, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 3, 10));
		input1.put(1, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 21, 10));
		input1.put(2, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 39, 10));
		input1.put(3, new PositionedStack(new ItemStack(Init.itemMultiConnector), 3, 28));
		input1.put(4, new PositionedStack(new ItemStack(Init.itemWiring), 21, 28));
		input1.put(5, new PositionedStack(new ItemStack(Init.itemServoMotor), 39, 28));
		input1.put(6, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 57, 28));
		input1.put(7, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 3, 46));
		input1.put(8, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 21, 46));
		input1.put(9, new PositionedStack(new ItemStack(Init.itemGearbox), 39, 46));
		input1.put(10, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 57, 46));
		input1.put(11, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 21, 64));
		input1.put(12, new PositionedStack(new ItemStack(Init.itemBronzeSlice), 39, 64));
		input1.put(13, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 57, 64));

		this.registerWeldingPanelRecipe(input1, new PositionedStack(new ItemStack(Init.itemGolemRightArm), 138, 38));
	
		HashMap<Integer, PositionedStack> input2 = new HashMap<Integer, PositionedStack>();
		input2.put(0, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 21, 10));
		input2.put(1, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 39, 10));
		input2.put(2, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 57, 10));
		input2.put(3, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 3, 28));
		input2.put(4, new PositionedStack(new ItemStack(Init.itemServoMotor), 21, 28));
		input2.put(5, new PositionedStack(new ItemStack(Init.itemWiring), 39, 28));
		input2.put(6, new PositionedStack(new ItemStack(Init.itemMultiConnector), 57, 28));
		input2.put(7, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 3, 46));
		input2.put(8, new PositionedStack(new ItemStack(Init.itemGearbox), 21, 46));
		input2.put(9, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 39, 46));
		input2.put(10, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 57, 46));
		input2.put(11, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 3, 64));
		input2.put(12, new PositionedStack(new ItemStack(Init.itemBronzeSlice), 21, 64));
		input2.put(13, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 39, 64));

		this.registerWeldingPanelRecipe(input2, new PositionedStack(new ItemStack(Init.itemGolemLeftArm), 138, 38));
		
		HashMap<Integer, PositionedStack> input3 = new HashMap<Integer, PositionedStack>();
		input3.put(0, new PositionedStack(new ItemStack(Init.itemBronzeSlice), 21, 10));
		input3.put(1, new PositionedStack(new ItemStack(Init.itemBronzeSlice), 39, 10));
		input3.put(2, new PositionedStack(new ItemStack(Init.itemBronzeSlice), 3, 28));
		input3.put(3, new PositionedStack(new ItemStack(Init.itemLargeGear), 21, 28));
		input3.put(4, new PositionedStack(new ItemStack(Init.itemLargeGear), 39, 28));
		input3.put(5, new PositionedStack(new ItemStack(Init.itemBronzeSlice), 57, 28));
		input3.put(6, new PositionedStack(new ItemStack(Init.itemBronzeSlice), 3, 46));
		input3.put(7, new PositionedStack(new ItemStack(Init.itemLargeGear), 21, 46));
		input3.put(8, new PositionedStack(new ItemStack(Init.itemLargeGear), 39, 46));
		input3.put(9, new PositionedStack(new ItemStack(Init.itemBronzeSlice), 57, 46));
		input3.put(10, new PositionedStack(new ItemStack(Init.itemSmallGear), 3, 64));
		input3.put(11, new PositionedStack(new ItemStack(Init.itemBronzeSlice), 21, 64));
		input3.put(12, new PositionedStack(new ItemStack(Init.itemBronzeSlice), 39, 64));

		this.registerWeldingPanelRecipe(input3, new PositionedStack(new ItemStack(Init.itemGearbox), 138, 38));
	
		HashMap<Integer, PositionedStack> input4 = new HashMap<Integer, PositionedStack>();
		input4.put(0, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 3, 10));
		input4.put(1, new PositionedStack(new ItemStack(Init.itemMultiConnector), 21, 10));
		input4.put(2, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 39, 10));
		input4.put(3, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 3, 28));
		input4.put(4, new PositionedStack(new ItemStack(Init.itemWiring), 21, 28));
		input4.put(5, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 39, 28));
		input4.put(6, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 3, 46));
		input4.put(7, new PositionedStack(new ItemStack(Init.itemWiring), 21, 46));
		input4.put(8, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 39, 46));
		input4.put(9, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 3, 64));
		input4.put(10, new PositionedStack(new ItemStack(Init.itemServoMotor), 21, 64));
		input4.put(11, new PositionedStack(new ItemStack(Init.itemGearbox), 39, 64));
		input4.put(12, new PositionedStack(new ItemStack(Init.itemBronzeSlice), 57, 64));

		this.registerWeldingPanelRecipe(input4, new PositionedStack(new ItemStack(Init.itemGolemRightLeg), 138, 38));
		
		HashMap<Integer, PositionedStack> input5 = new HashMap<Integer, PositionedStack>();
		input5.put(1, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 21, 10));
		input5.put(2, new PositionedStack(new ItemStack(Init.itemMultiConnector), 39, 10));
		input5.put(3, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 57, 10));
		input5.put(4, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 21, 28));
		input5.put(5, new PositionedStack(new ItemStack(Init.itemWiring), 39, 28));
		input5.put(6, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 57, 28));
		input5.put(7, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 21, 46));
		input5.put(8, new PositionedStack(new ItemStack(Init.itemWiring), 39, 46));
		input5.put(9, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 57, 46));
		input5.put(10, new PositionedStack(new ItemStack(Init.itemBronzeSlice), 3, 64));
		input5.put(11, new PositionedStack(new ItemStack(Init.itemGearbox), 21, 64));
		input5.put(12, new PositionedStack(new ItemStack(Init.itemServoMotor), 39, 64));
		input5.put(13, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 57, 64));

		this.registerWeldingPanelRecipe(input5, new PositionedStack(new ItemStack(Init.itemGolemLeftLeg), 138, 38));
		
		HashMap<Integer, PositionedStack> input6 = new HashMap<Integer, PositionedStack>();
		input6.put(0, new PositionedStack(new ItemStack(Init.itemHyperconductiveWiring), 3, 10));
		input6.put(1, new PositionedStack(new ItemStack(Init.itemHeatVent), 21, 10));
		input6.put(2, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 39, 10));
		input6.put(3, new PositionedStack(new ItemStack(Init.itemHyperconductiveWiring), 57, 10));
		input6.put(4, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 3, 28));
		input6.put(5, new PositionedStack(new ItemStack(Init.itemGearbox), 21, 28));
		input6.put(6, new PositionedStack(new ItemStack(Init.itemGearbox), 39, 28));
		input6.put(7, new PositionedStack(new ItemStack(Init.itemHeatVent), 57, 28));
		input6.put(8, new PositionedStack(new ItemStack(Init.itemHeatVent), 3, 46));
		input6.put(9, new PositionedStack(new ItemStack(Items.coal), 21, 46));
		input6.put(10, new PositionedStack(new ItemStack(Items.coal), 39, 46));
		input6.put(11, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 57, 46));
		input6.put(12, new PositionedStack(new ItemStack(Init.itemHyperconductiveWiring), 3, 64));
		input6.put(13, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 21, 64));
		input6.put(14, new PositionedStack(new ItemStack(Init.itemHeatVent), 39, 64));
		input6.put(15, new PositionedStack(new ItemStack(Init.itemHyperconductiveWiring), 57, 64));

		this.registerWeldingPanelRecipe(input6, new PositionedStack(new ItemStack(Init.itemThermoReactor), 138, 38));
		
		HashMap<Integer, PositionedStack> input7 = new HashMap<Integer, PositionedStack>();
		input7.put(0, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 3, 10));
		input7.put(1, new PositionedStack(new ItemStack(Init.itemMultiConnector), 21, 10));
		input7.put(2, new PositionedStack(new ItemStack(Init.itemMultiConnector), 39, 10));
		input7.put(3, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 57, 10));
		input7.put(4, new PositionedStack(new ItemStack(Init.itemMultiConnector), 3, 28));
		input7.put(5, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 21, 28));
		input7.put(6, new PositionedStack(new ItemStack(Init.itemThermoReactor), 39, 28));
		input7.put(7, new PositionedStack(new ItemStack(Init.itemMultiConnector), 57, 28));
		input7.put(8, new PositionedStack(new ItemStack(Init.itemMultiConnector), 3, 46));
		input7.put(9, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 21, 46));
		input7.put(10, new PositionedStack(new ItemStack(Init.itemGearbox), 39, 46));
		input7.put(11, new PositionedStack(new ItemStack(Init.itemMultiConnector), 57, 46));
		input7.put(12, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 3, 64));
		input7.put(13, new PositionedStack(new ItemStack(Init.itemMultiConnector), 21, 64));
		input7.put(14, new PositionedStack(new ItemStack(Init.itemMultiConnector), 39, 64));
		input7.put(15, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 57, 64));

		this.registerWeldingPanelRecipe(input7, new PositionedStack(new ItemStack(Init.itemGolemTorso), 138, 38));
		
		HashMap<Integer, PositionedStack> input8 = new HashMap<Integer, PositionedStack>();
		input8.put(0, new PositionedStack(new ItemStack(Init.itemFlourescentCube), 3, 10));
		input8.put(1, new PositionedStack(new ItemStack(Init.itemT1ProcessingUnit), 21, 10));
		input8.put(2, new PositionedStack(new ItemStack(Init.itemFlourescentCube), 39, 10));
		input8.put(3, new PositionedStack(new ItemStack(Init.itemT1LogicUnit), 21, 28));

		this.registerWeldingPanelRecipe(input8, new PositionedStack(new ItemStack(Init.itemGolemHead), 138, 38));
		
		HashMap<Integer, PositionedStack> input9 = new HashMap<Integer, PositionedStack>();
		input9.put(0, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 21, 10));
		input9.put(1, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 39, 10));
		input9.put(2, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 3, 28));
		input9.put(3, new PositionedStack(new ItemStack(Init.itemT1Processor), 21, 28));
		input9.put(4, new PositionedStack(new ItemStack(Init.itemThinGoldWiring), 39, 28));
		input9.put(5, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 57, 28));


		this.registerWeldingPanelRecipe(input9, new PositionedStack(new ItemStack(Init.itemT1ProcessingUnit), 138, 38));
		
		HashMap<Integer, PositionedStack> input10 = new HashMap<Integer, PositionedStack>();
		input10.put(0, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 3, 46));
		input10.put(1, new PositionedStack(new ItemStack(Init.itemThinGoldWiring), 21, 46));
		input10.put(2, new PositionedStack(new ItemStack(Init.itemT1LogicCard), 39, 46));
		input10.put(3, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 57, 46));
		input10.put(4, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 21, 64));
		input10.put(5, new PositionedStack(new ItemStack(Init.itemBronzeIngot), 39, 64));

		this.registerWeldingPanelRecipe(input10, new PositionedStack(new ItemStack(Init.itemT1LogicUnit), 138, 38));
		
		HashMap<Integer, PositionedStack> input11 = new HashMap<Integer, PositionedStack>();
		input11.put(0, new PositionedStack(new ItemStack(Init.itemThinWiring), 3, 10));
		input11.put(1, new PositionedStack(new ItemStack(Init.itemThinWiring), 21, 10));
		input11.put(2, new PositionedStack(new ItemStack(Init.itemThinWiring), 39, 10));
		input11.put(3, new PositionedStack(new ItemStack(Init.itemThinWiring), 57, 10));
		input11.put(4, new PositionedStack(new ItemStack(Init.itemThinWiring), 3, 28));
		input11.put(5, new PositionedStack(new ItemStack(Init.itemGearbox), 21, 28));
		input11.put(6, new PositionedStack(new ItemStack(Init.itemThinGoldWiring), 39, 28));
		input11.put(7, new PositionedStack(new ItemStack(Init.itemThinWiring), 57, 28));
		input11.put(8, new PositionedStack(new ItemStack(Init.itemThinWiring), 3, 46));
		input11.put(9, new PositionedStack(new ItemStack(Init.itemThinGoldWiring), 21, 46));
		input11.put(10, new PositionedStack(new ItemStack(Init.itemGearbox), 39, 46));
		input11.put(11, new PositionedStack(new ItemStack(Init.itemThinWiring), 57, 46));
		input11.put(12, new PositionedStack(new ItemStack(Init.itemThinWiring), 3, 64));
		input11.put(13, new PositionedStack(new ItemStack(Init.itemThinWiring), 21, 64));
		input11.put(14, new PositionedStack(new ItemStack(Init.itemThinWiring), 39, 64));
		input11.put(15, new PositionedStack(new ItemStack(Init.itemThinWiring), 57, 64));

		this.registerWeldingPanelRecipe(input11, new PositionedStack(new ItemStack(Init.itemT1Processor), 138, 38));
		
		HashMap<Integer, PositionedStack> input12 = new HashMap<Integer, PositionedStack>();

		input12.put(0, new PositionedStack(new ItemStack(Init.itemBronzeSlice), 3, 28));
		input12.put(1, new PositionedStack(new ItemStack(Init.itemGearbox), 21, 28));
		input12.put(2, new PositionedStack(new ItemStack(Init.itemThinGoldWiring), 39, 28));
		input12.put(3, new PositionedStack(new ItemStack(Init.itemBronzeSlice), 57, 28));
		input12.put(4, new PositionedStack(new ItemStack(Init.itemBronzeSlice), 3, 46));
		input12.put(5, new PositionedStack(new ItemStack(Init.itemThinGoldWiring), 21, 46));
		input12.put(6, new PositionedStack(new ItemStack(Init.itemGearbox), 39, 46));
		input12.put(7, new PositionedStack(new ItemStack(Init.itemBronzeSlice), 57, 46));

		this.registerWeldingPanelRecipe(input12, new PositionedStack(new ItemStack(Init.itemT1LogicCard), 138, 38));
		
		/*
	 HashMap<Integer, PositionedStack> input1 = new HashMap<Integer, PositionedStack>();
		input1.put(0, new PositionedStack(new ItemStack(Init.itemAsh), 3, 10));
		input1.put(1, new PositionedStack(new ItemStack(Init.itemAsh), 21, 10));
		input1.put(2, new PositionedStack(new ItemStack(Init.itemAsh), 39, 10));
		input1.put(3, new PositionedStack(new ItemStack(Init.itemAsh), 57, 10));
		input1.put(4, new PositionedStack(new ItemStack(Init.itemAsh), 3, 28));
		input1.put(5, new PositionedStack(new ItemStack(Init.itemAsh), 21, 28));
		input1.put(6, new PositionedStack(new ItemStack(Init.itemAsh), 39, 28));
		input1.put(7, new PositionedStack(new ItemStack(Init.itemAsh), 57, 28));
		input1.put(8, new PositionedStack(new ItemStack(Init.itemAsh), 3, 46));
		input1.put(9, new PositionedStack(new ItemStack(Init.itemAsh), 21, 46));
		input1.put(10, new PositionedStack(new ItemStack(Init.itemAsh), 39, 46));
		input1.put(11, new PositionedStack(new ItemStack(Init.itemAsh), 57, 46));
		input1.put(12, new PositionedStack(new ItemStack(Init.itemAsh), 3, 64));
		input1.put(13, new PositionedStack(new ItemStack(Init.itemAsh), 21, 64));
		input1.put(14, new PositionedStack(new ItemStack(Init.itemAsh), 39, 64));
		input1.put(15, new PositionedStack(new ItemStack(Init.itemAsh), 57, 64));

		this.registerWeldingPanelRecipe(input1, new PositionedStack(new ItemStack(Init.itemBlazingSingularity), 138, 38));
	 */
	}
	
	public void registerRefineryRecipe(HashMap<Integer, PositionedStack> input, PositionedStack output)
	{
		NEIAblazeConfig.refineryRecipes.put(input, output);
	}
	
	public static Set<Entry<HashMap<Integer, PositionedStack>, PositionedStack>> getrefineryRecipes()
	{
		return NEIAblazeConfig.refineryRecipes.entrySet();
	}
	
	public void registerExtractorRecipe(HashMap<Integer, PositionedStack> input, PositionedStack output)
	{
		NEIAblazeConfig.extractorRecipes.put(input, output);
	}
	
	public static Set<Entry<HashMap<Integer, PositionedStack>, PositionedStack>> getextractorRecipes()
	{
		return NEIAblazeConfig.extractorRecipes.entrySet();
	}
	
	public void registerInfuserRecipe(HashMap<Integer, PositionedStack> input, PositionedStack output)
	{
		NEIAblazeConfig.infuserRecipes.put(input, output);
	}
	
	public static Set<Entry<HashMap<Integer, PositionedStack>, PositionedStack>> getinfuserRecipes()
	{
		return NEIAblazeConfig.infuserRecipes.entrySet();
	}

	public void registerConcentratorRecipe(HashMap<Integer, PositionedStack> input, PositionedStack output)
	{
		NEIAblazeConfig.concentratorRecipes.put(input, output);
	}
	
	public static Set<Entry<HashMap<Integer, PositionedStack>, PositionedStack>> getconcentratorRecipes()
	{
		return NEIAblazeConfig.concentratorRecipes.entrySet();
	}
	
	public void registerWeldingPanelRecipe(HashMap<Integer, PositionedStack> input, PositionedStack output)
	{
		NEIAblazeConfig.weldingpanelRecipes.put(input, output);
	}
	
	public static Set<Entry<HashMap<Integer, PositionedStack>, PositionedStack>> getweldingpanelRecipes()
	{
		return NEIAblazeConfig.weldingpanelRecipes.entrySet();
	}
}