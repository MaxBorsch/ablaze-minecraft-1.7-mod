package Ablaze.item;

import java.util.List;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import Ablaze.Init;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemResearchNotes extends Item {

	public ItemResearchNotes() {
		this.setMaxStackSize(1);
	}

	public void setResearch(ItemStack item, String research){
		if (item.hasTagCompound() == false){
			NBTTagCompound com = new NBTTagCompound();
			item.setTagCompound(com);
		}
		NBTTagCompound tag = item.getTagCompound();
		tag.setString("research", research);
	}
	
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
    {
        if (par1ItemStack.hasTagCompound())
        {
            NBTTagCompound nbttagcompound = par1ItemStack.getTagCompound();
            String nbttagstring = nbttagcompound.getString("research");

            if (nbttagstring != null)
            {
                par3List.add(EnumChatFormatting.GRAY + nbttagstring);
            }
        }
    }
	
    /**
     * If this function returns true (or the item is damageable), the ItemStack's NBT tag will be sent to the client.
     */
    public boolean getShareTag()
    {
        return true;
    }
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister par1IconRegister)
	{
	    this.itemIcon = par1IconRegister.registerIcon(Init.modid + ":" + (this.getUnlocalizedName().substring(5)));

	}
	
}
