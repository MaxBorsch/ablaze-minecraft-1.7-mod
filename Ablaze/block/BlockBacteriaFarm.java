package Ablaze.block;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import Ablaze.Init;
import Ablaze.tileentity.TileEntityBacteriaFarm;

public class BlockBacteriaFarm extends BlockMachine {
	
	@Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
    {
        if (player.isSneaking())
        {
            return false;
        }
        else
        {
            if (!world.isRemote)
            {
                if (world.getTileEntity(x, y, z) instanceof TileEntityBacteriaFarm)
                {
                    player.openGui(Init.instance, Init.guiIdBacteriaFarm, world, x, y, z);
                }
            }

            return true;
        }
    }
	
	@Override
    public int getLightValue(IBlockAccess world, int x, int y, int z)
    {
        if ((world.getTileEntity(x, y, z) instanceof TileEntityBacteriaFarm) && (((TileEntityBacteriaFarm) world.getTileEntity(x, y, z)).getState() == 1))
        {
            return 15;
        }

        return super.getLightValue(world, x, y, z);
    }
	
	@Override
	public TileEntity createNewTileEntity(World world, int par2) {
		return new TileEntityBacteriaFarm();
	}
}
