package Ablaze.renderer;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import Ablaze.Init;
import Ablaze.model.ModelBioGenerator;
import Ablaze.tileentity.TileEntityBioGenerator;
import cpw.mods.fml.client.FMLClientHandler;

public class RendererBioGenerator extends TileEntitySpecialRenderer {

	private ModelBioGenerator model;
	
	public static final ResourceLocation TEXTURE_ON = new ResourceLocation(Init.modid, "textures/models/block/BioGenerator_active.png");
	public static final ResourceLocation TEXTURE_OFF = new ResourceLocation(Init.modid, "textures/models/block/BioGenerator.png");
	
	public RendererBioGenerator(){
		this.model = new ModelBioGenerator();
	}
	
	@Override
	public void renderTileEntityAt(TileEntity tileentity, double x, double y,double z, float f) {
	
		TileEntityBioGenerator te = (TileEntityBioGenerator) tileentity;
		
		FMLClientHandler.instance().getClient().renderEngine.bindTexture(te.getState() == 1 ? TEXTURE_ON : TEXTURE_OFF);
		
		GL11.glPushMatrix();
		GL11.glTranslatef((float) x + 0.5F, (float)y + 1.5F, (float)z + 0.5F);
		GL11.glRotatef(180, 0F, 0F, 1F);
		GL11.glPushMatrix();
		model.renderModel(te.getAnimation(), 0.0625F);
		GL11.glPopMatrix();
		GL11.glPopMatrix();
	}

}
