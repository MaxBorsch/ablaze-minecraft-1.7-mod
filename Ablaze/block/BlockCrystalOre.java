package Ablaze.block;

import java.util.Random;

import net.minecraft.item.Item;
import Ablaze.Init;

public class BlockCrystalOre extends BlockBasicBlock
{
	
    /**
     * Returns the quantity of items to drop on block destruction.
     */
    public int quantityDropped(Random par1Random)
    {
        return 1;
    }
    
    @Override
	public Item getItemDropped(int par1, Random rand, int par2){
		return Init.itemLucidCrystal;
	}
	
}
