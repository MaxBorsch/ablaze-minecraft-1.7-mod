package Ablaze.renderer;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import Ablaze.model.ModelBomb;
import Ablaze.tileentity.TileEntityBombBlazing;

public class ItemRendererBombBlazing implements IItemRenderer {

	private ModelBomb model;
	private TileEntitySpecialRenderer render;
	
	public ItemRendererBombBlazing(TileEntitySpecialRenderer renderer){
		this.model = new ModelBomb();
		this.render = renderer;
	}
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		this.render.renderTileEntityAt(new TileEntityBombBlazing(), 0.0D, 0.0D, 0.0D, 0.0F);
	}

}
