package Ablaze.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import Ablaze.Init;

public class EntityEvolvedHungry extends EntityMob
{
    
    public EntityEvolvedHungry(World par1World)
    {
        super(par1World);
        this.setSize(3.2F, 3.2F);
        this.getNavigator().setAvoidsWater(true);
    }

    protected void entityInit()
    {
        super.entityInit();
    }
    
    public void onUpdate()
    {
        super.onUpdate();
    }

    protected Entity findPlayerToAttack()
    {
        double d0 = 40.0D;
        return this.worldObj.getClosestVulnerablePlayerToEntity(this, d0);
    }
    
    protected void attackEntity(Entity par1Entity, float par2)
    {
       if (par2 > 2.0F && par2 < 8.0F && this.rand.nextInt(6) == 0)
            {
                if (this.onGround)
                {
                    double d0 = par1Entity.posX - this.posX;
                    double d1 = par1Entity.posZ - this.posZ;
                    float f2 = MathHelper.sqrt_double(d0 * d0 + d1 * d1);
                    this.motionX = d0 / (double)f2 * 1.1D * 0.800000011920929D + this.motionX * 0.20000000298023224D;
                    this.motionZ = d1 / (double)f2 * 1.1D * 0.800000011920929D + this.motionZ * 0.20000000298023224D;
                    this.motionY = 0.4f;
                }
            }
            else
            {
                super.attackEntity(par1Entity, par2);
            }
    }

    protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(100.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(1.7D);
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(6D);
        this.getEntityAttribute(SharedMonsterAttributes.knockbackResistance).setBaseValue(15D);
    }

    public void onDeath(DamageSource par1DamageSource)
    {
        super.onDeath(par1DamageSource);
        if (!worldObj.isRemote){
	        	ItemStack itemDrop = new ItemStack(Items.bone, 15);
	            EntityItem drop = new EntityItem(worldObj, this.posX, this.posY+1, this.posZ, itemDrop);
	            drop.setPosition(this.posX, this.posY+1, this.posZ);
	            worldObj.spawnEntityInWorld(drop);
        }
    }
    
    public boolean interact(EntityPlayer par1EntityPlayer)
    {
        ItemStack itemstack = par1EntityPlayer.inventory.getCurrentItem();
        
        if (!par1EntityPlayer.worldObj.isRemote && itemstack.getItem() == Init.itemCleansingSugar)
        {
        	if (!worldObj.isRemote){
        		itemstack.stackSize--;
        		if (itemstack.stackSize <= 0) {
        			itemstack = null;
        		}
        	}
        	
        	this.setDead();
        	
        	EntityHungry e = new EntityHungry(this.worldObj);
    		e.setPosition(this.posX, this.posY, this.posZ);
    		e.setHunger(10);
    		e.setHome(e.posX, e.posY, e.posZ);
    		this.worldObj.spawnEntityInWorld(e);
        	
            return true;
        }
        else
        {
            return super.interact(par1EntityPlayer);
        }
    }
    
    /**
     * Returns the volume for the sounds this mob makes.
     */
    protected float getSoundVolume()
    {
        return 0.2F;
    }
    
    /**
     * Returns the sound this mob makes while it's alive.
     */
    protected String getLivingSound()
    {
        return "mob.silverfish.say";
    }

    /**
     * Returns the sound this mob makes when it is hurt.
     */
    protected String getHurtSound()
    {
        return "mob.slime.small";
    }

    /**
     * Returns the sound this mob makes on death.
     */
    protected String getDeathSound()
    {
        return "mob.silverfish.hurt";
    }

    /**
     * Plays step sound at given x, y, z for the entity
     */
    protected void playStepSound(int par1, int par2, int par3, int par4)
    {
        this.playSound("mob.silverfish.step", 1.0F, 1.0F);
    }

}
