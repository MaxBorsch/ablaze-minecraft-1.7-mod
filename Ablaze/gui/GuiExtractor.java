package Ablaze.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import Ablaze.Init;
import Ablaze.container.ContainerExtractor;
import Ablaze.tileentity.TileEntityExtractor;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class GuiExtractor extends GuiContainer {

	public TileEntityExtractor Extractor;
	public static final ResourceLocation Bkg = new ResourceLocation(Init.modid, "textures/gui/extractor.png");
	
	public GuiExtractor(InventoryPlayer inventory, TileEntityExtractor entity) {
		super(new ContainerExtractor(inventory, entity));
		
		this.Extractor = entity;
		this.xSize = 176;
		this.ySize = 166;
	}

	public void drawGuiContainerForegroundLayer(int par1, int par2){
		String name = this.Extractor.getInventoryName();
		
		this.fontRendererObj.drawString(name, this.xSize/2 - this.fontRendererObj.getStringWidth(name)/2, 6, 4210752);
	}
	
	public void drawGuiContainerBackgroundLayer(float f, int i, int j) {
		GL11.glColor4f(1F, 1F, 1F, 1F);
		
		Minecraft.getMinecraft().getTextureManager().bindTexture(Bkg);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
	
	if (Extractor.burnTime>0){
		int k = this.Extractor.getBurnTimeRemainingScaled(12);
		drawTexturedModalRect(guiLeft+56, guiTop+38-k, 176, k, 14, 12);
		//------------------------------------------------------------------------
		int k2 = Extractor.getCookProgressScaled(24);
		drawTexturedModalRect(guiLeft+79, guiTop+34, 176, 14, k2, 16);
	}
	
	}

}
