package Ablaze.item;

import net.minecraft.item.Item;

public class ItemPowered extends Item {
	
	public ItemPowered() {
		this.setMaxDamage(16);
		this.setMaxStackSize(1);
	}
	
}
