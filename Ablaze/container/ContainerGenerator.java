package Ablaze.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import Ablaze.tileentity.TileEntityGenerator;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerGenerator extends Container {

	private TileEntityGenerator Generator;
	public int lastenergy;
	public int lastwork;
	
	public ContainerGenerator(InventoryPlayer inventory, TileEntityGenerator tileentity){
		this.Generator = tileentity;
		
		this.addSlots(tileentity, inventory);
	}
	
	public void addSlots(TileEntityGenerator tileentity, InventoryPlayer inventory) {
		this.addSlotToContainer(new Slot(tileentity, 0, 128, 31));
		this.addSlotToContainer(new Slot(tileentity, 1, 8, 10));
		this.addSlotToContainer(new Slot(tileentity, 2, 8, 31));
		this.addSlotToContainer(new Slot(tileentity, 3, 8, 52));

		for (int i=0; i<3; i++){
			for (int j=0; j<9; j++){
				this.addSlotToContainer(new Slot(inventory, j + i*9 + 9, 8 + j*18, 84 + i*18));
			}
		}
		
		for (int i=0; i<9; i++){
			this.addSlotToContainer(new Slot(inventory, i, 8 + i*18, 142));
		}
	}
	
	@Override
	public void addCraftingToCrafters(ICrafting icrafting){
		super.addCraftingToCrafters(icrafting);
		
		icrafting.sendProgressBarUpdate(this, 0, this.Generator.energy);
		icrafting.sendProgressBarUpdate(this, 1, this.Generator.work);
	}
	
	@Override
	public void detectAndSendChanges(){
		super.detectAndSendChanges();
		for (int i=0; i<this.crafters.size(); i++){
			ICrafting icrafting = (ICrafting) this.crafters.get(i);
			
			if (this.lastenergy != this.Generator.energy){
				icrafting.sendProgressBarUpdate(this, 0, this.Generator.energy);
			}
			
			this.lastenergy = this.Generator.energy;
			
			if (this.lastwork != this.Generator.work){
				icrafting.sendProgressBarUpdate(this, 1, this.Generator.work);
			}
			
			this.lastwork = this.Generator.work;
		}
	}
	
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int slot, int newValue){
		if(slot==0) this.Generator.energy = newValue;
		if(slot==1) this.Generator.work = newValue;
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int islot){
		return null;
    }
	
	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return this.Generator.isUseableByPlayer(entityplayer);
	}

}
