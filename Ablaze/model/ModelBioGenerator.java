package Ablaze.model;

import org.lwjgl.opengl.GL11;

import Ablaze.tileentity.TileEntityBioGenerator;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelBioGenerator extends ModelBase
{
	  //fields
    ModelRenderer Base;
    ModelRenderer P4;
    ModelRenderer P1;
    ModelRenderer P2;
    ModelRenderer P3;
    ModelRenderer Center;
    ModelRenderer P5;
    ModelRenderer Peak;
    ModelRenderer P6;
    ModelRenderer P7;
    ModelRenderer P8;
  
  public ModelBioGenerator()
  {
      Base = new ModelRenderer(this, 0, 0);
      Base.addBox(-7F, 0F, -7F, 14, 1, 14);
      Base.setRotationPoint(0F, 23F, 0F);
      Base.setTextureSize(64, 32);
      Base.mirror = true;
      setRotation(Base, 0F, 0F, 0F);
      P4 = new ModelRenderer(this, 24, 15);
      P4.addBox(-5F, 0F, -5F, 2, 1, 10);
      P4.setRotationPoint(0.001F, 21.001F, 0.001F);
      P4.setTextureSize(64, 32);
      P4.mirror = true;
      setRotation(P4, 0F, 0F, 0F);
      P1 = new ModelRenderer(this, 24, 15);
      P1.addBox(-1F, 0F, -5F, 2, 1, 10);
      P1.setRotationPoint(4.001F, 21.001F, 0.001F);
      P1.setTextureSize(64, 32);
      P1.mirror = true;
      setRotation(P1, 0F, 0F, 0F);
      P2 = new ModelRenderer(this, 24, 15);
      P2.addBox(-1F, 0F, -5F, 2, 1, 10);
      P2.setRotationPoint(0F, 21F, -4F);
      P2.setTextureSize(64, 32);
      P2.mirror = true;
      setRotation(P2, 0F, 1.570796F, 0F);
      P3 = new ModelRenderer(this, 24, 15);
      P3.addBox(0F, 0F, 0F, 2, 1, 10);
      P3.setRotationPoint(-5F, 21F, 5F);
      P3.setTextureSize(64, 32);
      P3.mirror = true;
      setRotation(P3, 0F, 1.570796F, 0F);
      Center = new ModelRenderer(this, 0, 15);
      Center.addBox(-2F, 0F, -2F, 4, 6, 4);
      Center.setRotationPoint(0F, 17F, 0F);
      Center.setTextureSize(64, 32);
      Center.mirror = true;
      setRotation(Center, 0F, 0F, 0F);
      P5 = new ModelRenderer(this, 24, 15);
      P5.addBox(3F, 0F, 0F, 2, 1, 10);
      P5.setRotationPoint(0.001F, 18.001F, -5.001F);
      P5.setTextureSize(64, 32);
      P5.mirror = true;
      setRotation(P5, 0F, 0F, 0F);
      Peak = new ModelRenderer(this, 16, 15);
      Peak.addBox(-1F, -1F, -1F, 2, 2, 2);
      Peak.setRotationPoint(0F, 17.6F, 0F);
      Peak.setTextureSize(64, 32);
      Peak.mirror = true;
      setRotation(Peak, 0.8028515F, 0F, 0.6283185F);
      P6 = new ModelRenderer(this, 24, 15);
      P6.addBox(0F, 0F, -5F, 2, 1, 10);
      P6.setRotationPoint(0F, 18F, -3F);
      P6.setTextureSize(64, 32);
      P6.mirror = true;
      setRotation(P6, 0F, 1.570796F, 0F);
      P7 = new ModelRenderer(this, 24, 15);
      P7.addBox(0F, 0F, -5F, 2, 1, 10);
      P7.setRotationPoint(-5.001F, 18.001F, 0.001F);
      P7.setTextureSize(64, 32);
      P7.mirror = true;
      setRotation(P7, 0F, 0F, 0F);
      P8 = new ModelRenderer(this, 24, 15);
      P8.addBox(0F, 0F, -5F, 2, 1, 10);
      P8.setRotationPoint(0F, 18F, 5F);
      P8.setTextureSize(64, 32);
      P8.mirror = true;
      setRotation(P8, 0F, 1.570796F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
	  super.render(entity, f, f1, f2, f3, f4, f5);
	    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
	    Base.render(f5);
	    P4.render(f5);
	    P1.render(f5);
	    P2.render(f5);
	    P3.render(f5);
	    Center.render(f5);
	    P5.render(f5);
	    Peak.render(f5);
	    P6.render(f5);
	    P7.render(f5);
	    P8.render(f5);
  }
  
  public void renderModel(float animation, float f5){
	    Base.render(f5);
	   
	    Center.render(f5);
	    GL11.glPushMatrix();
		GL11.glRotatef(animation, 0, 1, 0);
		 P1.render(f5);
		 P2.render(f5);
		 P3.render(f5);
		 P4.render(f5);
		 P5.render(f5);
		 P6.render(f5);
		 P7.render(f5);
		 P8.render(f5);
		GL11.glPopMatrix();
		Peak.render(f5);
	    
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
  }

}
