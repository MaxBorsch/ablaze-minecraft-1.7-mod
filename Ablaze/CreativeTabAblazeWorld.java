package Ablaze;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class CreativeTabAblazeWorld extends CreativeTabs {

	public CreativeTabAblazeWorld(int id, String label) {
		super(id, label);
	}
	
	@Override
	public String getTranslatedTabLabel(){
		return "Ablaze World";
	}
	
	@Override
	public Item getTabIconItem()
	{
		return Init.itemMoss;
	}
}
