package Ablaze.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import Ablaze.Init;
import Ablaze.container.ContainerWeldingPanel;
import Ablaze.tileentity.TileEntityWeldingPanel;

public class GuiWeldingPanel extends GuiContainer {

	public TileEntityWeldingPanel WeldingPanel;
	public static final ResourceLocation Bkg = new ResourceLocation(Init.modid, "textures/gui/weldingpanel.png");
	
	public GuiWeldingPanel(InventoryPlayer inventory, TileEntityWeldingPanel entity) {
		super(new ContainerWeldingPanel(inventory, entity));
		
		this.WeldingPanel = entity;
		this.xSize = 176;
		this.ySize = 166;
	}

	public void drawGuiContainerForegroundLayer(int par1, int par2){
		this.fontRendererObj.drawString("Soldering Panel", this.xSize - 12 - this.fontRendererObj.getStringWidth("Soldering Panel"), 6, 4210752);
	}
	
	public void drawGuiContainerBackgroundLayer(float f, int i, int j) {
		GL11.glColor4f(1F, 1F, 1F, 1F);
		
		Minecraft.getMinecraft().getTextureManager().bindTexture(Bkg);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
	
		int k2 = WeldingPanel.getCookProgressScaled(45);
		drawTexturedModalRect(guiLeft+84, guiTop+18, 176, 0, k2, 45);
	
	}

}
