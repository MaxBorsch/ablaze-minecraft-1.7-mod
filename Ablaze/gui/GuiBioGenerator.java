package Ablaze.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import Ablaze.Init;
import Ablaze.container.ContainerGenerator;
import Ablaze.tileentity.TileEntityBioGenerator;

public class GuiBioGenerator extends GuiContainer {

	public TileEntityBioGenerator BioGenerator;
	public static final ResourceLocation Bkg = new ResourceLocation(Init.modid, "textures/gui/generator.png");
	
	public GuiBioGenerator(InventoryPlayer inventory, TileEntityBioGenerator entity) {
		super(new ContainerGenerator(inventory, entity));
		
		this.BioGenerator = entity;
		this.xSize = 176;
		this.ySize = 166;
	}

	public void drawGuiContainerForegroundLayer(int par1, int par2){
		String name = this.BioGenerator.getInventoryName();
		this.fontRendererObj.drawString(name, this.xSize/2 - this.fontRendererObj.getStringWidth(name)/2, 9, 4210752);
		
		String string = this.BioGenerator.getState() == 1 ? "Generating" : "Not Generating";
		this.fontRendererObj.drawString(string, this.xSize/2 - this.fontRendererObj.getStringWidth(string)/2, 60, 4210752);
		
	}
	
	public void drawGuiContainerBackgroundLayer(float f, int i, int j) {
		GL11.glColor4f(1F, 1F, 1F, 1F);
		
		Minecraft.getMinecraft().getTextureManager().bindTexture(Bkg);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
		
		int k2 = this.BioGenerator.getEnergyScaled(42);
		drawTexturedModalRect(guiLeft + 164, guiTop + 18, 176, 0, 5, 42 - k2);
	
	}

}
