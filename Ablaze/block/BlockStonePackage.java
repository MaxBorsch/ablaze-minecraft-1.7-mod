package Ablaze.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.monster.EntitySilverfish;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.world.World;
import Ablaze.Init;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockStonePackage extends Block
{
	
    public BlockStonePackage()
    {
        super(Material.rock);
        this.setHardness(10F);
        this.setResistance(20F);
        this.setStepSound(soundTypeStone);
    }

	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
	    this.blockIcon = par1IconRegister.registerIcon(Init.modid + ":" + (this.getUnlocalizedName().substring(5)));

	}
	
	@Override
    public void breakBlock(World world, int x, int y, int z, Block var3, int var4){
		super.breakBlock(world,  x,  y,  z,  var3,  var4);
		
		if (new Random().nextInt(6)==1){
			world.setBlock(x, y, z, Init.blockWoodPackage);
		}
		if (new Random().nextInt(6)==1){
			EntitySilverfish sf = new EntitySilverfish(world);
			sf.setPosition(x, y+1, z);
			world.spawnEntityInWorld(sf);
		}
	}
	
    /**
     * Returns the quantity of items to drop on block destruction.
     */
    public int quantityDropped(Random par1Random)
    {
        return 6;
    }
    
    @Override
	public Item getItemDropped(int par1, Random r, int par2){
    	Item[] itemDrops = new Item[]{Init.itemXeniteIngot, Items.apple, Items.arrow, Items.iron_ingot, Items.blaze_powder, Items.bone};
    	int r1 = r.nextInt(6);
    	
		return itemDrops[r1];
	}
}
