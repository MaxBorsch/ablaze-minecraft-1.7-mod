package Ablaze.tileentity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import Ablaze.Init;

public class TileEntityWeldingPanel extends TileEntity implements ISidedInventory {

	private String localizedName;
	private ItemStack[] slots = new ItemStack[17];
	
	public int furnaceSpeed = 700;
	public int currentItemBurnTime = 0; //The currently burning items progress
	
	public int[][] recipes = new int[12][17];
	
	public int CookProgress = 0;
	
	public int rotation = 0;
	
	public int getSizeInventory(){
		return this.slots.length;
	}
	
	public String getInvName(){
		return this.isInvNameLocalized() ? this.localizedName : "container.WeldingPanel";
	}
	
	public boolean isInvNameLocalized(){
		return this.localizedName!=null && this.localizedName.length()>0;
	}
	
	public void setGuiDisplayName(String displayName) {
		this.localizedName = displayName;
	}

	@Override
	public ItemStack getStackInSlot(int i) {
		return this.slots[i];
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt){
		super.readFromNBT(nbt);
		
		NBTTagList list = nbt.getTagList("Items", 10);
		this.slots = new ItemStack[this.getSizeInventory()];
		
		for (int i=0; i<list.tagCount(); i++){
			NBTTagCompound compound = (NBTTagCompound) list.getCompoundTagAt(i);
			byte b = compound.getByte("Slot");
			
			if (b>=0 && b<this.slots.length){
				this.slots[b] = ItemStack.loadItemStackFromNBT(compound);
			}
		}
		
		this.currentItemBurnTime = nbt.getShort("currentItemBurnTime");
		
		if (nbt.hasKey("Name")){
			this.localizedName = nbt.getString("Name");
		}
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt){
		super.writeToNBT(nbt);
		
		nbt.setShort("currentItemBurnTime", (short) currentItemBurnTime);
		
		NBTTagList list = new NBTTagList();
		
		for (int i=0; i<this.slots.length; i++){
			if (this.slots[i]!=null){
				NBTTagCompound compound = new NBTTagCompound();
				compound.setByte("Slot", (byte) i);
				this.slots[i].writeToNBT(compound);
				list.appendTag(compound);
			}
		}
		nbt.setTag("Items", list);
		
		if (this.isInvNameLocalized()){
			nbt.setString("Name", this.localizedName);
		}
	}
	
	@Override
	public ItemStack decrStackSize(int i, int j) {
		if (this.slots[i]!=null){
			ItemStack itemStack;
			
			if (this.slots[i].stackSize<=j){
				itemStack = this.slots[i];
				this.slots[i]=null;
				return itemStack;
			}else{
				itemStack = this.slots[i].splitStack(j);
				if (this.slots[i].stackSize==0){
					this.slots[i]=null;
				}
				return itemStack;
			}
		}
		return null;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int i) {
		if (this.slots[i]!=null){
			ItemStack itemStack;
			itemStack = this.slots[i];
			this.slots[i]=null;
			return itemStack;
		}
		return null;
	}

	@Override
	public void setInventorySlotContents(int i, ItemStack itemstack) {
		this.slots[i] = itemstack;
		if (itemstack!=null && itemstack.stackSize>this.getInventoryStackLimit()){
			itemstack.stackSize = this.getInventoryStackLimit();
		}
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer) {
		return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord)!=this ? false : entityplayer.getDistanceSq((double) this.xCoord + 0.5D, (double) this.yCoord + 0.5D, (double) this.zCoord + 0.5D)<=100;
	}

	public void updateEntity(){
		boolean invChanged = false;
		
		if (!(this.worldObj).isRemote){
			int smelt = canSmelt();
			if (this.slots[16]==null && smelt!=-1){
				this.currentItemBurnTime++;
				if (this.currentItemBurnTime>=this.furnaceSpeed){
					smeltItem(smelt);
					this.currentItemBurnTime = 0;
				}
			}else{
				this.currentItemBurnTime = 0;
			}
		}
		
		
		this.rotation = this.worldObj.getBlockMetadata(this.xCoord, this.yCoord, this.zCoord);
		
	}

	public void clearRecipies(int[] ar){
		for (int i=0; i<16; i++){
			ar[i] = -1;
		}
	}
	
	private int canSmelt() {
		
		clearRecipies(this.recipes[0]);
		this.recipes[0][16] = Item.getIdFromItem(Init.itemGolemRightArm);
		this.recipes[0][0] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[0][1] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[0][2] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[0][7] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[0][8] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[0][11] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[0][13] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[0][15] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[0][9] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[0][4] = Item.getIdFromItem(Init.itemMultiConnector);
		this.recipes[0][5] = Item.getIdFromItem(Init.itemWiring);
		this.recipes[0][6] = Item.getIdFromItem(Init.itemServoMotor);
		this.recipes[0][10] = Item.getIdFromItem(Init.itemGearbox);
		this.recipes[0][14] = Item.getIdFromItem(Init.itemBronzeSlice);
		clearRecipies(this.recipes[1]);
		this.recipes[1][16] = Item.getIdFromItem(Init.itemGolemLeftArm);
		this.recipes[1][1] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[1][2] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[1][3] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[1][4] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[1][8] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[1][12] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[1][10] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[1][11] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[1][14] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[1][7] = Item.getIdFromItem(Init.itemMultiConnector);
		this.recipes[1][6] = Item.getIdFromItem(Init.itemWiring);
		this.recipes[1][5] = Item.getIdFromItem(Init.itemServoMotor);
		this.recipes[1][9] = Item.getIdFromItem(Init.itemGearbox);
		this.recipes[1][13] = Item.getIdFromItem(Init.itemBronzeSlice);
		clearRecipies(this.recipes[2]);
		this.recipes[2][16] = Item.getIdFromItem(Init.itemGearbox);
		this.recipes[2][1] = Item.getIdFromItem(Init.itemBronzeSlice);
		this.recipes[2][2] = Item.getIdFromItem(Init.itemBronzeSlice);
		this.recipes[2][7] = Item.getIdFromItem(Init.itemBronzeSlice);
		this.recipes[2][11] = Item.getIdFromItem(Init.itemBronzeSlice);
		this.recipes[2][13] = Item.getIdFromItem(Init.itemBronzeSlice);
		this.recipes[2][14] = Item.getIdFromItem(Init.itemBronzeSlice);
		this.recipes[2][4] = Item.getIdFromItem(Init.itemBronzeSlice);
		this.recipes[2][8] = Item.getIdFromItem(Init.itemBronzeSlice);
		this.recipes[2][12] = Item.getIdFromItem(Init.itemSmallGear);
		this.recipes[2][9] = Item.getIdFromItem(Init.itemLargeGear);
		this.recipes[2][10] = Item.getIdFromItem(Init.itemLargeGear);
		this.recipes[2][5] = Item.getIdFromItem(Init.itemLargeGear);
		this.recipes[2][6] = Item.getIdFromItem(Init.itemLargeGear);
		clearRecipies(this.recipes[3]);
		this.recipes[3][16] = Item.getIdFromItem(Init.itemGolemRightLeg);
		this.recipes[3][0] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[3][2] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[3][4] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[3][6] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[3][8] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[3][10] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[3][12] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[3][15] = Item.getIdFromItem(Init.itemBronzeSlice);
		this.recipes[3][14] = Item.getIdFromItem(Init.itemGearbox);
		this.recipes[3][13] = Item.getIdFromItem(Init.itemServoMotor);
		this.recipes[3][9] = Item.getIdFromItem(Init.itemWiring);
		this.recipes[3][5] = Item.getIdFromItem(Init.itemWiring);
		this.recipes[3][1] = Item.getIdFromItem(Init.itemMultiConnector);
		clearRecipies(this.recipes[4]);
		this.recipes[4][16] = Item.getIdFromItem(Init.itemGolemLeftLeg);
		this.recipes[4][1] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[4][3] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[4][5] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[4][7] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[4][9] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[4][11] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[4][15] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[4][12] = Item.getIdFromItem(Init.itemBronzeSlice);
		this.recipes[4][13] = Item.getIdFromItem(Init.itemGearbox);
		this.recipes[4][14] = Item.getIdFromItem(Init.itemServoMotor);
		this.recipes[4][10] = Item.getIdFromItem(Init.itemWiring);
		this.recipes[4][6] = Item.getIdFromItem(Init.itemWiring);
		this.recipes[4][2] = Item.getIdFromItem(Init.itemMultiConnector);
		clearRecipies(this.recipes[5]);
		this.recipes[5][16] = Item.getIdFromItem(Init.itemThermoReactor);
		this.recipes[5][1] = Item.getIdFromItem(Init.itemHeatVent);
		this.recipes[5][2] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[5][4] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[5][7] = Item.getIdFromItem(Init.itemHeatVent);
		this.recipes[5][8] = Item.getIdFromItem(Init.itemHeatVent);
		this.recipes[5][11] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[5][13] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[5][14] = Item.getIdFromItem(Init.itemHeatVent);
		this.recipes[5][5] = Item.getIdFromItem(Init.itemGearbox);
		this.recipes[5][6] = Item.getIdFromItem(Init.itemGearbox);
		this.recipes[5][9] = Item.getIdFromItem(Items.coal);
		this.recipes[5][10] = Item.getIdFromItem(Items.coal);
		this.recipes[5][0] = Item.getIdFromItem(Init.itemHyperconductiveWiring);
		this.recipes[5][3] = Item.getIdFromItem(Init.itemHyperconductiveWiring);
		this.recipes[5][12] = Item.getIdFromItem(Init.itemHyperconductiveWiring);
		this.recipes[5][15] = Item.getIdFromItem(Init.itemHyperconductiveWiring);
		clearRecipies(this.recipes[6]);
		this.recipes[6][16] = Item.getIdFromItem(Init.itemGolemTorso);
		this.recipes[6][0] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[6][3] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[6][12] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[6][15] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[6][1] = Item.getIdFromItem(Init.itemMultiConnector);
		this.recipes[6][2] = Item.getIdFromItem(Init.itemMultiConnector);
		this.recipes[6][4] = Item.getIdFromItem(Init.itemMultiConnector);
		this.recipes[6][7] = Item.getIdFromItem(Init.itemMultiConnector);
		this.recipes[6][8] = Item.getIdFromItem(Init.itemMultiConnector);
		this.recipes[6][11] = Item.getIdFromItem(Init.itemMultiConnector);
		this.recipes[6][13] = Item.getIdFromItem(Init.itemMultiConnector);
		this.recipes[6][14] = Item.getIdFromItem(Init.itemMultiConnector);
		this.recipes[6][5] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[6][6] = Item.getIdFromItem(Init.itemThermoReactor);
		this.recipes[6][9] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[6][10] = Item.getIdFromItem(Init.itemGearbox);
		clearRecipies(this.recipes[7]);
		this.recipes[7][16] = Item.getIdFromItem(Init.itemGolemHead);
		this.recipes[7][1] = Item.getIdFromItem(Init.itemT1ProcessingUnit);
		this.recipes[7][0] = Item.getIdFromItem(Init.itemFlourescentCube);
		this.recipes[7][2] = Item.getIdFromItem(Init.itemFlourescentCube);
		this.recipes[7][5] = Item.getIdFromItem(Init.itemT1LogicUnit);
		clearRecipies(this.recipes[8]);
		this.recipes[8][16] = Item.getIdFromItem(Init.itemT1ProcessingUnit);
		this.recipes[8][1] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[8][2] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[8][4] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[8][7] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[8][5] = Item.getIdFromItem(Init.itemT1Processor);
		this.recipes[8][6] = Item.getIdFromItem(Init.itemThinGoldWiring);
		clearRecipies(this.recipes[9]);
		this.recipes[9][16] = Item.getIdFromItem(Init.itemT1LogicUnit);
		this.recipes[9][8] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[9][11] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[9][13] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[9][14] = Item.getIdFromItem(Init.itemBronzeIngot);
		this.recipes[9][10] = Item.getIdFromItem(Init.itemT1LogicCard);
		this.recipes[9][9] = Item.getIdFromItem(Init.itemThinGoldWiring);
		clearRecipies(this.recipes[10]);
		this.recipes[10][16] = Item.getIdFromItem(Init.itemT1Processor);
		this.recipes[10][0] = Item.getIdFromItem(Init.itemThinWiring);
		this.recipes[10][1] = Item.getIdFromItem(Init.itemThinWiring);
		this.recipes[10][2] = Item.getIdFromItem(Init.itemThinWiring);
		this.recipes[10][3] = Item.getIdFromItem(Init.itemThinWiring);
		this.recipes[10][4] = Item.getIdFromItem(Init.itemThinWiring);
		this.recipes[10][7] = Item.getIdFromItem(Init.itemThinWiring);
		this.recipes[10][8] = Item.getIdFromItem(Init.itemThinWiring);
		this.recipes[10][11] = Item.getIdFromItem(Init.itemThinWiring);
		this.recipes[10][12] = Item.getIdFromItem(Init.itemThinWiring);
		this.recipes[10][13] = Item.getIdFromItem(Init.itemThinWiring);
		this.recipes[10][14] = Item.getIdFromItem(Init.itemThinWiring);
		this.recipes[10][15] = Item.getIdFromItem(Init.itemThinWiring);
		this.recipes[10][5] = Item.getIdFromItem(Init.itemGearbox);
		this.recipes[10][10] = Item.getIdFromItem(Init.itemGearbox);
		this.recipes[10][6] = Item.getIdFromItem(Init.itemThinGoldWiring);
		this.recipes[10][9] = Item.getIdFromItem(Init.itemThinGoldWiring);
		clearRecipies(this.recipes[11]);
		this.recipes[11][16] = Item.getIdFromItem(Init.itemT1LogicCard);
		this.recipes[11][4] = Item.getIdFromItem(Init.itemBronzeSlice);
		this.recipes[11][7] = Item.getIdFromItem(Init.itemBronzeSlice);
		this.recipes[11][8] = Item.getIdFromItem(Init.itemBronzeSlice);
		this.recipes[11][11] = Item.getIdFromItem(Init.itemBronzeSlice);
		this.recipes[11][5] = Item.getIdFromItem(Init.itemGearbox);
		this.recipes[11][10] = Item.getIdFromItem(Init.itemGearbox);
		this.recipes[11][6] = Item.getIdFromItem(Init.itemThinGoldWiring);
		this.recipes[11][9] = Item.getIdFromItem(Init.itemThinGoldWiring);
		
		for (int i=0; i<this.recipes.length; i++){
			int count = 0;
			for (int s=0; s<16; s++){
				if ((this.recipes[i][s]==-1 && this.slots[s]==null) || (this.slots[s]!=null && Item.getIdFromItem(this.slots[s].getItem()) == this.recipes[i][s])){
					count++;
				}
			}
			if (count==16){
				return this.recipes[i][16];
			}
		}
		
		return -1;
	}

	private void smeltItem(int it) {
		for (int i=0; i<16; i++){
			if (this.slots[i]!=null){
				this.slots[i].stackSize--;
				if (this.slots[i].stackSize<=0){
					this.slots[i] = null;
				}
			}
		}
		
		this.slots[16] = getItemStackFromId(it);
	}

	private ItemStack getItemStackFromId(int it) {
		return new ItemStack(Item.getItemById(it), 1);
	}

	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack) {
		return false;
	}

	@Override
	public boolean canInsertItem(int i, ItemStack itemstack, int j) {
		return false;
	}

	@Override
	public boolean canExtractItem(int i, ItemStack itemstack, int j) {
		return false;
	}

	public int getCookProgressScaled(int i) {
		int One = furnaceSpeed/i;
		
		if (currentItemBurnTime<=0){
			CookProgress = 0;
		}
		if (currentItemBurnTime>0 && currentItemBurnTime<=One){
			CookProgress = 1;
		}
		if (currentItemBurnTime>One && currentItemBurnTime<=One*2){
			CookProgress = 2;
		}
		if (currentItemBurnTime>One*2 && currentItemBurnTime<=One*3){
			CookProgress = 3;
		}
		if (currentItemBurnTime>One*3 && currentItemBurnTime<=One*4){
			CookProgress = 4;
		}
		if (currentItemBurnTime>One*4 && currentItemBurnTime<=One*5){
			CookProgress = 5;
		}
		if (currentItemBurnTime>One*5 && currentItemBurnTime<=One*6){
			CookProgress = 6;
		}
		if (currentItemBurnTime>One*6 && currentItemBurnTime<=One*7){
			CookProgress = 7;
		}
		if (currentItemBurnTime>One*7 && currentItemBurnTime<=One*8){
			CookProgress = 8;
		}
		if (currentItemBurnTime>One*8 && currentItemBurnTime<=One*9){
			CookProgress = 9;
		}
		if (currentItemBurnTime>One*9 && currentItemBurnTime<=One*10){
			CookProgress = 10;
		}
		if (currentItemBurnTime>One*10 && currentItemBurnTime<=One*11){
			CookProgress = 11;
		}
		if (currentItemBurnTime>One*11 && currentItemBurnTime<=One*12){
			CookProgress = 12;
		}
		if (currentItemBurnTime>One*12 && currentItemBurnTime<=One*13){
			CookProgress = 13;
		}
		if (currentItemBurnTime>One*13 && currentItemBurnTime<=One*14){
			CookProgress = 14;
		}
		if (currentItemBurnTime>One*14 && currentItemBurnTime<=One*15){
			CookProgress = 15;
		}
		if (currentItemBurnTime>One*15 && currentItemBurnTime<=One*16){
			CookProgress = 16;
		}
		if (currentItemBurnTime>One*16 && currentItemBurnTime<=One*17){
			CookProgress = 17;
		}
		if (currentItemBurnTime>One*17 && currentItemBurnTime<=One*18){
			CookProgress = 18;
		}
		if (currentItemBurnTime>One*18 && currentItemBurnTime<=One*19){
			CookProgress = 19;
		}
		if (currentItemBurnTime>One*19 && currentItemBurnTime<=One*20){
			CookProgress = 20;
		}
		if (currentItemBurnTime>One*20 && currentItemBurnTime<=One*21){
			CookProgress = 21;
		}
		if (currentItemBurnTime>One*21 && currentItemBurnTime<=One*22){
			CookProgress = 22;
		}
		if (currentItemBurnTime>One*22 && currentItemBurnTime<=One*23){
			CookProgress = 23;
		}
		if (currentItemBurnTime>One*23 && currentItemBurnTime<=One*24){
			CookProgress = 24;
		}
		if (currentItemBurnTime>One*24 && currentItemBurnTime<=One*25){
			CookProgress = 25;
		}
		if (currentItemBurnTime>One*25 && currentItemBurnTime<=One*26){
			CookProgress = 26;
		}
		if (currentItemBurnTime>One*26 && currentItemBurnTime<=One*27){
			CookProgress = 27;
		}
		if (currentItemBurnTime>One*27 && currentItemBurnTime<=One*28){
			CookProgress = 28;
		}
		if (currentItemBurnTime>One*28 && currentItemBurnTime<=One*29){
			CookProgress = 29;
		}
		if (currentItemBurnTime>One*29 && currentItemBurnTime<=One*30){
			CookProgress = 30;
		}
		if (currentItemBurnTime>One*30 && currentItemBurnTime<=One*31){
			CookProgress = 31;
		}
		if (currentItemBurnTime>One*31 && currentItemBurnTime<=One*32){
			CookProgress = 32;
		}
		if (currentItemBurnTime>One*32 && currentItemBurnTime<=One*33){
			CookProgress = 33;
		}
		if (currentItemBurnTime>One*33 && currentItemBurnTime<=One*34){
			CookProgress = 34;
		}
		if (currentItemBurnTime>One*34 && currentItemBurnTime<=One*35){
			CookProgress = 35;
		}
		if (currentItemBurnTime>One*35 && currentItemBurnTime<=One*36){
			CookProgress = 36;
		}
		if (currentItemBurnTime>One*36 && currentItemBurnTime<=One*37){
			CookProgress = 37;
		}
		if (currentItemBurnTime>One*37 && currentItemBurnTime<=One*38){
			CookProgress = 38;
		}
		if (currentItemBurnTime>One*38 && currentItemBurnTime<=One*39){
			CookProgress = 39;
		}
		if (currentItemBurnTime>One*39 && currentItemBurnTime<=One*40){
			CookProgress = 40;
		}
		if (currentItemBurnTime>One*40 && currentItemBurnTime<=One*41){
			CookProgress = 41;
		}
		if (currentItemBurnTime>One*41 && currentItemBurnTime<=One*42){
			CookProgress = 42;
		}
		if (currentItemBurnTime>One*42 && currentItemBurnTime<=One*43){
			CookProgress = 43;
		}
		if (currentItemBurnTime>One*43 && currentItemBurnTime<=One*44){
			CookProgress = 44;
		}
		if (currentItemBurnTime>One*44 && currentItemBurnTime<=One*45){
			CookProgress = 45;
		}
		return CookProgress;
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int var1) {
		return null;
	}

	@Override
	public String getInventoryName() {
		return "Soldering Panel";
	}

	@Override
	public boolean hasCustomInventoryName() {
		return true;
	}

	@Override
	public void openInventory() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void closeInventory() {
		// TODO Auto-generated method stub
		
	}

}