package Ablaze.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import Ablaze.Init;
import Ablaze.container.ContainerAutomatedSpawner;
import Ablaze.container.ContainerBacteriaFarm;
import Ablaze.container.ContainerConcentrator;
import Ablaze.container.ContainerEnergyBlock;
import Ablaze.container.ContainerExtractor;
import Ablaze.container.ContainerGenerator;
import Ablaze.container.ContainerGlowGenerator;
import Ablaze.container.ContainerInfuser;
import Ablaze.container.ContainerRefinery;
import Ablaze.container.ContainerResearchDesk;
import Ablaze.container.ContainerScrappingTable;
import Ablaze.container.ContainerTinkerTable;
import Ablaze.container.ContainerWeldingPanel;
import Ablaze.container.ContainerWritingDesk;
import Ablaze.tileentity.TileEntityAutomatedSpawner;
import Ablaze.tileentity.TileEntityBacteriaFarm;
import Ablaze.tileentity.TileEntityBioGenerator;
import Ablaze.tileentity.TileEntityConcentrator;
import Ablaze.tileentity.TileEntityEnergyBlock;
import Ablaze.tileentity.TileEntityExtractor;
import Ablaze.tileentity.TileEntityGlowGenerator;
import Ablaze.tileentity.TileEntityInfuser;
import Ablaze.tileentity.TileEntityRefinery;
import Ablaze.tileentity.TileEntityResearchDesk;
import Ablaze.tileentity.TileEntityScrappingTable;
import Ablaze.tileentity.TileEntityTinkerTable;
import Ablaze.tileentity.TileEntityWeldingPanel;
import Ablaze.tileentity.TileEntityWritingDesk;
import cpw.mods.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler {

	public GuiHandler(){
	}
	
	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity entity = world.getTileEntity(x, y, z);
		if (entity != null){
			switch(ID){
			case Init.guiIdRefinery:
				if (entity instanceof TileEntityRefinery){
					return new ContainerRefinery(player.inventory, (TileEntityRefinery) entity);
				}
			case Init.guiIdConcentrator:
				if (entity instanceof TileEntityConcentrator){
					return new ContainerConcentrator(player.inventory, (TileEntityConcentrator) entity);
				}
			case Init.guiIdExtractor:
				if (entity instanceof TileEntityExtractor){
					return new ContainerExtractor(player.inventory, (TileEntityExtractor) entity);
				}
			case Init.guiIdInfuser:
				if (entity instanceof TileEntityInfuser){
					return new ContainerInfuser(player.inventory, (TileEntityInfuser) entity);
				}
			case Init.guiIdEnergyBlock:
				if (entity instanceof TileEntityEnergyBlock){
					return new ContainerEnergyBlock(player.inventory, (TileEntityEnergyBlock) entity);
				}
			case Init.guiIdBioGenerator:
				if (entity instanceof TileEntityBioGenerator){
					return new ContainerGenerator(player.inventory, (TileEntityBioGenerator) entity);
				}
			case Init.guiIdGlowGenerator:
				if (entity instanceof TileEntityGlowGenerator){
					return new ContainerGlowGenerator(player.inventory, (TileEntityGlowGenerator) entity);
				}
			case Init.guiIdTinkerTable:
				if (entity instanceof TileEntityTinkerTable){
					return new ContainerTinkerTable(player.inventory, (TileEntityTinkerTable) entity);
				}
			case Init.guiIdScrappingTable:
				if (entity instanceof TileEntityScrappingTable){
					return new ContainerScrappingTable(player.inventory, (TileEntityScrappingTable) entity);
				}
			case Init.guiIdWritingDesk:
				if (entity instanceof TileEntityWritingDesk){
					return new ContainerWritingDesk(player.inventory, (TileEntityWritingDesk) entity);
				}
			case Init.guiIdWeldingPanel:
				if (entity instanceof TileEntityWeldingPanel){
					return new ContainerWeldingPanel(player.inventory, (TileEntityWeldingPanel) entity);
				}
			case Init.guiIdResearchDesk:
				if (entity instanceof TileEntityResearchDesk){
					return new ContainerResearchDesk(player.inventory, (TileEntityResearchDesk) entity);
				}
			case Init.guiIdAutomatedSpawner:
				if (entity instanceof TileEntityAutomatedSpawner){
					return new ContainerAutomatedSpawner(player.inventory, (TileEntityAutomatedSpawner) entity);
				}
			case Init.guiIdBacteriaFarm:
				if (entity instanceof TileEntityBacteriaFarm){
					return new ContainerBacteriaFarm(player.inventory, (TileEntityBacteriaFarm) entity);
				}
			}
		}
		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity entity = world.getTileEntity(x, y, z);
		if (entity != null){
			switch(ID){

			case Init.guiIdRefinery:
				if (entity instanceof TileEntityRefinery){
					return new GuiRefinery(player.inventory, (TileEntityRefinery) entity);
				}
			case Init.guiIdConcentrator:
				if (entity instanceof TileEntityConcentrator){
					return new GuiConcentrator(player.inventory, (TileEntityConcentrator) entity);
				}
			case Init.guiIdExtractor:
				if (entity instanceof TileEntityExtractor){
					return new GuiExtractor(player.inventory, (TileEntityExtractor) entity);
				}
			case Init.guiIdInfuser:
				if (entity instanceof TileEntityInfuser){
					return new GuiInfuser(player.inventory, (TileEntityInfuser) entity);
				}
			case Init.guiIdEnergyBlock:
				if (entity instanceof TileEntityEnergyBlock){
					return new GuiEnergyBlock(player.inventory, (TileEntityEnergyBlock) entity);
				}
			case Init.guiIdBioGenerator:
				if (entity instanceof TileEntityBioGenerator){
					return new GuiBioGenerator(player.inventory, (TileEntityBioGenerator) entity);
				}
			case Init.guiIdGlowGenerator:
				if (entity instanceof TileEntityGlowGenerator){
					return new GuiGlowGenerator(player.inventory, (TileEntityGlowGenerator) entity);
				}
			case Init.guiIdTinkerTable:
				if (entity instanceof TileEntityTinkerTable){
					return new GuiTinkerTable(player.inventory, (TileEntityTinkerTable) entity);
				}
			case Init.guiIdScrappingTable:
				if (entity instanceof TileEntityScrappingTable){
					return new GuiScrappingTable(player.inventory, (TileEntityScrappingTable) entity);
				}
			case Init.guiIdWritingDesk:
				if (entity instanceof TileEntityWritingDesk){
					return new GuiWritingDesk(player.inventory, (TileEntityWritingDesk) entity);
				}
			case Init.guiIdWeldingPanel:
				if (entity instanceof TileEntityWeldingPanel){
					return new GuiWeldingPanel(player.inventory, (TileEntityWeldingPanel) entity);
				}
			case Init.guiIdResearchDesk:
				if (entity instanceof TileEntityResearchDesk){
					return new GuiResearchDesk(player.inventory, (TileEntityResearchDesk) entity);
				}
			case Init.guiIdAutomatedSpawner:
				if (entity instanceof TileEntityAutomatedSpawner){
					return new GuiAutomatedSpawner(player.inventory, (TileEntityAutomatedSpawner) entity);
				}
			case Init.guiIdBacteriaFarm:
				if (entity instanceof TileEntityBacteriaFarm){
					return new GuiBacteriaFarm(player.inventory, (TileEntityBacteriaFarm) entity);
				}
			}
		}
		return null;
	}

}
