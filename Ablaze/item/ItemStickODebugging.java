package Ablaze.item;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;
import Ablaze.Init;
import Ablaze.entity.EntityMechanicalGolem;
import Ablaze.tileentity.TileEntityEnergyHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemStickODebugging extends Item {

	private int spawnerMode = 0;

    public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10)
    {
		if (!par3World.isRemote){
    	if (!par2EntityPlayer.isSneaking()){
    	if (par3World.getBlock(par4, par5, par6)==Init.blockConduit){
    		TileEntityEnergyHandler te = (TileEntityEnergyHandler) par3World.getTileEntity(par4, par5, par6);
    		if (te!=null){
    			par2EntityPlayer.addChatMessage(new ChatComponentText("Energy: " + te.energy));
    		}
    	}
    	if (par3World.getBlock(par4, par5, par6)==Init.blockScorchedBrick){
    		if (spawnerMode==1){
    		EntityMechanicalGolem golem = new EntityMechanicalGolem(par3World);
    		golem.setPosition(par4, par5+1, par6);
    		par3World.spawnEntityInWorld(golem);
    		}else if (spawnerMode==0){
    		
    		}
    	}
    	}else{
    		if (par3World.getBlock(par4, par5, par6)==Init.blockScorchedBrick){
        	this.spawnerMode++;
        	if (this.spawnerMode>1){
        		this.spawnerMode = 0;
    		}
        	}
        	if (par3World.getBlock(par4, par5, par6)==Init.blockConduit){
        		TileEntityEnergyHandler te = (TileEntityEnergyHandler) par3World.getTileEntity(par4, par5, par6);
        		if (te!=null){
        			par2EntityPlayer.addChatMessage(new ChatComponentText("Requesting: " + te.getRequesting()));
        		}
        	}
    	}
		}
    	return true;
    }
	
	@Override
	public boolean hasEffect(ItemStack par1ItemStack){
		return true;
	}
	
	
	
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister par1IconRegister)
	{
	    this.itemIcon = par1IconRegister.registerIcon(Init.modid + ":" + (this.getUnlocalizedName().substring(5)));

	}
	
}
