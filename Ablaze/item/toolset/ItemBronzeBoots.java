package Ablaze.item.toolset;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import Ablaze.Init;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemBronzeBoots extends ItemArmor {

	public ItemBronzeBoots(ArmorMaterial par2EnumArmorMaterial, int par3, int par4) {
		super(par2EnumArmorMaterial, par3, par4);
	}

	public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
	{
	if(stack.getItem() == Init.itemBronzeHelmet || stack.getItem() == Init.itemBronzePlate || stack.getItem() == Init.itemBronzeBoots)
	{
	return Init.modid + ":textures/models/armor/bronze_layer_1.png";
	}
	if(stack.getItem() == Init.itemBronzeLegs)
	{
	return Init.modid + ":textures/models/armor/bronze_layer_2.png";
	}
	else return null;
	}
	
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister par1IconRegister)
	{
	    this.itemIcon = par1IconRegister.registerIcon(Init.modid + ":" + (this.getUnlocalizedName().substring(5)));

	}
	
}
