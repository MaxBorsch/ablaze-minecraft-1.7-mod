package Ablaze.block;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import Ablaze.Init;
import Ablaze.tileentity.TileEntityWeldingFrame;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockWeldingFrame extends BlockContainer
{
    public BlockWeldingFrame()
    {
        super(Material.rock);
        this.setBlockBounds(0F, 0.0F, 0F, 1F, 0.5F, 1F);
    }

	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
	    this.blockIcon = par1IconRegister.registerIcon(Init.modid + ":" + "stone");

	}

	@Override
	public TileEntity createNewTileEntity(World world, int var2) {
		return new TileEntityWeldingFrame();
	}
	
	public boolean isOpaqueCube(){
		return false;
	}
	
	public boolean renderAsNormalBlock(){
		return false;
	}
	
    public int getRenderType()
    {
        return -1;
    }
    
}
