package Ablaze.worldgen;
 
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;
import Ablaze.Init;
 
public class WorldGenAcid extends WorldGenerator
{
   public WorldGenAcid()
   {
     
   }
   
   public boolean generate(World world, Random rand, int x, int y, int z)
   {
	      if(world.getBlock(x, y, z)== Blocks.water || world.getBlock(x, y, z)== Blocks.ice || world.getBlock(x, y, z)== Blocks.leaves || world.getBlock(x, y, z)== Blocks.sand || world.getBlock(x, y + 1, z)!= Blocks.air || world.getBlock(x, y + 1, z)== Blocks.log)
	      {
	         return false;
	      }
	      
                Block block = Init.blockAcidFluid;
                
                world.setBlock(x, y, z, block);
                world.setBlock(x, y, z+1, block);
                world.setBlock(x, y, z-1, block);
                world.setBlock(x+1, y, z, block);
                world.setBlock(x-1, y, z, block);
                world.setBlock(x+1, y, z+1, block);
                world.setBlock(x+1, y, z-1, block);   
                world.setBlock(x-1, y, z+1, block);
                world.setBlock(x-1, y, z-1, block);  
                world.setBlock(x-2, y, z-1, block);  
                world.setBlock(x-3, y, z-1, block);  
                world.setBlock(x-1, y, z-2, block);  
                for (int i=1; i<5; i++){
                	world.setBlock(x+rand.nextInt(6), y, z+rand.nextInt(6), block);  
                }

                return true;
   }

}