package Ablaze.tileentity;

import net.minecraft.block.Block;
import net.minecraft.block.BlockBush;
import net.minecraft.init.Blocks;

public class TileEntityBioGenerator extends TileEntityGenerator {

	private float animation = 0;
	
	private static final int RANGE = 10;
	
	@Override
	public String getInventoryName() {
		return "Bio Generator";
	}
	
	@Override
	public boolean canGenerate() {
		if (hasNearbyPlant()) {
			this.animation += 1 + getSpeedBonus() * 0.5f;
			this.worldObj.addBlockEvent(this.xCoord, this.yCoord, this.zCoord, this.getBlockType(), 3, (int) this.animation * 10);
			return true;
		}
		
		return false;
	}
	
	@Override
	public int Generate() {
		int range = getRange();
		
		for (int z=0; z < range; z++){
			for (int x=0; x < range; x++){
				if (isPlant(this.worldObj.getBlock(this.xCoord - range/2 + x, this.yCoord, this.zCoord - range/2 + z))) {
					Die(this.xCoord - range/2 + x, this.yCoord, this.zCoord - range/2 + z);
					
					return 1 + getEfficiencyBonus();
				}
			}
		}
		
		return 0;
	}
	
	public boolean hasNearbyPlant() {
		int range = getRange();
		
		for (int z=0; z < range; z++){
			for (int x=0; x < range; x++){
				if (isPlant(this.worldObj.getBlock(this.xCoord - range/2 + x, this.yCoord, this.zCoord - range/2 + z))) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	@Override
    public boolean receiveClientEvent(int eventId, int eventData)
    {
        if (eventId == 3)
        {
            this.animation = (float) eventData / 10f;
            return true;
            
        }
        else 
        {
            return super.receiveClientEvent(eventId, eventData);
        }
    }
	
	public boolean isPlant(Block block){
		if (block instanceof BlockBush && block != Blocks.deadbush){
			return true;
		}
		
		return false;
	}
	
	private void Die(int x, int y, int z){
		this.worldObj.setBlock(x, y, z, Blocks.deadbush);
	}
	
	public int getRange() {
		return RANGE + (getRangeBonus() * 6);
	}
	
	public float getAnimation() {
		return this.animation;
	}
	
}
