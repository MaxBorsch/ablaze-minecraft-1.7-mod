package Ablaze.renderer;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import Ablaze.model.ModelIncendiaryModule;
import Ablaze.tileentity.TileEntityIncendiaryModule;

public class ItemRendererIncendiaryModule implements IItemRenderer {

	private ModelIncendiaryModule model;
	private TileEntitySpecialRenderer render;
	
	public ItemRendererIncendiaryModule(TileEntitySpecialRenderer renderer){
		this.model = new ModelIncendiaryModule();
		this.render = renderer;
	}
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		this.render.renderTileEntityAt(new TileEntityIncendiaryModule(), 0.0D, 0.0D, 0.0D, 0.0F);
	}

}
