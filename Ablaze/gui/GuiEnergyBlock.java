package Ablaze.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import Ablaze.Init;
import Ablaze.container.ContainerEnergyBlock;
import Ablaze.tileentity.TileEntityEnergyBlock;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class GuiEnergyBlock extends GuiContainer {

	public TileEntityEnergyBlock EnergyBlock;
	public static final ResourceLocation Bkg = new ResourceLocation(Init.modid, "textures/gui/energyblock.png");
	
	public GuiEnergyBlock(InventoryPlayer inventory, TileEntityEnergyBlock entity) {
		super(new ContainerEnergyBlock(inventory, entity));
		
		this.EnergyBlock = entity;
		this.xSize = 176;
		this.ySize = 166;
	}

	public void drawGuiContainerForegroundLayer(int par1, int par2){
		String name = this.EnergyBlock.getInventoryName();
		
		this.fontRendererObj.drawString(name, this.xSize/2 - this.fontRendererObj.getStringWidth(name)/2, 6, 4210752);
	}
	
	public void drawGuiContainerBackgroundLayer(float f, int i, int j) {
		GL11.glColor4f(1F, 1F, 1F, 1F);
		
		Minecraft.getMinecraft().getTextureManager().bindTexture(Bkg);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
	
		int k2 = EnergyBlock.getCookProgressScaled(42);
		drawTexturedModalRect(guiLeft+94, guiTop+18, 176, 0, 5, 42-k2);
	
	}

}
