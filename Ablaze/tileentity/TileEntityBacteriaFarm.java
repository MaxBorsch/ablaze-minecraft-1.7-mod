package Ablaze.tileentity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.util.ForgeDirection;
import Ablaze.Init;

public class TileEntityBacteriaFarm extends TileEntityEnergyHandler implements ISidedInventory {

	public static final int INVENTORY_SIZE = 2;
    public static final int INPUT_INVENTORY_INDEX = 1;
    public static final int OUTPUT_INVENTORY_INDEX = 0;
    	
	public int maxWork = 50;
	public int work = 0;
	public boolean hasJar = false;
	
	private int lastWork = 0;
	private boolean hadItem;
	private boolean hadWork;
	
	private ItemStack[] inventory;
	
	
	public TileEntityBacteriaFarm() {
		this.alwaysRequest = true;
		this.maxEnergy = 100;
		
		inventory = new ItemStack[INVENTORY_SIZE];
	}
	
	@Override
	public String getInventoryName() {
		return "Bacteria Farm";
	}
	
	public int getSizeInventory(){
		return this.inventory.length;
	}

	@Override
	public ItemStack getStackInSlot(int i) {
		return this.inventory[i];
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt){
		super.readFromNBT(nbt);
		
		NBTTagList list = nbt.getTagList("Items", 10);
		inventory = new ItemStack[this.getSizeInventory()];
		
		for (int i=0; i<list.tagCount(); i++){
			NBTTagCompound compound = (NBTTagCompound) list.getCompoundTagAt(i);
			byte b = compound.getByte("Slot");
			
			if (b>=0 && b<inventory.length){
				this.inventory[b] = ItemStack.loadItemStackFromNBT(compound);
			}
		}
		
		this.work = nbt.getShort("work");
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt){
		super.writeToNBT(nbt);
				
		NBTTagList list = new NBTTagList();
		
		for (int i=0; i<inventory.length; i++){
			if (inventory[i]!=null){
				NBTTagCompound compound = new NBTTagCompound();
				compound.setByte("Slot", (byte) i);
				inventory[i].writeToNBT(compound);
				list.appendTag(compound);
			}
		}
		nbt.setTag("Items", list);
		
		nbt.setShort("work", (short) this.work);
	}
	
	public void updateEntity(){
		if (!(this.worldObj).isRemote){
			boolean flag = false;
			boolean flag2 = false;
			boolean update = false;
			
			if (inventory[OUTPUT_INVENTORY_INDEX] != null 
			&& inventory[OUTPUT_INVENTORY_INDEX].getItem() == Init.itemBacteriaContainer 
			&& inventory[OUTPUT_INVENTORY_INDEX].getItemDamage() > 0 ) {
				flag = true;
				
				if (inventory[INPUT_INVENTORY_INDEX] != null 
				&& inventory[INPUT_INVENTORY_INDEX].getItem() == Init.itemFungiSample) {
					
					if (this.work < this.maxWork && this.energy > 0) {
						this.work ++;
						this.energy --;
					} else if (this.work >= this.maxWork) {
						this.work = 0;
						
						inventory[OUTPUT_INVENTORY_INDEX].setItemDamage(inventory[OUTPUT_INVENTORY_INDEX].getItemDamage() - 1);
						
						inventory[INPUT_INVENTORY_INDEX].stackSize--;
						if (inventory[INPUT_INVENTORY_INDEX].stackSize <= 0) {
							inventory[INPUT_INVENTORY_INDEX] = null;
						}
					}
				}
			}
			
			if (this.work != this.lastWork) {
				flag2 = true;
			}
			
			if (this.hadItem && !flag) {
				this.hasJar = false;
				update = true;
			} else if (!this.hadItem && flag) {
				this.hasJar = true;
				update = true;
			}
			
			if (this.hadWork && !flag2) {
				update = true;
			} else if (!this.hadWork && flag2) {
				update = true;
			}
			
			
			if (update) {
				markDirty();
				this.state = this.work != this.lastWork ? (byte) 1 : (byte) 0;
				this.worldObj.addBlockEvent(this.xCoord, this.yCoord, this.zCoord, this.getBlockType(), 1, this.state);
				this.worldObj.addBlockEvent(this.xCoord, this.yCoord, this.zCoord, this.getBlockType(), 2, this.hasJar ? 1 : 0);
				worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
			}
			
			this.lastWork = this.work;
			this.hadItem = flag;
			this.hadWork = flag2;
		}
	}
	
	@Override
    public boolean receiveClientEvent(int eventId, int eventData)
    {
        if (eventId == 1)
        {
            this.state = (byte) eventData;
            //this.worldObj.updateAllLightTypes
            this.worldObj.func_147451_t(this.xCoord, this.yCoord, this.zCoord);
            return true;
            
        }
        else if (eventId == 2) 
        {
        	this.hasJar = eventData == 1 ? true : false;
        	return true;
        }
        else 
        {
            return super.receiveClientEvent(eventId, eventData);
        }
    }
	
	@Override
    public ItemStack decrStackSize(int slotIndex, int decrementAmount)
    {
        ItemStack itemStack = getStackInSlot(slotIndex);
        if (itemStack != null)
        {
            if (itemStack.stackSize <= decrementAmount)
            {
                setInventorySlotContents(slotIndex, null);
            }
            else
            {
                itemStack = itemStack.splitStack(decrementAmount);
                if (itemStack.stackSize == 0)
                {
                    setInventorySlotContents(slotIndex, null);
                }
            }
        }

        return itemStack;
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int slotIndex)
    {
        ItemStack itemStack = getStackInSlot(slotIndex);
        if (itemStack != null)
        {
            setInventorySlotContents(slotIndex, null);
        }
        return itemStack;
    }


    @Override
    public void setInventorySlotContents(int slotIndex, ItemStack itemStack)
    {
        inventory[slotIndex] = itemStack;
        if (itemStack != null && itemStack.stackSize > getInventoryStackLimit())
        {
            itemStack.stackSize = getInventoryStackLimit();
        }
    }

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer) {
		return true;
	}
	
	@Override
	public boolean allowInput(ForgeDirection side) {
		return true;
	}
	
	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack) {
		return true;
	}

	@Override
    public int[] getAccessibleSlotsFromSide(int side)
    {
        return side == ForgeDirection.DOWN.ordinal() ? new int[]{OUTPUT_INVENTORY_INDEX} : new int[]{INPUT_INVENTORY_INDEX, OUTPUT_INVENTORY_INDEX};
    }

	@Override
	public boolean canInsertItem(int i, ItemStack itemstack, int j) {
		return this.isItemValidForSlot(i, itemstack);
	}

	@Override
    public boolean canExtractItem(int slotIndex, ItemStack itemStack, int side)
    {
        return slotIndex == OUTPUT_INVENTORY_INDEX;
    }

	public int getEnergyScaled(int i) {		
		return (int) ((((float) this.energy) / ((float) this.maxEnergy)) * (float) i);
	}
	
	public int getWorkScaled(int i) {				
		return (int) ((((float) this.work) / ((float) this.maxWork)) * (float) i);
	}

	@Override
	public boolean hasCustomInventoryName() {
		return true;
	}

	@Override
	public void openInventory() {}

	@Override
	public void closeInventory() {}

}
