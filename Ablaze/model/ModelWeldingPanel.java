package Ablaze.model;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelWeldingPanel extends ModelBase
{
	 //fields
    ModelRenderer Shape1;
    ModelRenderer Shape2;
    ModelRenderer Shape3;
    ModelRenderer Shape4;
    ModelRenderer Shape5;
    ModelRenderer Shape6;
    ModelRenderer Shape7;
    ModelRenderer Shape8;
    ModelRenderer Shape9;
    ModelRenderer Shape10;
  
  
  public ModelWeldingPanel()
  {
	  textureWidth = 64;
	    textureHeight = 64;
	    
	      Shape1 = new ModelRenderer(this, 0, 54);
	      Shape1.addBox(-8F, -2F, -8F, 16, 2, 3);
	      Shape1.setRotationPoint(0F, 17F, 6F);
	      Shape1.setTextureSize(64, 32);
	      Shape1.mirror = true;
	      setRotation(Shape1, 0F, 0F, 0F);
	      Shape2 = new ModelRenderer(this, 0, 18);
	      Shape2.addBox(-1F, 0F, -1F, 1, 10, 2);
	      Shape2.setRotationPoint(-6F, 18F, 0F);
	      Shape2.setTextureSize(64, 32);
	      Shape2.mirror = true;
	      setRotation(Shape2, 0.7853982F, 0.7853982F, 0F);
	      Shape3 = new ModelRenderer(this, 0, 18);
	      Shape3.addBox(0F, 0F, -1F, 1, 10, 2);
	      Shape3.setRotationPoint(6F, 18F, 0F);
	      Shape3.setTextureSize(64, 32);
	      Shape3.mirror = true;
	      setRotation(Shape3, 0.7853982F, 0F, 0F);
	      Shape4 = new ModelRenderer(this, 0, 18);
	      Shape4.addBox(0F, 0F, -1F, 1, 10, 2);
	      Shape4.setRotationPoint(6F, 18F, 0F);
	      Shape4.setTextureSize(64, 32);
	      Shape4.mirror = true;
	      setRotation(Shape4, -0.7853982F, 0.7853982F, 0F);
	      Shape5 = new ModelRenderer(this, 0, 18);
	      Shape5.addBox(-1F, 0F, -1F, 1, 10, 2);
	      Shape5.setRotationPoint(-6F, 18F, 0F);
	      Shape5.setTextureSize(64, 32);
	      Shape5.mirror = true;
	      setRotation(Shape5, -0.7853982F, 0F, 0F);
	      Shape6 = new ModelRenderer(this, 0, 31);
	      Shape6.addBox(-8F, 0F, -8F, 16, 2, 12);
	      Shape6.setRotationPoint(0F, 12F, 1F);
	      Shape6.setTextureSize(64, 32);
	      Shape6.mirror = true;
	      setRotation(Shape6, 0.7853982F, 0F, 0F);
	      Shape7 = new ModelRenderer(this, 0, 0);
	      Shape7.addBox(-8F, 0F, -8F, 16, 2, 16);
	      Shape7.setRotationPoint(0F, 17F, 0F);
	      Shape7.setTextureSize(64, 32);
	      Shape7.mirror = true;
	      setRotation(Shape7, 0F, 0F, 0F);
	      Shape8 = new ModelRenderer(this, 6, 18);
	      Shape8.addBox(-8F, -2F, -8F, 16, 8, 5);
	      Shape8.setRotationPoint(0F, 11F, 11F);
	      Shape8.setTextureSize(64, 32);
	      Shape8.mirror = true;
	      setRotation(Shape8, 0F, 0F, 0F);
	      Shape9 = new ModelRenderer(this, 0, 45);
	      Shape9.addBox(-8F, -2F, -8F, 16, 4, 5);
	      Shape9.setRotationPoint(0F, 15F, 8F);
	      Shape9.setTextureSize(64, 32);
	      Shape9.mirror = true;
	      setRotation(Shape9, 0F, 0F, 0F);
	      Shape10 = new ModelRenderer(this, 0, 59);
	      Shape10.addBox(-8F, -2F, -8F, 16, 2, 2);
	      Shape10.setRotationPoint(0F, 14F, 10F);
	      Shape10.setTextureSize(64, 32);
	      Shape10.mirror = true;
	      setRotation(Shape10, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    Shape1.render(f5);
    Shape2.render(f5);
    Shape3.render(f5);
    Shape4.render(f5);
    Shape5.render(f5);
    Shape6.render(f5);
    Shape7.render(f5);
    Shape8.render(f5);
    Shape9.render(f5);
    Shape10.render(f5);
  }
  
  public void renderModel(float f5, int rotation){
	  if (rotation == 5){
		  rotation = 90;
	  }else if (rotation == 4){
		  rotation = -90;
	  }else if (rotation == 3){
		  rotation = 180;
	  }
	  GL11.glPushMatrix();
		GL11.glRotatef(rotation, 0, 1, 0);
		 Shape1.render(f5);
		    Shape2.render(f5);
		    Shape3.render(f5);
		    Shape4.render(f5);
		    Shape5.render(f5);
		    Shape6.render(f5);
		    Shape7.render(f5);
		    Shape8.render(f5);
		    Shape9.render(f5);
		    Shape10.render(f5);
		GL11.glPopMatrix();
	  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
  }

}
