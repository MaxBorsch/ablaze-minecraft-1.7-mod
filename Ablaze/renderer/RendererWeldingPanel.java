package Ablaze.renderer;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import Ablaze.Init;
import Ablaze.model.ModelWeldingPanel;
import Ablaze.tileentity.TileEntityWeldingPanel;

public class RendererWeldingPanel extends TileEntitySpecialRenderer {

	private ModelWeldingPanel model;
	private static final ResourceLocation texture = new ResourceLocation(Init.modid, "textures/models/block/WeldingPanel.png");
	
	public RendererWeldingPanel(){
		this.model = new ModelWeldingPanel();
	}
	
	@Override
	public void renderTileEntityAt(TileEntity tileentity, double x, double y,double z, float f) {
	TileEntityWeldingPanel te = (TileEntityWeldingPanel) tileentity;
		
	GL11.glPushMatrix();
	GL11.glTranslatef((float) x + 0.5F, (float)y + 1.5F, (float)z + 0.5F);
	GL11.glRotatef(180, 0F, 0F, 1F);
	this.bindTexture(texture);
	GL11.glPushMatrix();
	model.renderModel(0.0625F, te.rotation);
	GL11.glPopMatrix();
	GL11.glPopMatrix();
	}

}
