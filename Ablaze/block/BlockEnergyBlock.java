package Ablaze.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import Ablaze.Init;
import Ablaze.tileentity.TileEntityEnergyBlock;
import cpw.mods.fml.common.network.internal.FMLNetworkHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockEnergyBlock extends BlockContainer {

	private final boolean isActive;
	
	@SideOnly(Side.CLIENT)
	private IIcon iconTop;
	
	private Random random = new Random();
	
	private static boolean keepInventory;
	
	public BlockEnergyBlock(boolean isActive) {
		super(Material.rock);
		this.isActive = isActive;
	}

	public Item getBlockDropped(int par1, Random rand, int par2){
		return Init.blockEnergyBlockIdle.getItem(null, 0, 0, 0);
	}
	
	@SideOnly(Side.CLIENT)
	public void randomDisplayTick(World world, int x, int y, int z, Random r){
	if (this.isActive){
		int direction = world.getBlockMetadata(x, y, z);
		
		float x1 = (float) x + 0.5F;
		float y1 = (float) y + r.nextFloat();
		float z1 = (float) z + 0.5F;
		
		float f = 0.52F;
		float f1 = r.nextFloat() * 0.6F - 0.3F;

		if (direction == 4){
			world.spawnParticle("smoke", (double)(x1-f), (double)y1, (double)(z1+f1), 0D, 0D, 0D);
			world.spawnParticle("flame", (double)(x1-f), (double)y1, (double)(z1+f1), 0D, 0D, 0D);
		}else if (direction == 5){
			world.spawnParticle("smoke", (double)(x1+f), (double)y1, (double)(z1+f1), 0D, 0D, 0D);
			world.spawnParticle("flame", (double)(x1+f), (double)y1, (double)(z1+f1), 0D, 0D, 0D);
		}else if (direction == 2){
			world.spawnParticle("smoke", (double)(x1+f1), (double)y1, (double)(z1-f), 0D, 0D, 0D);
			world.spawnParticle("flame", (double)(x1+f1), (double)y1, (double)(z1-f), 0D, 0D, 0D);
		}else if (direction == 3){
			world.spawnParticle("smoke", (double)(x1+f1), (double)y1, (double)(z1+f), 0D, 0D, 0D);
			world.spawnParticle("flame", (double)(x1+f1), (double)y1, (double)(z1+f), 0D, 0D, 0D);
		}
		//ParticleEffects.spawnParticle("concentratedfire", (double)x, (double)y + 2, (double)z, 0.0D, 0.1D, 0.0D);

	}
	}
	
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ){
		if (!world.isRemote){
			FMLNetworkHandler.openGui(player, Init.instance, Init.guiIdEnergyBlock, world, x, y, z);
		}
		return true;
	}
		
	@Override
    public void breakBlock(World world, int x, int y, int z, Block var3, int var4){
		if (!keepInventory){
			TileEntityEnergyBlock tileentity = (TileEntityEnergyBlock) world.getTileEntity(x, y, z);
			if (tileentity!=null){
				for (int i=0; i<tileentity.getSizeInventory(); i++){
					ItemStack itemstack = tileentity.getStackInSlot(i);
					
					if (itemstack!=null){
						float f = random.nextFloat() * 0.8F + 0.1F;
						float f2 = random.nextFloat() * 0.8F + 0.1F;
						float f3 = random.nextFloat() * 0.8F + 0.1F;
						
						while (itemstack.stackSize > 0){
							int j = random.nextInt(21) + 10;
							
							if (j > itemstack.stackSize){
								j = itemstack.stackSize;
							}
							itemstack.stackSize-= j;
							
							EntityItem item = new EntityItem(world, (double)((float)x + f), (double)((float)y + f2), (double)((float)z + f3), new ItemStack(itemstack.getItem(), j, itemstack.getItemDamage()));
						
						if (itemstack.hasTagCompound()){
							item.getEntityItem().setTagCompound((NBTTagCompound) itemstack.getTagCompound().copy());
						}
						
						float f4 = 0.5F;
						item.motionX = (double)((float)this.random.nextGaussian() * f4);
						item.motionY = (double)((float)this.random.nextGaussian() * f4);
						item.motionZ = (double)((float)this.random.nextGaussian() * f4 + 0.2F);
						
						world.spawnEntityInWorld(item);
						}
					}
				}
			}
		}
		super.breakBlock(world, x, y, z, var3, var4);
	}
	
	@Override
	public TileEntity createNewTileEntity(World world, int par2) {
		return new TileEntityEnergyBlock();
	}

	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
	    this.blockIcon = par1IconRegister.registerIcon(Init.modid + ":" + "energyblock_front_idle");
	    this.iconTop = par1IconRegister.registerIcon(Init.modid + ":" + "energyblock_side_idle");

	}
	
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int metadata){
		return side == 1 ? this.iconTop : this.blockIcon;
	}

	public static void updateState(boolean b, World worldObj, int xCoord, int yCoord, int zCoord) {
		int i = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
		TileEntity tileentity = worldObj.getTileEntity(xCoord, yCoord, zCoord);
		keepInventory = true;
		if (b){
			worldObj.setBlock(xCoord, yCoord, zCoord, Init.blockEnergyBlockActive);
		}else{
			worldObj.setBlock(xCoord, yCoord, zCoord, Init.blockEnergyBlockIdle);
		}
		keepInventory = false;
		
		worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, i, 2);
		
		if (tileentity != null){
			tileentity.validate();
			worldObj.setTileEntity(xCoord, yCoord, zCoord, tileentity);
		}
	}
	
	@Override
	public boolean hasComparatorInputOverride(){
		return true;
	}
	
	@Override
	public int getComparatorInputOverride(World world, int x, int y, int z, int i){
		return Container.calcRedstoneFromInventory((IInventory) world.getTileEntity(x, y, z));
	}
	
	/*
	public boolean isOpaqueCube(){
		return false;
	}
	
	public boolean renderAsNormalBlock(){
		return false;
	}
	
    public int getRenderType()
    {
        return -1;
    }
	*/
	
}
