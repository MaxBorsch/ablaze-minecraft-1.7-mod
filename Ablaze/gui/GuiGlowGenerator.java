package Ablaze.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import Ablaze.Init;
import Ablaze.container.ContainerGlowGenerator;
import Ablaze.tileentity.TileEntityGlowGenerator;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class GuiGlowGenerator extends GuiContainer {

	public TileEntityGlowGenerator GlowGenerator;
	public static final ResourceLocation Bkg = new ResourceLocation(Init.modid, "textures/gui/glowgenerator.png");
	
	public GuiGlowGenerator(InventoryPlayer inventory, TileEntityGlowGenerator entity) {
		super(new ContainerGlowGenerator(inventory, entity));
		
		this.GlowGenerator = entity;
		this.xSize = 176;
		this.ySize = 166;
	}

	public void drawGuiContainerForegroundLayer(int par1, int par2){
		String name = this.GlowGenerator.getInventoryName();
		
		this.fontRendererObj.drawString(name, this.xSize/2 - this.fontRendererObj.getStringWidth(name)/2, 6, 4210752);
	}
	
	public void drawGuiContainerBackgroundLayer(float f, int i, int j) {
		GL11.glColor4f(1F, 1F, 1F, 1F);
		
		Minecraft.getMinecraft().getTextureManager().bindTexture(Bkg);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
	
	if (GlowGenerator.generating){
		drawTexturedModalRect(guiLeft+80, guiTop+37, 176, 0, 14, 12);
	}
	
	}

}
