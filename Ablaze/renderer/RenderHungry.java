package Ablaze.renderer;

import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import Ablaze.Init;
import Ablaze.entity.EntityHungry;
import Ablaze.model.ModelHungry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderHungry extends RenderLiving
{
    private static final ResourceLocation hungryTextures = new ResourceLocation(Init.modid + ":" + "textures/models/entity/Hungry.png");

    /** Iron Golem's Model. */
    private final ModelHungry hungryModel;

    public RenderHungry()
    {
        super(new ModelHungry(), 0.4F);
        this.hungryModel = (ModelHungry)this.mainModel;
    }

    protected void preRenderCallback(EntityHungry entity, float par2)
    {
    	float f = .8f + entity.getHunger()*.1f;
    	GL11.glScalef(f, f, f);
    }
    
    protected void preRenderCallback(EntityLivingBase par1EntityLivingBase, float par2)
    {
        this.preRenderCallback((EntityHungry)par1EntityLivingBase, par2);
    }
    
    public void doRenderHungry(EntityHungry par1EntityHungry, double par2, double par4, double par6, float par8, float par9)
    {
        super.doRender(par1EntityHungry, par2, par4, par6, par8, par9);
    }
    
    public void doRenderLiving(EntityLiving par1EntityLiving, double par2, double par4, double par6, float par8, float par9)
    {
        this.doRenderHungry((EntityHungry)par1EntityLiving, par2, par4, par6, par8, par9);
    }

    public void renderPlayer(EntityLivingBase par1EntityLivingBase, double par2, double par4, double par6, float par8, float par9)
    {
        this.doRenderHungry((EntityHungry)par1EntityLivingBase, par2, par4, par6, par8, par9);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected ResourceLocation getEntityTexture(Entity par1Entity)
    {
        return hungryTextures;
    }

    /**
     * Actually renders the given argument. This is a synthetic bridge method, always casting down its argument and then
     * handing it off to a worker function which does the actual work. In all probabilty, the class Render is generic
     * (Render<T extends Entity) and this method has signature public void doRender(T entity, double d, double d1,
     * double d2, float f, float f1). But JAD is pre 1.5 so doesn't do that.
     */
    public void doRender(Entity par1Entity, double par2, double par4, double par6, float par8, float par9)
    {
        this.doRenderHungry((EntityHungry)par1Entity, par2, par4, par6, par8, par9);
    }
}
