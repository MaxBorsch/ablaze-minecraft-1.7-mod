package Ablaze.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.ItemStack;
import Ablaze.tileentity.TileEntityWritingDesk;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerWritingDesk extends Container {

	private TileEntityWritingDesk WritingDesk;
	public int lastBurnTime;
	public int lastItemBurnTime;
	
	public ContainerWritingDesk(InventoryPlayer inventory, TileEntityWritingDesk tileentity){
		this.WritingDesk = tileentity;
		
		this.addSlotToContainer(new SlotNotebook(tileentity, 0, 29, 35));
		this.addSlotToContainer(new Slot(tileentity, 1, 57, 35));
		this.addSlotToContainer(new SlotFurnace(inventory.player, tileentity, 2, 116, 35));

		for (int i=0; i<3; i++){
			for (int j=0; j<9; j++){
				this.addSlotToContainer(new Slot(inventory, j + i*9 + 9, 8 + j*18, 84 + i*18));
			}
		}
		
		for (int i=0; i<9; i++){
			this.addSlotToContainer(new Slot(inventory, i, 8 + i*18, 142));
		}
		
	}
	
	@Override
	public void addCraftingToCrafters(ICrafting icrafting){
		super.addCraftingToCrafters(icrafting);
		icrafting.sendProgressBarUpdate(this, 1, this.WritingDesk.currentItemBurnTime);
	}
	
	@Override
	public void detectAndSendChanges(){
		super.detectAndSendChanges();
		for (int i=0; i<this.crafters.size(); i++){
			ICrafting icrafting = (ICrafting) this.crafters.get(i);

			
			if (this.lastItemBurnTime != this.WritingDesk.currentItemBurnTime){
				icrafting.sendProgressBarUpdate(this, 0, this.WritingDesk.currentItemBurnTime);
			}
			
			this.lastItemBurnTime = this.WritingDesk.currentItemBurnTime;
			
		}
	}
	
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int slot, int newValue){
		if(slot==0) this.WritingDesk.currentItemBurnTime = newValue;
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int islot){
		return null;
    }
	
	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return this.WritingDesk.isUseableByPlayer(entityplayer);
	}

}
