package Ablaze.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelBacteriaFarm extends ModelBase
{
	
	 ModelRenderer Side1;
	    ModelRenderer Top1;
	    ModelRenderer Base;
	    ModelRenderer Core;
	    ModelRenderer Top2;
	    ModelRenderer Top3;
	    ModelRenderer Top4;
	    ModelRenderer Side2;
	    ModelRenderer Jar;
	  
	  public ModelBacteriaFarm()
	  {
	    textureWidth = 128;
	    textureHeight = 32;
	    
	      Side1 = new ModelRenderer(this, 0, 17);
	      Side1.addBox(-5F, 0F, -2F, 10, 9, 4);
	      Side1.setRotationPoint(0F, 11F, 0F);
	      Side1.setTextureSize(64, 32);
	      Side1.mirror = true;
	      setRotation(Side1, 0F, 0F, 0F);
	      Top1 = new ModelRenderer(this, 29, 20);
	      Top1.addBox(2F, 0F, -3F, 1, 1, 6);
	      Top1.setRotationPoint(0F, 8F, 0F);
	      Top1.setTextureSize(64, 32);
	      Top1.mirror = true;
	      setRotation(Top1, 0F, 0F, 0F);
	      Base = new ModelRenderer(this, 0, 0);
	      Base.addBox(-6F, -2F, -6F, 12, 4, 12);
	      Base.setRotationPoint(0F, 22F, 0F);
	      Base.setTextureSize(64, 32);
	      Base.mirror = true;
	      setRotation(Base, 0F, 0F, 0F);
	      Core = new ModelRenderer(this, 49, 0);
	      Core.addBox(-4F, -7F, -4F, 8, 11, 8);
	      Core.setRotationPoint(0F, 16F, 0F);
	      Core.setTextureSize(64, 32);
	      Core.mirror = true;
	      setRotation(Core, 0F, 0F, 0F);
	      Top2 = new ModelRenderer(this, 29, 17);
	      Top2.addBox(-2F, 0F, 2F, 4, 1, 1);
	      Top2.setRotationPoint(0F, 8F, 0F);
	      Top2.setTextureSize(64, 32);
	      Top2.mirror = true;
	      setRotation(Top2, 0F, 0F, 0F);
	      Top3 = new ModelRenderer(this, 29, 20);
	      Top3.addBox(-3F, 0F, -3F, 1, 1, 6);
	      Top3.setRotationPoint(0F, 8F, 0F);
	      Top3.setTextureSize(64, 32);
	      Top3.mirror = true;
	      setRotation(Top3, 0F, 0F, 0F);
	      Top4 = new ModelRenderer(this, 29, 17);
	      Top4.addBox(-2F, 0F, -3F, 4, 1, 1);
	      Top4.setRotationPoint(0F, 8F, 0F);
	      Top4.setTextureSize(64, 32);
	      Top4.mirror = true;
	      setRotation(Top4, 0F, 0F, 0F);
	      Side2 = new ModelRenderer(this, 82, 0);
	      Side2.addBox(-2F, 0F, -5F, 4, 5, 10);
	      Side2.setRotationPoint(0F, 11F, 0F);
	      Side2.setTextureSize(64, 32);
	      Side2.mirror = true;
	      setRotation(Side2, 0F, 0F, 0F);
	      Jar = new ModelRenderer(this, 44, 20);
	      Jar.addBox(-2F, -5F, -2F, 4, 6, 4);
	      Jar.setRotationPoint(0F, 8F, 0F);
	      Jar.setTextureSize(64, 32);
	      Jar.mirror = true;
	      setRotation(Jar, 0F, 0F, 0F);
	  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    Side1.render(f5);
    Top1.render(f5);
    Base.render(f5);
    Core.render(f5);
    Top2.render(f5);
    Top3.render(f5);
    Top4.render(f5);
    Side2.render(f5);
    Jar.render(f5);
  }
  
  public void renderModel(float f5, boolean showJar){
	  Side1.render(f5);
	    Top1.render(f5);
	    Base.render(f5);
	    Core.render(f5);
	    Top2.render(f5);
	    Top3.render(f5);
	    Top4.render(f5);
	    Side2.render(f5);
	    if (showJar) {
	    	Jar.render(f5);
	    }
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
  }

}
