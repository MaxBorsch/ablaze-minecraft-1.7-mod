package Ablaze.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import Ablaze.tileentity.TileEntityTinkerTable;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerTinkerTable extends Container {

	private TileEntityTinkerTable TinkerTable;
	
	public ContainerTinkerTable(InventoryPlayer inventory, TileEntityTinkerTable tileentity){
		this.TinkerTable = tileentity;
		
		this.addSlotToContainer(new Slot(tileentity, 0, 7, 7));
		this.addSlotToContainer(new SlotNotebook(tileentity, 1, 7, 29));
		
		this.addSlotToContainer(new Slot(tileentity, 2, 45, 8));
		this.addSlotToContainer(new Slot(tileentity, 3, 45, 29));
		this.addSlotToContainer(new Slot(tileentity, 4, 66, 8));
		this.addSlotToContainer(new Slot(tileentity, 5, 66, 29));
		
		this.addSlotToContainer(new Slot(tileentity, 6, 100, 8));
		this.addSlotToContainer(new Slot(tileentity, 7, 100, 29));
		this.addSlotToContainer(new Slot(tileentity, 8, 100, 50));
		this.addSlotToContainer(new Slot(tileentity, 9, 121, 8));
		this.addSlotToContainer(new Slot(tileentity, 10, 121, 29));
		this.addSlotToContainer(new Slot(tileentity, 11, 121, 50));

		for (int i=0; i<3; i++){
			for (int j=0; j<9; j++){
				this.addSlotToContainer(new Slot(inventory, j + i*9 + 9, 8 + j*18, 84 + i*18));
			}
		}
		
		for (int i=0; i<9; i++){
			this.addSlotToContainer(new Slot(inventory, i, 8 + i*18, 142));
		}
		
	}
	
	@Override
	public void addCraftingToCrafters(ICrafting icrafting){
		super.addCraftingToCrafters(icrafting);
	}
	
	@Override
	public void detectAndSendChanges(){
		super.detectAndSendChanges();
		for (int i=0; i<this.crafters.size(); i++){
			ICrafting icrafting = (ICrafting) this.crafters.get(i);
		}
	}
	
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int slot, int newValue){

	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int islot){
		return null;
    }
	
	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return this.TinkerTable.isUseableByPlayer(entityplayer);
	}

}
