package Ablaze.container;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import Ablaze.tileentity.TileEntityInfuser;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.ItemStack;

public class ContainerInfuser extends Container {

	private TileEntityInfuser Infuser;
	public int lastBurnTime;
	public int lastItemBurnTime;
	
	public ContainerInfuser(InventoryPlayer inventory, TileEntityInfuser tileentity){
		this.Infuser = tileentity;
		
		this.addSlotToContainer(new Slot(tileentity, 0, 56, 17));
		this.addSlotToContainer(new Slot(tileentity, 1, 56, 53));
		this.addSlotToContainer(new Slot(tileentity, 3, 30, 17));
		this.addSlotToContainer(new SlotFurnace(inventory.player, tileentity, 2, 116, 35));
		

		for (int i=0; i<3; i++){
			for (int j=0; j<9; j++){
				this.addSlotToContainer(new Slot(inventory, j + i*9 + 9, 8 + j*18, 84 + i*18));
			}
		}
		
		for (int i=0; i<9; i++){
			this.addSlotToContainer(new Slot(inventory, i, 8 + i*18, 142));
		}
		
	}
	
	@Override
	public void addCraftingToCrafters(ICrafting icrafting){
		super.addCraftingToCrafters(icrafting);
		icrafting.sendProgressBarUpdate(this, 0, this.Infuser.burnTime);
		icrafting.sendProgressBarUpdate(this, 1, this.Infuser.currentItemBurnTime);
	}
	
	@Override
	public void detectAndSendChanges(){
		super.detectAndSendChanges();
		for (int i=0; i<this.crafters.size(); i++){
			ICrafting icrafting = (ICrafting) this.crafters.get(i);
			
			if (this.lastBurnTime != this.Infuser.burnTime){
				icrafting.sendProgressBarUpdate(this, 0, this.Infuser.burnTime);
			}
			
			if (this.lastItemBurnTime != this.Infuser.currentItemBurnTime){
				icrafting.sendProgressBarUpdate(this, 1, this.Infuser.currentItemBurnTime);
			}
			
			this.lastBurnTime = this.Infuser.burnTime;
			this.lastItemBurnTime = this.Infuser.currentItemBurnTime;
			
		}
	}
	
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int slot, int newValue){
		if(slot==0) this.Infuser.burnTime = newValue;
		if(slot==1) this.Infuser.currentItemBurnTime = newValue;
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int islot){
		return null;
    }
	
	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return this.Infuser.isUseableByPlayer(entityplayer);
	}

}
