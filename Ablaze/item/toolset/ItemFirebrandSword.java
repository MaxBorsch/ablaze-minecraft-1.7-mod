package Ablaze.item.toolset;

import java.util.Random;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import Ablaze.Init;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemFirebrandSword extends ItemSword {

	public ItemFirebrandSword(ToolMaterial par2EnumToolMaterial) {
		super(par2EnumToolMaterial);
	}

	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister par1IconRegister)
	{
	    this.itemIcon = par1IconRegister.registerIcon(Init.modid + ":" + (this.getUnlocalizedName().substring(5)));

	}
	
	@Override
    public boolean hitEntity(ItemStack par1ItemStack, EntityLivingBase par2EntityLivingBase, EntityLivingBase par3EntityLivingBase)
    {
		boolean safe = false;
		ItemStack ca = par3EntityLivingBase.getEquipmentInSlot(1);
		 ItemStack ca2 = par3EntityLivingBase.getEquipmentInSlot(2);
		 ItemStack ca3 = par3EntityLivingBase.getEquipmentInSlot(3);
		 ItemStack ca4 = par3EntityLivingBase.getEquipmentInSlot(4);
		if (ca!=null && ca2!=null && ca3!=null && ca4!=null){
			if (ca==new ItemStack(Init.itemFirebrandBoots, 1) && ca2==new ItemStack(Init.itemFirebrandLegs, 1) && ca3==new ItemStack(Init.itemFirebrandPlate, 1) && ca4==new ItemStack(Init.itemFirebrandHelmet, 1)){
				safe = true;
			}
		}
		 
	 if (!safe){
		Random r = new Random();
		if (r.nextInt(20)==2){
			par3EntityLivingBase.setFire(2);
		}
	 }
		par2EntityLivingBase.setFire(4);
        par1ItemStack.damageItem(1, par3EntityLivingBase);
        return true;
    }
	
}
