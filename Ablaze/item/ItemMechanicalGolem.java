package Ablaze.item;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import Ablaze.Init;
import Ablaze.entity.EntityMechanicalGolem;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemMechanicalGolem extends Item {

	public ItemMechanicalGolem() {
		this.setMaxStackSize(1);
	}

    public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10)
    {
    	if (!par3World.isRemote){
    	if (par2EntityPlayer.isSneaking()){

    	}else{
        		par1ItemStack.stackSize--;
        		EntityMechanicalGolem golem = new EntityMechanicalGolem(par3World);
        		golem.setPosition(par4+.5, par5+1, par6+.5);
        		par3World.spawnEntityInWorld(golem);
    	}
    	}
    	return true;
    }

	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister par1IconRegister)
	{
	    this.itemIcon = par1IconRegister.registerIcon(Init.modid + ":" + (this.getUnlocalizedName().substring(5)));

	}
	
}
