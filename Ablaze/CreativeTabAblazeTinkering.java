package Ablaze;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class CreativeTabAblazeTinkering extends CreativeTabs {

	public CreativeTabAblazeTinkering(int id, String label) {
		super(id, label);
	}
	
	@Override
	public String getTranslatedTabLabel(){
		return "Ablaze Tinkering";
	}
	
	@Override
	public Item getTabIconItem()
	{
		return Init.itemGolemFrame;
	}
}
