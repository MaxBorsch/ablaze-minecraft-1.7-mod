package Ablaze.item;

import java.util.Random;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import Ablaze.Init;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemLighter extends ItemPowered {

	 /**
     * Callback for item usage. If the item does something special on right clicking, he will have one of those. Return
     * True if something happen and false if it don't. This is for ITEMS, not BLOCKS
     */
    public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10)
    {
    	int a = par4;
    	int b = par5;
    	int c = par6;
    	
        if (par7 == 0)
        {
            --par5;
        }

        if (par7 == 1)
        {
            ++par5;
        }

        if (par7 == 2)
        {
            --par6;
        }

        if (par7 == 3)
        {
            ++par6;
        }

        if (par7 == 4)
        {
            --par4;
        }

        if (par7 == 5)
        {
            ++par4;
        }

        if (!par2EntityPlayer.canPlayerEdit(par4, par5, par6, par7, par1ItemStack))
        {
            return false;
        }
        else
        {
        	if (!par2EntityPlayer.isSneaking() && par1ItemStack.getItemDamage() < par1ItemStack.getMaxDamage()){
            if (par3World.isAirBlock(par4, par5, par6) && par3World.getBlock(a, b, c)!=Blocks.ice)
            {
                par3World.playSoundEffect((double)par4 + 0.5D, (double)par5 + 0.5D, (double)par6 + 0.5D, "fire.ignite", 1.0F, itemRand.nextFloat() * 0.4F + 0.8F);
                par3World.setBlock(par4, par5, par6, Blocks.fire);
                Random r = new Random();
        		float x1 = (float) a + 0.5F;
        		float y1 = (float) b + r.nextFloat();
        		float z1 = (float) c + 0.5F;
        		
        		float f = 0.52F;
        		float f1 = r.nextFloat() * 0.6F - 0.3F;
        		
        		float dir2 = r.nextFloat()/6;
        		float dir3 = r.nextFloat()/6;
                par3World.spawnParticle("flame", (double)x1, (double)b+f, (double)z1, r.nextBoolean()==true ? dir2 : -dir2, 2*(r.nextBoolean()==true ? dir3 : -dir3), r.nextBoolean()==true ? dir3 : -dir3);
                par3World.spawnParticle("flame", (double)x1, (double)b+f, (double)z1, r.nextBoolean()==true ? dir2 : -dir2, 2*(r.nextBoolean()==true ? dir3 : -dir3), r.nextBoolean()==true ? dir3 : -dir3);
                par3World.spawnParticle("flame", (double)x1, (double)b+f, (double)z1, r.nextBoolean()==true ? dir3 : -dir3, 2*(r.nextBoolean()==true ? dir2 : -dir2), r.nextBoolean()==true ? dir2 : -dir2);
                par3World.spawnParticle("flame", (double)x1, (double)b+f, (double)z1, r.nextBoolean()==true ? dir3 : -dir3, 2*(r.nextBoolean()==true ? dir2 : -dir2), r.nextBoolean()==true ? dir2 : -dir2);
                par1ItemStack.damageItem(1, par2EntityPlayer);
            }else if(par3World.getBlock(a, b, c)==Blocks.ice || par3World.getBlock(a, b-1, c)==Blocks.ice || par3World.getBlock(a, b+1, c)==Blocks.ice){
                par3World.playSoundEffect((double)par4 + 0.5D, (double)par5 + 0.5D, (double)par6 + 0.5D, "random.fizz", 1.0F, itemRand.nextFloat() * 0.4F + 0.8F);
                Random r = new Random();
        		float x1 = (float) a + 0.5F;
        		float y1 = (float) b + r.nextFloat();
        		float z1 = (float) c + 0.5F;
        		
        		float f = 0.52F;
        		float f1 = r.nextFloat() * 0.6F - 0.3F;
        		
        		float dir2 = r.nextFloat()/6;
        		float dir3 = r.nextFloat()/6;
                par1ItemStack.damageItem(1, par2EntityPlayer);
                if(par3World.getBlock(a, b, c)==Blocks.ice){
                par3World.setBlock(a, b, c, Blocks.water);
                }
                if(par3World.getBlock(a, b-1, c)==Blocks.ice){
                par3World.setBlock(a, b-1, c, Blocks.water);
                }
                if(par3World.getBlock(a, b+1, c)==Blocks.ice){
                par3World.setBlock(a, b+1, c, Blocks.water);
                }
                par3World.spawnParticle("largesmoke", (double)x1, (double)b+f, (double)z1, r.nextBoolean()==true ? dir3 : -dir3, dir2, r.nextBoolean()==true ? dir2 : -dir2);

        	}
        	}
            return true;
        }
    }

	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister par1IconRegister)
	{
	    this.itemIcon = par1IconRegister.registerIcon(Init.modid + ":" + (this.getUnlocalizedName().substring(5)));

	}
	
}
