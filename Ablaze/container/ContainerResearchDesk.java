package Ablaze.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.ItemStack;
import Ablaze.tileentity.TileEntityResearchDesk;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerResearchDesk extends Container {

	private TileEntityResearchDesk ResearchDesk;
	public int lastBurnTime;
	public int lastItemBurnTime;
	
	public ContainerResearchDesk(InventoryPlayer inventory, TileEntityResearchDesk tileentity){
		this.ResearchDesk = tileentity;
		
		this.addSlotToContainer(new Slot(tileentity, 0, 57, 35));
		this.addSlotToContainer(new SlotFurnace(inventory.player, tileentity, 1, 116, 35));
		this.addSlotToContainer(new Slot(tileentity, 2, 7, 7));
		this.addSlotToContainer(new Slot(tileentity, 3, 26, 7));
		
		for (int i=0; i<3; i++){
			for (int j=0; j<9; j++){
				this.addSlotToContainer(new Slot(inventory, j + i*9 + 9, 8 + j*18, 84 + i*18));
			}
		}
		
		for (int i=0; i<9; i++){
			this.addSlotToContainer(new Slot(inventory, i, 8 + i*18, 142));
		}
		
	}
	
	@Override
	public void addCraftingToCrafters(ICrafting icrafting){
		super.addCraftingToCrafters(icrafting);
		icrafting.sendProgressBarUpdate(this, 1, this.ResearchDesk.currentItemBurnTime);
	}
	
	@Override
	public void detectAndSendChanges(){
		super.detectAndSendChanges();
		for (int i=0; i<this.crafters.size(); i++){
			ICrafting icrafting = (ICrafting) this.crafters.get(i);

			
			if (this.lastItemBurnTime != this.ResearchDesk.currentItemBurnTime){
				icrafting.sendProgressBarUpdate(this, 0, this.ResearchDesk.currentItemBurnTime);
			}
			
			this.lastItemBurnTime = this.ResearchDesk.currentItemBurnTime;
			
		}
	}
	
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int slot, int newValue){
		if(slot==0) this.ResearchDesk.currentItemBurnTime = newValue;
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int islot){
		return null;
    }
	
	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return this.ResearchDesk.isUseableByPlayer(entityplayer);
	}

}
