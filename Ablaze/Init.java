package Ablaze;

import java.io.IOException;
import java.util.Arrays;

import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialLiquid;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.world.MinecraftException;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.oredict.OreDictionary;
import Ablaze.block.BlockAcidFluid;
import Ablaze.block.BlockAsh;
import Ablaze.block.BlockAutomatedSpawner;
import Ablaze.block.BlockBacteriaFarm;
import Ablaze.block.BlockBasicBlock;
import Ablaze.block.BlockBioGenerator;
import Ablaze.block.BlockBlazeTorch;
import Ablaze.block.BlockBombBio;
import Ablaze.block.BlockBombBioActive;
import Ablaze.block.BlockBombBlazing;
import Ablaze.block.BlockBombBlazingActive;
import Ablaze.block.BlockBombChilling;
import Ablaze.block.BlockBombChillingActive;
import Ablaze.block.BlockBombFrame;
import Ablaze.block.BlockConcentrator;
import Ablaze.block.BlockConduit;
import Ablaze.block.BlockCrystalOre;
import Ablaze.block.BlockEnergeticPlate;
import Ablaze.block.BlockEnergyBlock;
import Ablaze.block.BlockExtractor;
import Ablaze.block.BlockFungi;
import Ablaze.block.BlockFungiBlock;
import Ablaze.block.BlockGlowGenerator;
import Ablaze.block.BlockHardenedStoneCompound;
import Ablaze.block.BlockInfuser;
import Ablaze.block.BlockRefinery;
import Ablaze.block.BlockReflector;
import Ablaze.block.BlockResearchDesk;
import Ablaze.block.BlockScrappingTable;
import Ablaze.block.BlockStonePackage;
import Ablaze.block.BlockSugarShroom;
import Ablaze.block.BlockTinkerTable;
import Ablaze.block.BlockWeldingFrame;
import Ablaze.block.BlockWeldingPanel;
import Ablaze.block.BlockWoodPackage;
import Ablaze.block.BlockWritingDesk;
import Ablaze.entity.EntityBareMechanicalGolem;
import Ablaze.entity.EntityEvolvedHungry;
import Ablaze.entity.EntityHungry;
import Ablaze.entity.EntityMechanicalGolem;
import Ablaze.gui.GuiHandler;
import Ablaze.item.ItemAugmentedWrench;
import Ablaze.item.ItemBareMechanicalGolem;
import Ablaze.item.ItemBasicItem;
import Ablaze.item.ItemBasicWrench;
import Ablaze.item.ItemBioModule;
import Ablaze.item.ItemBioSingularity;
import Ablaze.item.ItemBioWand;
import Ablaze.item.ItemBlazingSingularity;
import Ablaze.item.ItemChillingSingularity;
import Ablaze.item.ItemCompletedNotebook;
import Ablaze.item.ItemConduit;
import Ablaze.item.ItemCowEffigy;
import Ablaze.item.ItemCreeperEffigy;
import Ablaze.item.ItemEncasedCrystal;
import Ablaze.item.ItemEnergyConverter;
import Ablaze.item.ItemFreezer;
import Ablaze.item.ItemIncendiaryModule;
import Ablaze.item.ItemIncompleteMechanicalGolem;
import Ablaze.item.ItemLighter;
import Ablaze.item.ItemMechanicalGolem;
import Ablaze.item.ItemNotebook;
import Ablaze.item.ItemResearchNotes;
import Ablaze.item.ItemStickODebugging;
import Ablaze.item.ItemTranscendantWrench;
import Ablaze.item.ItemUpgrade;
import Ablaze.item.toolset.ItemBronzeBoots;
import Ablaze.item.toolset.ItemBronzeHelmet;
import Ablaze.item.toolset.ItemBronzeLegs;
import Ablaze.item.toolset.ItemBronzePickaxe;
import Ablaze.item.toolset.ItemBronzePlate;
import Ablaze.item.toolset.ItemBronzeShovel;
import Ablaze.item.toolset.ItemBronzeSword;
import Ablaze.item.toolset.ItemFirebrandHelmet;
import Ablaze.item.toolset.ItemFirebrandPickaxe;
import Ablaze.item.toolset.ItemFirebrandShovel;
import Ablaze.item.toolset.ItemFirebrandSword;
import Ablaze.item.toolset.ItemXeniteBoots;
import Ablaze.item.toolset.ItemXeniteHelmet;
import Ablaze.item.toolset.ItemXeniteLegs;
import Ablaze.item.toolset.ItemXenitePickaxe;
import Ablaze.item.toolset.ItemXenitePlate;
import Ablaze.item.toolset.ItemXeniteShovel;
import Ablaze.item.toolset.ItemXeniteSword;
import Ablaze.item.toolset.ItemXeniteVisoredHelmet;
import Ablaze.proxy.CommonProxy;
import Ablaze.tileentity.TileEntityAutomatedSpawner;
import Ablaze.tileentity.TileEntityBacteriaFarm;
import Ablaze.tileentity.TileEntityBioGenerator;
import Ablaze.tileentity.TileEntityBombBio;
import Ablaze.tileentity.TileEntityBombBioActive;
import Ablaze.tileentity.TileEntityBombBlazing;
import Ablaze.tileentity.TileEntityBombBlazingActive;
import Ablaze.tileentity.TileEntityBombChilling;
import Ablaze.tileentity.TileEntityBombChillingActive;
import Ablaze.tileentity.TileEntityBombFrame;
import Ablaze.tileentity.TileEntityConcentrator;
import Ablaze.tileentity.TileEntityConduit;
import Ablaze.tileentity.TileEntityEnergyBlock;
import Ablaze.tileentity.TileEntityExtractor;
import Ablaze.tileentity.TileEntityGlowGenerator;
import Ablaze.tileentity.TileEntityInfuser;
import Ablaze.tileentity.TileEntityRefinery;
import Ablaze.tileentity.TileEntityResearchDesk;
import Ablaze.tileentity.TileEntityScrappingTable;
import Ablaze.tileentity.TileEntityTinkerTable;
import Ablaze.tileentity.TileEntityWeldingPanel;
import Ablaze.tileentity.TileEntityWritingDesk;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.ModMetadata;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid = Init.modid, name = "Ablaze", version = "1.0.0")

public class Init {
	public static final String modid = "desharp_ablaze";
	@Instance(modid)
	public static Init instance;
	
	@SidedProxy(clientSide="Ablaze.proxy.ClientProxy", serverSide = "Ablaze.proxy.CommonProxy")
	
	public static CommonProxy proxy;

	@Mod.Metadata(modid)
    public static ModMetadata metadata;
	
	ToolMaterial FirebrandTool = EnumHelper.addToolMaterial("Firebrand", 2, 300, 8.0F, 2.5F, 20);
	ArmorMaterial FirebrandArmor = EnumHelper.addArmorMaterial("Firebrand", 20, new int[]{3, 8, 6, 3}, 6);
	
	ToolMaterial XeniteTool = EnumHelper.addToolMaterial("Xenite", 2, 300, 8.0F, 2.5F, 20);
	ArmorMaterial XeniteArmor = EnumHelper.addArmorMaterial("Xenite", 20, new int[]{3, 8, 6, 3}, 6);
	
	ToolMaterial BronzeTool = EnumHelper.addToolMaterial("Bronze", 2, 340, 6.0F, 2.0F, 16);
	ArmorMaterial BronzeArmor = EnumHelper.addArmorMaterial("Bronze", 25, new int[]{2, 6, 5, 2}, 11);
	
	
	public static Material materialAcid;
	
	public static CreativeTabs TAB;
	public static CreativeTabs TABTinkering;
	public static CreativeTabs TABTools;
	public static CreativeTabs TABWorld;
	
	EventManager eventmanager = new EventManager();

	//Blocks
	public static Block blockBlazeTorch;
	public static Block blockScorchedBrick;
	public static Block blockAsh;
	public static Block blockRefineryIdle;
	public static Block blockRefineryActive;
	public static Block blockInfuserIdle;
	public static Block blockInfuserActive;
	public static Block blockConcentratorIdle;
	public static Block blockConcentratorActive;
	public static Block blockExtractorIdle;
	public static Block blockExtractorActive;
	public static Block blockEnergyBlockIdle;
	public static Block blockEnergyBlockActive;
	public static Block blockBioGenerator;
	public static Block blockXeniteOre;
	public static Block blockAcidFluid;
	public static Block blockStonePackage;
	public static Block blockWoodPackage;
	public static Block blockReflector;
	public static Block blockCrystalOre;
	public static Block blockCopperOre;
	public static Block blockTinOre;
	public static Block blockMachineBlock;
	public static Block blockBombFrame;
	public static Block blockBombBlazing;
	public static Block blockBombChilling;
	public static Block blockBombBio;
	public static Block blockBombBlazingActive;
	public static Block blockBombChillingActive;
	public static Block blockBombBioActive;
	public static Block blockHardenedStone;
	public static Block blockHardenedStoneCompound;
	public static Block blockGlowGenIdle;
	public static Block blockGlowGenActive;
	public static Block blockConduit;
	public static Block blockEnergeticPlate;
	public static Block blockTinkerTable;
	public static Block blockScrappingTable;
	public static Block blockWritingDesk;
	public static Block blockWeldingFrame;
	public static Block blockWeldingPanel;
	public static Block blockResearchDesk;
	public static Block blockSugarShroom;
	public static Block blockAutomatedSpawner;
	public static Block blockFungiBlock;
	public static Block blockFungi;
	public static Block blockFungiGlow;
	public static Block blockBacteriaFarm;
	
	//Items
	public static Item itemNotebook;
	public static Item itemCompletedNotebook;
	public static Item itemChain;
	public static Item itemAsh;
	public static Item itemBlazeIngot;
	public static Item itemRefinedBlazeDust;
	public static Item itemWitherDust;
	public static Item itemXeniteIngot;
	public static Item itemThermobicPlate;
	public static Item itemBlazingSingularity;
	public static Item itemChillingSingularity;
	public static Item itemBioSingularity;
	public static Item itemBlazingEssence;
	public static Item itemChillingEssence;
	public static Item itemBioEssence;
	public static Item itemLucidCrystal;
	public static Item itemChillingCrystal;
	public static Item itemBlazingCrystal;
	public static Item itemBioCrystal;
	public static Item itemBronzeIngot;
	public static Item itemCopperIngot;
	public static Item itemTinIngot;
	public static Item itemCopperDust;
	public static Item itemTinDust;
	public static Item itemBronzeDust;
	public static Item itemHeatVent;
	public static Item itemEnergyConverter;
	public static Item itemLighter;
	public static Item itemFreezer;
	public static Item itemBioWand;
	public static Item itemEncasedCrystal;
	public static Item itemThermobicSheet;
	public static Item itemStickODebugging;
	public static Item itemConduit;
	public static Item itemIncendiaryModule;
	public static Item itemBioModule;
	public static Item itemBasicWrench;
	public static Item itemAugmentedWrench;
	public static Item itemTranscendantWrench;
	public static Item itemBareMechanicalGolem;
	public static Item itemMechanicalGolem;
	public static Item itemGolemFrame;
	public static Item itemIncompleteMechanicalGolem;
	public static Item itemGolemHead;
	public static Item itemGolemTorso;
	public static Item itemGolemRightArm;
	public static Item itemGolemLeftArm;
	public static Item itemGolemRightLeg;
	public static Item itemGolemLeftLeg;
	public static Item itemResearchNotes;
	public static Item itemWiring;
	public static Item itemGoldWiring;
	public static Item itemHyperconductiveWiring;
	public static Item itemThinWiring;
	public static Item itemThinGoldWiring;
	public static Item itemServoMotor;
	public static Item itemCoil;
	public static Item itemSolenoid;
	public static Item itemThermoReactor;
	public static Item itemSmallGear;
	public static Item itemLargeGear;
	public static Item itemGearbox;
	public static Item itemBronzeSlice;
	public static Item itemGoldSlice;
	public static Item itemMultiConnector;
	public static Item itemT1Processor;
	public static Item itemT2Processor;
	public static Item itemFlourescentCube;
	public static Item itemT1LogicCard;
	public static Item itemT2LogicCard;
	public static Item itemT1ProcessingUnit;
	public static Item itemT1LogicUnit;
	public static Item itemPen;
	public static Item itemScrapPaper;
	public static Item itemMoss;
	public static Item itemFungiSample;
	public static Item itemVileSugar;
	public static Item itemCleansingSugar;
	public static Item itemCreeperEffigy;
	public static Item itemCowEffigy;
	public static Item itemBacteriaContainer;
	public static Item itemUnattainableEnergy;
	public static Item itemSpeedUpgrade;
	public static Item itemEfficiencyUpgrade;
	public static Item itemRangeUpgrade;
	
	//Toolset
	public static Item itemFirebrandHelmet;
	public static Item itemFirebrandPlate;
	public static Item itemFirebrandLegs;
	public static Item itemFirebrandBoots;
	public static Item itemFirebrandSword;
	public static Item itemFirebrandShovel;
	public static Item itemFirebrandPickaxe;
	public static Item itemXeniteHelmet;
	public static Item itemXeniteVisoredHelmet;
	public static Item itemXenitePlate;
	public static Item itemXeniteLegs;
	public static Item itemXeniteBoots;
	public static Item itemXeniteSword;
	public static Item itemXeniteShovel;
	public static Item itemXenitePickaxe;
	public static Item itemBronzeHelmet;
	public static Item itemBronzePlate;
	public static Item itemBronzeLegs;
	public static Item itemBronzeBoots;
	public static Item itemBronzeSword;
	public static Item itemBronzeShovel;
	public static Item itemBronzePickaxe;
	
	public static Fluid fluidAcid;
	
	public static final int idFluidAcid = 1001;
	
	public static final int guiIdRefinery = 0;
	public static final int guiIdConcentrator = 1;
	public static final int guiIdExtractor = 2;
	public static final int guiIdInfuser = 3;
	public static final int guiIdEnergyBlock = 4;
	public static final int guiIdBioGenerator = 5;
	public static final int guiIdGlowGenerator = 6;
	public static final int guiIdTinkerTable = 7;
	public static final int guiIdScrappingTable = 8;
	public static final int guiIdWritingDesk = 9;
	public static final int guiIdWeldingPanel = 10;
	public static final int guiIdResearchDesk = 11;
	public static final int guiIdAutomatedSpawner = 12;
	public static final int guiIdBacteriaFarm = 13;

    @EventHandler
    public void init(FMLInitializationEvent evt) throws MinecraftException
    {
    	metadata.modId = modid;
        metadata.name = "Ablaze";
        metadata.description = "A mod revolving around a combination of Machines, Magic, and Tinkering with little gadgets!";
        metadata.url = "";
        metadata.logoFile = "";
        metadata.version = "0.9.0 Beta";
        metadata.authorList = Arrays.asList(new String[] {"DeSharp"});
        metadata.credits = "";
        metadata.autogenerated = false;
    }
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent evt) throws IOException {
    	TAB = new CreativeTabAblaze(CreativeTabs.getNextID(), "Ablaze");
    	TABTinkering = new CreativeTabAblazeTinkering(CreativeTabs.getNextID(), "Ablaze Tinkering");
    	TABTools = new CreativeTabAblazeToolsets(CreativeTabs.getNextID(), "Ablaze Tools & Armor");
    	TABWorld = new CreativeTabAblazeWorld(CreativeTabs.getNextID(), "Ablaze World");

    	materialAcid = new MaterialLiquid(MapColor.grassColor);
		
		fluidAcid = new Fluid("acid").setDensity(20).setLuminosity(10);
		FluidRegistry.registerFluid(fluidAcid);
    	
    	blockBlazeTorch = new BlockBlazeTorch().setBlockName("blazetorchBlock").setCreativeTab(TABWorld);
    	blockScorchedBrick = new BlockBasicBlock().setBlockName("scorchedbrickBlock").setCreativeTab(TABWorld).setHardness(10F).setResistance(20F);
    	blockAsh = new BlockAsh().setBlockName("ashBlock").setCreativeTab(TABWorld);
    	blockRefineryIdle = new BlockRefinery( false).setBlockName("refineryidleBlock").setCreativeTab(TAB);
    	blockRefineryActive = new BlockRefinery( true).setBlockName("refineryactiveBlock").setLightLevel(.9F);
    	blockConcentratorIdle = new BlockConcentrator( false).setBlockName("concentratoridleBlock").setCreativeTab(TAB);
    	blockConcentratorActive = new BlockConcentrator( true).setBlockName("concentratoractiveBlock").setLightLevel(.9F);
    	blockExtractorIdle = new BlockExtractor( false).setBlockName("extractoridleBlock").setCreativeTab(TAB);
    	blockExtractorActive = new BlockExtractor( true).setBlockName("extractoractiveBlock").setLightLevel(.9F);
    	blockInfuserIdle = new BlockInfuser( false).setBlockName("infuseridleBlock").setCreativeTab(TAB);
    	blockInfuserActive = new BlockInfuser( true).setBlockName("infuseractiveBlock").setLightLevel(.9F);
    	blockEnergyBlockIdle = new BlockEnergyBlock( false).setBlockName("energyblockidleBlock").setCreativeTab(TAB);
    	blockEnergyBlockActive = new BlockEnergyBlock( true).setBlockName("energyblockactiveBlock").setLightLevel(.9F);
    	blockAutomatedSpawner = new BlockAutomatedSpawner().setBlockName("automatedspawnerBlock").setCreativeTab(TAB);
    	blockBacteriaFarm = new BlockBacteriaFarm().setBlockName("bacteriafarmBlock").setCreativeTab(TAB);
    	blockBioGenerator = new BlockBioGenerator().setBlockName("biogeneratorBlock");
    	blockGlowGenIdle = new BlockGlowGenerator( false).setBlockName("glowgeneratoridleBlock");
    	blockGlowGenActive = new BlockGlowGenerator( true).setBlockName("glowgeneratoractiveBlock");
    	blockXeniteOre = new BlockBasicBlock().setBlockName("xeniteBlock").setCreativeTab(TABWorld);
    	blockAcidFluid = new BlockAcidFluid(idFluidAcid).setBlockName("acidfluidBlock").setCreativeTab(TABWorld);
    	blockStonePackage = new BlockStonePackage().setBlockName("stonepackageBlock").setCreativeTab(TABWorld);
    	blockWoodPackage = new BlockWoodPackage().setBlockName("woodpackageBlock").setCreativeTab(TABWorld);
    	blockReflector = new BlockReflector().setBlockName("reflectorBlock").setCreativeTab(TAB);
    	blockCrystalOre = new BlockCrystalOre().setBlockName("crystalBlock").setCreativeTab(TABWorld);
    	blockCopperOre = new BlockBasicBlock().setBlockName("copperBlock").setCreativeTab(TABWorld);
    	blockTinOre = new BlockBasicBlock().setBlockName("tinBlock").setCreativeTab(TABWorld);
    	blockMachineBlock = new BlockBasicBlock().setBlockName("machineblockBlock").setCreativeTab(TAB);
    	blockBombChilling = new BlockBombChilling().setBlockName("bombchillingBlock");
    	blockBombBlazing = new BlockBombBlazing().setBlockName("bombblazingBlock");
    	blockBombBio = new BlockBombBio().setBlockName("bombbioBlock");
    	blockBombChillingActive = new BlockBombChillingActive().setBlockName("bombchillingactiveBlock");
    	blockBombBlazingActive = new BlockBombBlazingActive().setBlockName("bombblazingactiveBlock");
    	blockBombBioActive = new BlockBombBioActive().setBlockName("bombbioactiveBlock");
    	blockBombFrame = new BlockBombFrame().setBlockName("bombframeBlock").setCreativeTab(TAB);
    	blockHardenedStone = new BlockBasicBlock().setBlockName("hardenedstoneBlock").setCreativeTab(TABWorld).setHardness(10F).setResistance(20F);
    	blockHardenedStoneCompound = new BlockHardenedStoneCompound().setBlockName("hardenedstonecompoundBlock").setCreativeTab(TAB);
    	blockConduit = new BlockConduit().setBlockName("conduitBlock");
    	blockEnergeticPlate = new BlockEnergeticPlate().setBlockName("energeticplateBlock").setCreativeTab(TAB);
    	blockTinkerTable = new BlockTinkerTable().setBlockName("tinkertableBlock").setCreativeTab(TAB);
    	blockScrappingTable = new BlockScrappingTable().setBlockName("scrappingtableBlock").setCreativeTab(TAB);
    	blockWritingDesk = new BlockWritingDesk().setBlockName("writingdeskBlock").setCreativeTab(TAB);
    	blockResearchDesk = new BlockResearchDesk().setBlockName("researchdeskBlock").setCreativeTab(TAB);
    	blockWeldingFrame = new BlockWeldingFrame().setBlockName("weldingframeBlock").setCreativeTab(TAB);
    	blockWeldingPanel = new BlockWeldingPanel().setBlockName("weldingpanelBlock").setCreativeTab(TAB);
    	blockSugarShroom = new BlockSugarShroom().setBlockName("sugarshroomBlock").setCreativeTab(TABWorld);
    	blockFungiBlock = new BlockFungiBlock().setBlockName("fungiblockBlock").setCreativeTab(TABWorld);
    	blockFungi = new BlockFungi().setBlockName("fungiBlock").setCreativeTab(TABWorld);
    	blockFungiGlow = new BlockFungi().setBlockName("fungiglowBlock").setCreativeTab(TABWorld).setLightLevel(0.25F);

    	itemFirebrandSword = new ItemFirebrandSword(FirebrandTool).setUnlocalizedName("firebrandswordItem").setCreativeTab(TABTools);
    	itemFirebrandHelmet = new ItemFirebrandHelmet(FirebrandArmor, 3, 0).setUnlocalizedName("firebrandhelmetItem").setCreativeTab(TABTools);
    	itemFirebrandPlate = new ItemFirebrandHelmet(FirebrandArmor, 3, 1).setUnlocalizedName("firebrandplateItem").setCreativeTab(TABTools);
    	itemFirebrandLegs = new ItemFirebrandHelmet(FirebrandArmor, 3, 2).setUnlocalizedName("firebrandlegsItem").setCreativeTab(TABTools);
    	itemFirebrandBoots = new ItemFirebrandHelmet(FirebrandArmor, 3, 3).setUnlocalizedName("firebrandbootsItem").setCreativeTab(TABTools);
    	itemBlazeIngot = new ItemBasicItem().setUnlocalizedName("blazeingotItem").setCreativeTab(TAB);
    	itemFirebrandShovel = new ItemFirebrandShovel(FirebrandTool).setUnlocalizedName("firebrandshovelItem").setCreativeTab(TABTools);
    	itemFirebrandPickaxe = new ItemFirebrandPickaxe(FirebrandTool).setUnlocalizedName("firebrandpickaxeItem").setCreativeTab(TABTools);
    	itemRefinedBlazeDust = new ItemBasicItem().setUnlocalizedName("refinedblazedustItem").setCreativeTab(TAB);
    	itemWitherDust = new ItemBasicItem().setUnlocalizedName("witherdustItem").setCreativeTab(TAB);
    	itemXeniteIngot = new ItemBasicItem().setUnlocalizedName("xeniteingotItem").setCreativeTab(TAB);
    	itemXeniteSword = new ItemXeniteSword(XeniteTool).setUnlocalizedName("xeniteswordItem").setCreativeTab(TABTools);
    	itemXeniteHelmet = new ItemXeniteHelmet(XeniteArmor, 3, 0).setUnlocalizedName("xenitehelmetItem").setCreativeTab(TABTools);
    	itemXeniteVisoredHelmet = new ItemXeniteVisoredHelmet(XeniteArmor, 3, 0).setUnlocalizedName("xenitevisoredhelmetItem").setCreativeTab(TABTools);
    	itemXenitePlate = new ItemXenitePlate(XeniteArmor, 3, 1).setUnlocalizedName("xeniteplateItem").setCreativeTab(TABTools);
    	itemXeniteLegs = new ItemXeniteLegs(XeniteArmor, 3, 2).setUnlocalizedName("xenitelegsItem").setCreativeTab(TABTools);
    	itemXeniteBoots = new ItemXeniteBoots(XeniteArmor, 3, 3).setUnlocalizedName("xenitebootsItem").setCreativeTab(TABTools);
    	itemXeniteShovel = new ItemXeniteShovel(XeniteTool).setUnlocalizedName("xeniteshovelItem").setCreativeTab(TABTools);
    	itemXenitePickaxe = new ItemXenitePickaxe(XeniteTool).setUnlocalizedName("xenitepickaxeItem").setCreativeTab(TABTools);
    	itemChain = new ItemBasicItem().setUnlocalizedName("chainItem").setCreativeTab(TAB);
    	itemAsh = new ItemBasicItem().setUnlocalizedName("ashItem").setCreativeTab(TAB);
    	itemNotebook = new ItemNotebook().setUnlocalizedName("notebookItem").setCreativeTab(TAB);
    	itemCompletedNotebook = new ItemCompletedNotebook().setUnlocalizedName("completednotebookItem").setCreativeTab(TAB);
    	itemThermobicPlate = new ItemBasicItem().setUnlocalizedName("thermobicplateItem").setCreativeTab(TAB);
    	itemBlazingSingularity = new ItemBlazingSingularity().setUnlocalizedName("blazingsingularityItem").setCreativeTab(TAB);
    	itemChillingSingularity = new ItemChillingSingularity().setUnlocalizedName("chillingsingularityItem").setCreativeTab(TAB);
    	itemBioSingularity = new ItemBioSingularity().setUnlocalizedName("biosingularityItem").setCreativeTab(TAB);
    	itemBlazingEssence = new ItemBasicItem().setUnlocalizedName("blazingessenceItem").setCreativeTab(TAB);
    	itemChillingEssence = new ItemBasicItem().setUnlocalizedName("chillingessenceItem").setCreativeTab(TAB);
    	itemBioEssence = new ItemBasicItem().setUnlocalizedName("bioessenceItem").setCreativeTab(TAB);
    	itemLucidCrystal = new ItemBasicItem().setUnlocalizedName("lucidcrystalItem").setCreativeTab(TAB);
    	itemChillingCrystal = new ItemBasicItem().setUnlocalizedName("chillingcrystalItem").setCreativeTab(TAB);
    	itemBlazingCrystal = new ItemBasicItem().setUnlocalizedName("blazingcrystalItem").setCreativeTab(TAB);
    	itemBioCrystal = new ItemBasicItem().setUnlocalizedName("biocrystalItem").setCreativeTab(TAB);
    	itemBronzeIngot = new ItemBasicItem().setUnlocalizedName("bronzeingotItem").setCreativeTab(TAB);
    	itemCopperIngot = new ItemBasicItem().setUnlocalizedName("copperingotItem").setCreativeTab(TAB);
    	itemTinIngot = new ItemBasicItem().setUnlocalizedName("tiningotItem").setCreativeTab(TAB);
    	itemCopperDust = new ItemBasicItem().setUnlocalizedName("copperdustItem").setCreativeTab(TAB);
    	itemTinDust = new ItemBasicItem().setUnlocalizedName("tindustItem").setCreativeTab(TAB);
    	itemBronzeDust = new ItemBasicItem().setUnlocalizedName("bronzedustItem").setCreativeTab(TAB);
    	itemHeatVent = new ItemBasicItem().setUnlocalizedName("heatventItem").setCreativeTab(TAB);
    	itemEnergyConverter = new ItemEnergyConverter().setUnlocalizedName("energyconverterItem").setCreativeTab(TAB);
    	itemLighter = new ItemLighter().setUnlocalizedName("lighterItem").setCreativeTab(TAB);
    	itemFreezer = new ItemFreezer().setUnlocalizedName("freezerItem").setCreativeTab(TAB);
    	itemBioWand = new ItemBioWand().setUnlocalizedName("biowandItem").setCreativeTab(TAB);
    	itemEncasedCrystal = new ItemEncasedCrystal().setUnlocalizedName("encasedcrystalItem").setCreativeTab(TAB);
    	itemThermobicSheet = new ItemBasicItem().setUnlocalizedName("thermobicsheetItem").setCreativeTab(TAB);
    	itemStickODebugging = new ItemStickODebugging().setUnlocalizedName("stickodebuggingItem").setCreativeTab(TAB);
    	itemConduit = new ItemConduit().setUnlocalizedName("conduitItem").setCreativeTab(TAB);
    	itemIncendiaryModule = new ItemIncendiaryModule().setUnlocalizedName("incendiarymoduleItem").setCreativeTab(TAB);
    	itemBioModule = new ItemBioModule().setUnlocalizedName("biomoduleItem").setCreativeTab(TAB);
    	itemBasicWrench = new ItemBasicWrench().setUnlocalizedName("basicwrenchItem").setCreativeTab(TAB);
    	itemAugmentedWrench = new ItemAugmentedWrench().setUnlocalizedName("augmentedwrenchItem").setCreativeTab(TAB);
    	itemTranscendantWrench = new ItemTranscendantWrench().setUnlocalizedName("transcendantwrenchItem").setCreativeTab(TAB);
    	itemBareMechanicalGolem = new ItemBareMechanicalGolem().setUnlocalizedName("baremechanicalgolemItem").setCreativeTab(TAB);
    	itemMechanicalGolem = new ItemMechanicalGolem().setUnlocalizedName("mechanicalgolemItem").setCreativeTab(TAB);
    	itemGolemFrame = new ItemBasicItem().setUnlocalizedName("golemframeItem").setCreativeTab(TABTinkering);
    	itemIncompleteMechanicalGolem = new ItemIncompleteMechanicalGolem().setUnlocalizedName("incompletemechanicalgolemItem").setCreativeTab(TABTinkering);
    	itemGolemHead = new ItemBasicItem().setUnlocalizedName("golemheadItem").setCreativeTab(TABTinkering);
    	itemGolemTorso = new ItemBasicItem().setUnlocalizedName("golemtorsoItem").setCreativeTab(TABTinkering);
    	itemGolemRightArm = new ItemBasicItem().setUnlocalizedName("golemrightarmItem").setCreativeTab(TABTinkering);
    	itemGolemLeftArm = new ItemBasicItem().setUnlocalizedName("golemleftarmItem").setCreativeTab(TABTinkering);
    	itemGolemRightLeg = new ItemBasicItem().setUnlocalizedName("golemrightlegItem").setCreativeTab(TABTinkering);
    	itemGolemLeftLeg = new ItemBasicItem().setUnlocalizedName("golemleftlegItem").setCreativeTab(TABTinkering);
    	itemResearchNotes = new ItemResearchNotes().setUnlocalizedName("researchnotesItem").setCreativeTab(TAB);
    	itemBronzeSlice = new ItemBasicItem().setUnlocalizedName("bronzesliceItem").setCreativeTab(TABTinkering);
    	itemGoldSlice = new ItemBasicItem().setUnlocalizedName("goldsliceItem").setCreativeTab(TABTinkering);
    	itemWiring = new ItemBasicItem().setUnlocalizedName("wiringItem").setCreativeTab(TABTinkering);
    	itemGoldWiring = new ItemBasicItem().setUnlocalizedName("goldwiringItem").setCreativeTab(TABTinkering);
    	itemHyperconductiveWiring = new ItemBasicItem().setUnlocalizedName("hyperconductivewiringItem").setCreativeTab(TABTinkering);
    	itemThinWiring = new ItemBasicItem().setUnlocalizedName("thinwiringItem").setCreativeTab(TABTinkering);
    	itemThinGoldWiring = new ItemBasicItem().setUnlocalizedName("thingoldwiringItem").setCreativeTab(TABTinkering);
    	itemServoMotor = new ItemBasicItem().setUnlocalizedName("servomotorItem").setCreativeTab(TABTinkering);
    	itemCoil = new ItemBasicItem().setUnlocalizedName("coilItem").setCreativeTab(TABTinkering);
    	itemSolenoid = new ItemBasicItem().setUnlocalizedName("solenoidItem").setCreativeTab(TABTinkering);
    	itemThermoReactor = new ItemBasicItem().setUnlocalizedName("thermoreactorItem").setCreativeTab(TABTinkering);
    	itemSmallGear = new ItemBasicItem().setUnlocalizedName("smallgearItem").setCreativeTab(TABTinkering);
    	itemLargeGear = new ItemBasicItem().setUnlocalizedName("largegearItem").setCreativeTab(TABTinkering);
    	itemGearbox = new ItemBasicItem().setUnlocalizedName("gearboxItem").setCreativeTab(TABTinkering);
    	itemMultiConnector = new ItemBasicItem().setUnlocalizedName("multiconnectorItem").setCreativeTab(TABTinkering);
    	itemT1Processor = new ItemBasicItem().setUnlocalizedName("t1processorItem").setCreativeTab(TABTinkering);
    	itemT2Processor = new ItemBasicItem().setUnlocalizedName("t2processorItem").setCreativeTab(TABTinkering);
    	itemT1LogicCard = new ItemBasicItem().setUnlocalizedName("t1logiccardItem").setCreativeTab(TABTinkering);
    	itemT2LogicCard = new ItemBasicItem().setUnlocalizedName("t2logiccardItem").setCreativeTab(TABTinkering);
    	itemFlourescentCube = new ItemBasicItem().setUnlocalizedName("flourescentcubeItem").setCreativeTab(TABTinkering);
    	itemT1ProcessingUnit = new ItemBasicItem().setUnlocalizedName("t1processingunitItem").setCreativeTab(TABTinkering);
    	itemT1LogicUnit = new ItemBasicItem().setUnlocalizedName("t1logicunitItem").setCreativeTab(TABTinkering);
    	itemBronzeSword = new ItemBronzeSword(BronzeTool).setUnlocalizedName("bronzeswordItem").setCreativeTab(TABTools);
    	itemBronzeHelmet = new ItemBronzeHelmet(BronzeArmor, 3, 0).setUnlocalizedName("bronzehelmetItem").setCreativeTab(TABTools);
    	itemBronzePlate = new ItemBronzePlate(BronzeArmor, 3, 1).setUnlocalizedName("bronzeplateItem").setCreativeTab(TABTools);
    	itemBronzeLegs = new ItemBronzeLegs(BronzeArmor, 3, 2).setUnlocalizedName("bronzelegsItem").setCreativeTab(TABTools);
    	itemBronzeBoots = new ItemBronzeBoots(BronzeArmor, 3, 3).setUnlocalizedName("bronzebootsItem").setCreativeTab(TABTools);
    	itemBronzeShovel = new ItemBronzeShovel(BronzeTool).setUnlocalizedName("bronzeshovelItem").setCreativeTab(TABTools);
    	itemBronzePickaxe = new ItemBronzePickaxe(BronzeTool).setUnlocalizedName("bronzepickaxeItem").setCreativeTab(TABTools);
    	itemPen = new ItemBasicItem().setUnlocalizedName("penItem").setMaxStackSize(1).setMaxDamage(16).setCreativeTab(TAB);
    	itemScrapPaper = new ItemBasicItem().setUnlocalizedName("scrappaperItem").setCreativeTab(TAB);
    	itemMoss = new ItemBasicItem().setUnlocalizedName("mossItem").setCreativeTab(TABWorld);
    	itemFungiSample = new ItemBasicItem().setUnlocalizedName("fungisampleItem").setCreativeTab(TABWorld);
    	itemVileSugar = new ItemBasicItem().setUnlocalizedName("vilesugarItem").setCreativeTab(TAB);
    	itemCleansingSugar = new ItemBasicItem().setUnlocalizedName("cleansingsugarItem").setCreativeTab(TAB);
    	itemCreeperEffigy = new ItemCreeperEffigy().setUnlocalizedName("creepereffigyItem").setCreativeTab(TAB);
    	itemCowEffigy = new ItemCowEffigy().setUnlocalizedName("coweffigyItem").setCreativeTab(TAB);
    	itemBacteriaContainer = new ItemBasicItem().setUnlocalizedName("bacteriacontainerItem").setMaxStackSize(1).setMaxDamage(64).setCreativeTab(TAB);
    	itemUnattainableEnergy = new ItemBasicItem().setUnlocalizedName("unattainableenergyItem").setCreativeTab(TAB);
    	itemSpeedUpgrade = new ItemUpgrade(0, 1).setUnlocalizedName("speedupgradeItem").setCreativeTab(TAB);
    	itemEfficiencyUpgrade = new ItemUpgrade(1, 1).setUnlocalizedName("efficiencyupgradeItem").setCreativeTab(TAB);
    	itemRangeUpgrade = new ItemUpgrade(2, 1).setUnlocalizedName("rangeupgradeItem").setCreativeTab(TAB);

    	registerBlock(blockBlazeTorch);
    	registerBlock(blockScorchedBrick);
    	registerBlock(blockAsh);
    	registerBlock(blockRefineryIdle);
    	registerBlock(blockRefineryActive);
    	registerBlock(blockConcentratorIdle);
    	registerBlock(blockConcentratorActive);
    	registerBlock(blockExtractorIdle);
    	registerBlock(blockExtractorActive);
    	registerBlock(blockInfuserIdle);
    	registerBlock(blockInfuserActive);
    	registerBlock(blockEnergyBlockIdle);
    	registerBlock(blockEnergyBlockActive);
    	registerBlock(blockAutomatedSpawner);
    	registerBlock(blockBacteriaFarm);
    	registerBlock(blockReflector);
    	registerBlock(blockXeniteOre);
    	registerBlock(blockCrystalOre);
    	registerBlock(blockAcidFluid);
    	registerBlock(blockStonePackage);
    	registerBlock(blockWoodPackage);
    	registerBlock(blockCopperOre);
    	registerBlock(blockTinOre);
    	registerBlock(blockMachineBlock);
    	registerBlock(blockBioGenerator);
    	registerBlock(blockBombChilling);
    	registerBlock(blockBombBlazing);
    	registerBlock(blockBombBio);
    	registerBlock(blockBombChillingActive);
    	registerBlock(blockBombBlazingActive);
    	registerBlock(blockBombBioActive);
    	registerBlock(blockBombFrame);
    	registerBlock(blockHardenedStone);
    	registerBlock(blockHardenedStoneCompound);
    	registerBlock(blockGlowGenIdle);
    	registerBlock(blockGlowGenActive);
    	registerBlock(blockConduit);
    	registerBlock(blockEnergeticPlate);
    	registerBlock(blockTinkerTable);
    	registerBlock(blockScrappingTable);
    	registerBlock(blockWritingDesk);
    	registerBlock(blockResearchDesk);
    	registerBlock(blockWeldingFrame);
    	registerBlock(blockWeldingPanel);
    	registerBlock(blockSugarShroom);
    	registerBlock(blockFungiBlock);
    	registerBlock(blockFungi);
    	registerBlock(blockFungiGlow);

    	registerItem(itemBlazeIngot);
    	registerItem(itemFirebrandSword);
    	registerItem(itemFirebrandHelmet);
    	registerItem(itemFirebrandPlate);
    	registerItem(itemFirebrandLegs);
    	registerItem(itemFirebrandBoots);
    	registerItem(itemFirebrandShovel);
    	registerItem(itemFirebrandPickaxe);
    	registerItem(itemXeniteSword);
    	registerItem(itemXeniteHelmet);
    	registerItem(itemXeniteVisoredHelmet);
    	registerItem(itemXenitePlate);
    	registerItem(itemXeniteLegs);
    	registerItem(itemXeniteBoots);
    	registerItem(itemXeniteShovel);
    	registerItem(itemXenitePickaxe);
    	registerItem(itemBronzeSword);
    	registerItem(itemBronzeHelmet);
    	registerItem(itemBronzePlate);
    	registerItem(itemBronzeLegs);
    	registerItem(itemBronzeBoots);
    	registerItem(itemBronzeShovel);
    	registerItem(itemBronzePickaxe);
    	
    	registerItem(itemRefinedBlazeDust);
    	registerItem(itemWitherDust);
    	registerItem(itemXeniteIngot);
    	registerItem(itemChain);
    	registerItem(itemAsh);
    	registerItem(itemNotebook);
    	registerItem(itemCompletedNotebook);
    	registerItem(itemThermobicPlate);
    	registerItem(itemBlazingSingularity);
    	registerItem(itemChillingSingularity);
    	registerItem(itemBioSingularity);
    	registerItem(itemBlazingEssence);
    	registerItem(itemChillingEssence);
    	registerItem(itemBioEssence);
    	registerItem(itemLucidCrystal);
    	registerItem(itemChillingCrystal);
    	registerItem(itemBlazingCrystal);
    	registerItem(itemBioCrystal);
    	registerItem(itemBronzeIngot);
    	registerItem(itemCopperIngot);
    	registerItem(itemTinIngot);
    	registerItem(itemCopperDust);
    	registerItem(itemTinDust);
    	registerItem(itemBronzeDust);
    	registerItem(itemHeatVent);
    	registerItem(itemEnergyConverter);
    	registerItem(itemLighter);
    	registerItem(itemFreezer);
    	registerItem(itemBioWand);
    	registerItem(itemEncasedCrystal);
    	registerItem(itemThermobicSheet);
    	registerItem(itemStickODebugging);
    	registerItem(itemConduit);
    	registerItem(itemIncendiaryModule);
    	registerItem(itemBioModule);
    	registerItem(itemBasicWrench);
    	registerItem(itemAugmentedWrench);
    	registerItem(itemTranscendantWrench);
    	registerItem(itemBareMechanicalGolem);
    	registerItem(itemMechanicalGolem);
    	registerItem(itemGolemFrame);
    	registerItem(itemIncompleteMechanicalGolem);
    	registerItem(itemGolemHead);
    	registerItem(itemGolemTorso);
    	registerItem(itemGolemRightArm);
    	registerItem(itemGolemLeftArm);
    	registerItem(itemGolemRightLeg);
    	registerItem(itemGolemLeftLeg);
    	registerItem(itemResearchNotes);
    	registerItem(itemWiring);
    	registerItem(itemGoldWiring);
    	registerItem(itemHyperconductiveWiring);
    	registerItem(itemThinWiring);
    	registerItem(itemThinGoldWiring);
    	registerItem(itemCoil);
    	registerItem(itemSolenoid);
    	registerItem(itemServoMotor);
    	registerItem(itemThermoReactor);
    	registerItem(itemSmallGear);
    	registerItem(itemLargeGear);
    	registerItem(itemGearbox);
    	registerItem(itemBronzeSlice);
    	registerItem(itemGoldSlice);
    	registerItem(itemMultiConnector);
    	registerItem(itemT1Processor);
    	registerItem(itemT2Processor);
    	registerItem(itemT1LogicCard);
    	registerItem(itemT2LogicCard);
    	registerItem(itemFlourescentCube);
    	registerItem(itemT1ProcessingUnit);
    	registerItem(itemT1LogicUnit);
    	registerItem(itemPen);
    	registerItem(itemScrapPaper);
    	registerItem(itemMoss);
    	registerItem(itemFungiSample);
    	registerItem(itemVileSugar);
    	registerItem(itemCleansingSugar);
    	registerItem(itemCreeperEffigy);
    	registerItem(itemCowEffigy);
    	registerItem(itemBacteriaContainer);
    	registerItem(itemUnattainableEnergy);
    	registerItem(itemSpeedUpgrade);
    	registerItem(itemEfficiencyUpgrade);
    	registerItem(itemRangeUpgrade);

    }
	
    @EventHandler
    public void load(FMLInitializationEvent event)
    {
     
		NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandler());
    	
    	proxy.registerEVERYTHING();
    	
    	GameRegistry.registerTileEntity(TileEntityRefinery.class, "tileEntityRefinery");
    	GameRegistry.registerTileEntity(TileEntityConcentrator.class, "tileEntityConcentrator");
    	GameRegistry.registerTileEntity(TileEntityExtractor.class, "tileEntityExtractor");
    	GameRegistry.registerTileEntity(TileEntityInfuser.class, "tileEntityInfuser");
    	GameRegistry.registerTileEntity(TileEntityEnergyBlock.class, "tileEntityEnergyBlock");
    	GameRegistry.registerTileEntity(TileEntityBioGenerator.class, "tileEntityBioGenerator");
    	GameRegistry.registerTileEntity(TileEntityBombChilling.class, "tileEntityBombChilling");
    	GameRegistry.registerTileEntity(TileEntityBombBlazing.class, "tileEntityBombBlazing");
    	GameRegistry.registerTileEntity(TileEntityBombBio.class, "tileEntityBombBio");
    	GameRegistry.registerTileEntity(TileEntityBombChillingActive.class, "tileEntityBombChillingActive");
    	GameRegistry.registerTileEntity(TileEntityBombBlazingActive.class, "tileEntityBombBlazingActive");
    	GameRegistry.registerTileEntity(TileEntityBombBioActive.class, "tileEntityBombBioActive");
    	GameRegistry.registerTileEntity(TileEntityBombFrame.class, "tileEntityBombFrame");
    	GameRegistry.registerTileEntity(TileEntityGlowGenerator.class, "tileEntityGlowGenerator");
    	GameRegistry.registerTileEntity(TileEntityConduit.class, "tileEntityConduit");
    	GameRegistry.registerTileEntity(TileEntityTinkerTable.class, "tileEntityTinkerTable");
    	GameRegistry.registerTileEntity(TileEntityScrappingTable.class, "tileEntityScrappingTable");
    	GameRegistry.registerTileEntity(TileEntityWritingDesk.class, "tileEntityWritingDesk");
    	GameRegistry.registerTileEntity(TileEntityWeldingPanel.class, "tileEntityWeldingPanel");
    	GameRegistry.registerTileEntity(TileEntityResearchDesk.class, "tileEntityResearchDesk");
    	GameRegistry.registerTileEntity(TileEntityAutomatedSpawner.class, "tileEntityAutomatedSpawner");
    	GameRegistry.registerTileEntity(TileEntityBacteriaFarm.class, "tileEntityBacteriaFarm");

    	EntityRegistry.registerGlobalEntityID(EntityMechanicalGolem.class, "MechanicalGolem", EntityRegistry.findGlobalUniqueEntityId());
    	EntityRegistry.registerModEntity(EntityMechanicalGolem.class, "MechanicalGolem", 1, this, 128, 1, false); 
    	
    	EntityRegistry.registerGlobalEntityID(EntityBareMechanicalGolem.class, "BareMechanicalGolem", EntityRegistry.findGlobalUniqueEntityId());
    	EntityRegistry.registerModEntity(EntityBareMechanicalGolem.class, "BareMechanicalGolem", 2, this, 128, 1, false); 
    	
    	EntityRegistry.registerGlobalEntityID(EntityHungry.class, "Hungry", EntityRegistry.findGlobalUniqueEntityId());
    	EntityRegistry.registerModEntity(EntityHungry.class, "Hungry", 3, this, 128, 1, false); 
    	
    	EntityRegistry.registerGlobalEntityID(EntityEvolvedHungry.class, "EvolvedHungry", EntityRegistry.findGlobalUniqueEntityId());
    	EntityRegistry.registerModEntity(EntityEvolvedHungry.class, "EvolvedHungry", 4, this, 128, 1, false); 

    	OreDictionary.registerOre("ingotBlaze", itemBlazeIngot);
    	OreDictionary.registerOre("ingotXenite", itemXeniteIngot);
    	OreDictionary.registerOre("ingotCopper", itemCopperIngot);
    	OreDictionary.registerOre("ingotTin", itemTinIngot);
    	OreDictionary.registerOre("ingotBronze", itemBronzeIngot);
    	OreDictionary.registerOre("oreXenite", blockXeniteOre);
    	OreDictionary.registerOre("oreCrystal", blockCrystalOre);
    	OreDictionary.registerOre("oreCopper", blockCopperOre);
    	OreDictionary.registerOre("oreTin", blockTinOre);
    	OreDictionary.registerOre("dustCopper", itemCopperDust);
    	OreDictionary.registerOre("dustTin", itemTinDust);
    	OreDictionary.registerOre("dustBronze", itemBronzeDust);

    	GameRegistry.registerWorldGenerator(eventmanager, 25);
    }
    
    @EventHandler
	public void postInit(FMLPostInitializationEvent evt)
	{
    	
    	
    	//Item Crafting
    	GameRegistry.addSmelting(blockHardenedStoneCompound, new ItemStack(blockHardenedStone, 1), 20F);
    	GameRegistry.addSmelting(itemCopperDust, new ItemStack(itemCopperIngot, 1), 20F);
    	GameRegistry.addSmelting(itemTinDust, new ItemStack(itemTinIngot, 1), 20F);
    	GameRegistry.addSmelting(itemBronzeDust, new ItemStack(itemBronzeIngot, 1), 20F);
    	GameRegistry.addSmelting(blockXeniteOre, new ItemStack(itemXeniteIngot, 1), 20F);
    	GameRegistry.addSmelting(itemRefinedBlazeDust, new ItemStack(itemBlazeIngot, 1), 20F);
    	GameRegistry.addSmelting(blockTinOre, new ItemStack(itemTinIngot, 1), 20F);
    	GameRegistry.addSmelting(blockCopperOre, new ItemStack(itemCopperIngot, 1), 20F);

    	GameRegistry.addShapelessRecipe(new ItemStack(itemPen, 1), new Object[]{
            itemPen, new ItemStack(Items.dye, 1, 0)
     });
    	GameRegistry.addRecipe(new ItemStack(itemBacteriaContainer, 1, 64), new Object[]{
            "XXX",
            "YYY",
            "XXX", 'X', itemBronzeSlice, 'Y', Blocks.glass
     });
    	GameRegistry.addShapelessRecipe(new ItemStack(itemCleansingSugar, 1), new Object[]{
            Items.sugar, itemRefinedBlazeDust
     });
    	GameRegistry.addShapelessRecipe(new ItemStack(itemVileSugar, 1), new Object[]{
            Items.sugar, itemWitherDust
     });
    	GameRegistry.addRecipe(new ItemStack(itemGolemFrame, 1), new Object[]{
            " Z ",
            "ZYZ",
            " Z ", 'Z', itemBronzeIngot, 'Y', blockMachineBlock
     });
    	GameRegistry.addRecipe(new ItemStack(blockWeldingPanel, 1), new Object[]{
            "ZZ ",
            "ZYZ",
            "X X", 'X', Items.blaze_rod, 'Z', itemBronzeIngot, 'Y', blockMachineBlock
     });
    	GameRegistry.addRecipe(new ItemStack(blockWeldingFrame, 1), new Object[]{
            "ZZZ",
            "X X", 'X', Items.blaze_rod, 'Z', itemBronzeIngot
     });
    	GameRegistry.addRecipe(new ItemStack(blockResearchDesk, 1), new Object[]{
            "ZCZ",
            "XYX",
            "XXX", 'Z', Blocks.stone, 'Y', Blocks.bookshelf, 'X', Items.stick, 'C', Blocks.planks
     });
    	GameRegistry.addRecipe(new ItemStack(itemPen, 1), new Object[]{
            " ZZ",
            "ZYZ",
            "XZ ", 'Z', Init.itemCopperIngot, 'Y', new ItemStack(Items.dye, 1, 0), 'X', Items.iron_ingot
     });
    	GameRegistry.addRecipe(new ItemStack(Items.paper, 1), new Object[]{
            "XX",
            "XX", 'X', Init.itemScrapPaper
     });
    	GameRegistry.addRecipe(new ItemStack(itemMultiConnector, 1), new Object[]{
            "X",
            "Y",
            "X", 'X', Init.itemWiring, 'Y', Init.itemHyperconductiveWiring
     });
    	GameRegistry.addRecipe(new ItemStack(itemSmallGear, 1), new Object[]{
            " X ",
            "XXX",
            " X ", 'X', Init.itemBronzeSlice
     });
    	GameRegistry.addRecipe(new ItemStack(itemLargeGear, 1), new Object[]{
            " X ",
            "XXX",
            " X ", 'X', Init.itemBronzeIngot
     });
    	GameRegistry.addRecipe(new ItemStack(itemServoMotor, 1), new Object[]{
            "ZCZ",
            "ZYZ",
            "XZX", 'X', Init.itemThinWiring, 'Y', Init.itemSolenoid, 'Z', Init.itemBronzeIngot, 'C', Items.stick
     });
    	GameRegistry.addRecipe(new ItemStack(itemSolenoid, 1), new Object[]{
            "XYX", 'X', Init.itemCoil, 'Y', Items.iron_ingot
     });
    	GameRegistry.addRecipe(new ItemStack(itemFlourescentCube, 1), new Object[]{
            "XXX",
            "XYX",
            "XXX", 'X', Blocks.glass_pane, 'Y', Items.glowstone_dust
     });
    	GameRegistry.addRecipe(new ItemStack(itemCoil, 1), new Object[]{
            " X ",
            "X X",
            " X ", 'X', Init.itemThinWiring
     });
    	GameRegistry.addRecipe(new ItemStack(itemHyperconductiveWiring, 1), new Object[]{
            "X",
            "Y",
            "X", 'X', Init.itemWiring, 'Y', Init.itemGoldWiring
     });
    	GameRegistry.addRecipe(new ItemStack(itemWiring, 1), new Object[]{
            "XXX", 'X', Init.itemThinWiring
     });
    	GameRegistry.addRecipe(new ItemStack(itemGoldWiring, 1), new Object[]{
            "XXX", 'X', Init.itemThinGoldWiring
     });
    	GameRegistry.addRecipe(new ItemStack(itemThinWiring, 3), new Object[]{
            "XXX", 'X', Init.itemBronzeSlice
     });
    	GameRegistry.addRecipe(new ItemStack(itemThinGoldWiring, 3), new Object[]{
            "XXX", 'X', Init.itemGoldSlice
     });
    	GameRegistry.addRecipe(new ItemStack(itemGoldSlice, 9), new Object[]{
            "XXX", 'X', Items.gold_ingot
     });
    	GameRegistry.addRecipe(new ItemStack(itemBronzeSlice, 9), new Object[]{
            "XXX", 'X', Init.itemBronzeIngot
     });
    	GameRegistry.addRecipe(new ItemStack(itemNotebook, 1), new Object[]{
            "XY", 'X', Items.book, 'Y', Items.name_tag
     });
    	GameRegistry.addRecipe(new ItemStack(itemEnergyConverter, 2), new Object[]{
            "XYX",
            "XZX",
            "XYX", 'X', Blocks.stone, 'Z', Init.itemLucidCrystal, 'Y', Items.coal
     });
    	GameRegistry.addRecipe(new ItemStack(blockWritingDesk, 1), new Object[]{
            "YXY",
            "YZY",
            "Y Y", 'X', Blocks.planks, 'Z', Items.stick, 'Y', Blocks.stone
     });
    	GameRegistry.addRecipe(new ItemStack(blockScrappingTable, 1), new Object[]{
            "XX",
            "XX", 'X', Init.blockHardenedStone
     });
    	GameRegistry.addRecipe(new ItemStack(blockTinkerTable, 1), new Object[]{
            "Z  ",
            "CYY",
            "XYX", 'X', Items.stick, 'Z', Items.glowstone_dust, 'Y', Init.blockHardenedStone, 'C', Blocks.obsidian
     });
    	GameRegistry.addRecipe(new ItemStack(itemWitherDust, 6), new Object[]{
            "X", 'X', new ItemStack(Items.skull, 1, 1)
     });
    	GameRegistry.addRecipe(new ItemStack(itemBioModule, 1), new Object[]{
            " Y ",
            "XZX",
            "XZX", 'X', Blocks.planks, 'Z', Init.itemEnergyConverter, 'Y', Init.itemBioCrystal
     });
    	GameRegistry.addRecipe(new ItemStack(itemIncendiaryModule, 1), new Object[]{
            "XZX",
            "ZYZ",
            "XZX", 'X', Blocks.planks, 'Z', Init.itemEnergyConverter, 'Y', Init.itemBlazingCrystal
     });
    	GameRegistry.addRecipe(new ItemStack(itemBasicWrench, 1), new Object[]{
            "X X",
            "YZY",
            "YZY", 'X', Blocks.cobblestone, 'Z', Items.stick, 'Y',  Items.leather
     });
    	GameRegistry.addRecipe(new ItemStack(blockEnergeticPlate, 1), new Object[]{
            "XXX",
            "ZZZ", 'X', Blocks.stone_pressure_plate, 'Z', Init.itemLucidCrystal
     });
    	GameRegistry.addRecipe(new ItemStack(itemConduit, 4), new Object[]{
            "XXX",
            "ZZZ",
            "XXX", 'X', Init.itemBronzeIngot, 'Z', Init.itemLucidCrystal
     });
    	GameRegistry.addRecipe(new ItemStack(blockReflector, 1), new Object[]{
            "XYZ", 'X', Init.blockScorchedBrick, 'Y', Blocks.glass, 'Z', Init.itemThermobicSheet
     });
    	GameRegistry.addRecipe(new ItemStack(itemThermobicPlate, 1), new Object[]{
            "XXX",
            "ZYZ",
            "XXX", 'X', Init.itemThermobicSheet, 'Y', Items.slime_ball, 'Z', Init.itemLucidCrystal
     });
    	GameRegistry.addRecipe(new ItemStack(itemThermobicSheet, 2), new Object[]{
            "X",
            'X', blockHardenedStone
     });
    	GameRegistry.addRecipe(new ItemStack(blockHardenedStoneCompound, 1), new Object[]{
            "YXY",
            "XZX",
            "YXY", 'X', Blocks.stone, 'Y', Items.clay_ball, 'Z', Items.iron_ingot
     });
    	GameRegistry.addRecipe(new ItemStack(blockBombFrame, 1), new Object[]{
            " X ",
            "XZX",
            "XYX", 'X', Init.blockHardenedStone, 'Y', Items.redstone, 'Z', Init.itemEncasedCrystal
     });
    	GameRegistry.addRecipe(new ItemStack(itemEncasedCrystal, 1, 16), new Object[]{
            " X ",
            "XYX",
            " X ", 'X', Items.gold_nugget, 'Y', Init.itemLucidCrystal
     });
    	GameRegistry.addRecipe(new ItemStack(blockEnergyBlockIdle, 1), new Object[]{
            "YXY",
            "XZX",
            "YXY", 'X', Blocks.obsidian, 'Y', Items.gold_ingot, 'Z', Init.itemEncasedCrystal
     });
    	GameRegistry.addRecipe(new ItemStack(itemLighter, 1, 16), new Object[]{
            " Z ",
            " Y ",
            " X ", 'X', Items.stick, 'Y', Items.gold_ingot, 'Z', Init.itemBlazingCrystal
     });
    	GameRegistry.addRecipe(new ItemStack(itemFreezer, 1, 16), new Object[]{
            " Z ",
            " Y ",
            " X ", 'X', Items.stick, 'Y', Items.gold_ingot, 'Z', Init.itemChillingCrystal
     });
    	GameRegistry.addRecipe(new ItemStack(itemBioWand, 1, 16), new Object[]{
            " X ",
            " Z ",
            " Y ", 'X', Init.itemBioCrystal, 'Z', Items.gold_ingot, 'Y', Items.stick
     });
    	GameRegistry.addRecipe(new ItemStack(itemHeatVent, 1), new Object[]{
            "XXX",
            "X X",
            "XXX", 'X', Items.iron_ingot
     });
    	GameRegistry.addRecipe(new ItemStack(blockRefineryIdle, 1), new Object[]{
            "XXX",
            "ZYZ",
            "XCX", 'X', itemBronzeIngot, 'Y', blockMachineBlock, 'C', Items.redstone, 'Z', itemHeatVent
     });
    	GameRegistry.addRecipe(new ItemStack(blockExtractorIdle, 1), new Object[]{
            "XXX",
            "ZYZ",
            "XCX", 'X', itemBronzeIngot, 'Y', blockMachineBlock, 'C', Blocks.piston, 'Z', Items.redstone
     });
    	GameRegistry.addRecipe(new ItemStack(blockConcentratorIdle, 1), new Object[]{
            "XXX",
            "ZYZ",
            "XCX", 'X', itemBronzeIngot, 'Y', blockMachineBlock, 'C', Init.itemEncasedCrystal, 'Z', Items.redstone
     });
    	GameRegistry.addRecipe(new ItemStack(blockInfuserIdle, 1), new Object[]{
            "XZX",
            "PYP",
            "XCX", 'X', itemBronzeIngot, 'Y', blockMachineBlock, 'C', Items.gold_ingot, 'Z', Items.redstone, 'P', Items.redstone
     });
    	GameRegistry.addRecipe(new ItemStack(blockMachineBlock, 1), new Object[]{
            "XXX",
            "XXX",
            "XXX", 'X', itemBronzeIngot
     });
    	GameRegistry.addRecipe(new ItemStack(itemBronzeDust, 1), new Object[]{
            "XY ",
            "Y  ",
            'X', itemCopperDust, 'Y', itemTinDust
     });
    	GameRegistry.addShapelessRecipe(new ItemStack(itemCopperDust, 1), new Object[]{
            itemCopperIngot, Blocks.sand
     });
    	GameRegistry.addShapelessRecipe(new ItemStack(itemTinDust, 1), new Object[]{
             itemTinIngot, Blocks.sand
     });
    	GameRegistry.addRecipe(new ItemStack(itemXeniteVisoredHelmet, 1), new Object[]{
            " X ",
            "YYY", 'X', itemXeniteHelmet, 'Y', Blocks.glass_pane
     });
    	GameRegistry.addRecipe(new ItemStack(blockAsh, 1), new Object[]{
            "XXX",
            "XXX",
            "XXX", 'X', itemAsh
     });
    	GameRegistry.addRecipe(new ItemStack(itemXenitePickaxe, 1), new Object[]{
            "XXX",
            " Y ",
            " Y ", 'X', itemXeniteIngot, 'Y', Items.stick
     });
    	GameRegistry.addRecipe(new ItemStack(itemXeniteSword, 1), new Object[]{
            " X ",
            " X ",
            " Y ", 'X', itemXeniteIngot, 'Y', Items.stick
     });
    	GameRegistry.addRecipe(new ItemStack(itemXeniteShovel, 1), new Object[]{
            " X ",
            " Y ",
            " Y ", 'X', itemXeniteIngot, 'Y', Items.stick
     });
    	GameRegistry.addRecipe(new ItemStack(itemXeniteHelmet, 1), new Object[]{
            "XXX",
            "X X", 'X', itemXeniteIngot
     });
    	GameRegistry.addRecipe(new ItemStack(itemXenitePlate, 1), new Object[]{
            "X X",
            "XXX",
            "XXX",'X', itemXeniteIngot
     });
    	GameRegistry.addRecipe(new ItemStack(itemXeniteLegs, 1), new Object[]{
            "XXX",
            "X X",
            "X X",'X', itemXeniteIngot
     });
    	GameRegistry.addRecipe(new ItemStack(itemXeniteBoots, 1), new Object[]{
            "X X",
            "X X", 'X', itemXeniteIngot
     });
    	GameRegistry.addRecipe(new ItemStack(itemFirebrandPickaxe, 1), new Object[]{
            "XXX",
            " Y ",
            " Y ", 'X', itemBlazeIngot, 'Y', Items.blaze_rod
     });
    	GameRegistry.addRecipe(new ItemStack(itemFirebrandSword, 1), new Object[]{
            " X ",
            " X ",
            " Y ", 'X', itemBlazeIngot, 'Y', Items.blaze_rod
     });
    	GameRegistry.addRecipe(new ItemStack(itemFirebrandShovel, 1), new Object[]{
            " X ",
            " Y ",
            " Y ", 'X', itemBlazeIngot, 'Y', Items.blaze_rod
     });
    	GameRegistry.addRecipe(new ItemStack(itemFirebrandHelmet, 1), new Object[]{
            "XXX",
            "X X", 'X', itemBlazeIngot
     });
    	GameRegistry.addRecipe(new ItemStack(itemFirebrandPlate, 1), new Object[]{
            "X X",
            "XXX",
            "XXX",'X', itemBlazeIngot
     });
    	GameRegistry.addRecipe(new ItemStack(itemFirebrandLegs, 1), new Object[]{
            "XXX",
            "X X",
            "X X",'X', itemBlazeIngot
     });
    	GameRegistry.addRecipe(new ItemStack(itemFirebrandBoots, 1), new Object[]{
            "X X",
            "X X", 'X', itemBlazeIngot
     });
    	GameRegistry.addRecipe(new ItemStack(itemBronzePickaxe, 1), new Object[]{
            "XXX",
            " Y ",
            " Y ", 'X', itemBronzeIngot, 'Y', Items.stick
     });
    	GameRegistry.addRecipe(new ItemStack(itemBronzeSword, 1), new Object[]{
            " X ",
            " X ",
            " Y ", 'X', itemBronzeIngot, 'Y', Items.stick
     });
    	GameRegistry.addRecipe(new ItemStack(itemBronzeShovel, 1), new Object[]{
            " X ",
            " Y ",
            " Y ", 'X', itemBronzeIngot, 'Y', Items.stick
     });
    	GameRegistry.addRecipe(new ItemStack(itemBronzeHelmet, 1), new Object[]{
            "XXX",
            "X X", 'X', itemBronzeIngot
     });
    	GameRegistry.addRecipe(new ItemStack(itemBronzePlate, 1), new Object[]{
            "X X",
            "XXX",
            "XXX",'X', itemBronzeIngot
     });
    	GameRegistry.addRecipe(new ItemStack(itemBronzeLegs, 1), new Object[]{
            "XXX",
            "X X",
            "X X",'X', itemBronzeIngot
     });
    	GameRegistry.addRecipe(new ItemStack(itemBronzeBoots, 1), new Object[]{
            "X X",
            "X X", 'X', itemBronzeIngot
     });
    	GameRegistry.addRecipe(new ItemStack(itemChain, 2), new Object[]{
            "X X",
            " X ",
            "X X", 'X', Items.iron_ingot
     });
    	GameRegistry.addRecipe(new ItemStack(blockBlazeTorch, 8), new Object[]{
            " X ",
            " Y ", 'X', Items.coal, 'Y', Items.blaze_rod
     });
    	
    	//Chain Armor Crafting
    	GameRegistry.addRecipe(new ItemStack(Items.chainmail_helmet), new Object[]{
            "XYX",
            "X X", 'X', itemChain, 'Y', Items.leather
     });
    	GameRegistry.addRecipe(new ItemStack(Items.chainmail_chestplate), new Object[]{
            "X X",
            "XYX",
            "XYX", 'X', itemChain, 'Y', Items.leather
     });
    	GameRegistry.addRecipe(new ItemStack(Items.chainmail_leggings), new Object[]{
            "XYX",
            "Y Y",
            "X X", 'X', itemChain, 'Y', Items.leather
     });
    	GameRegistry.addRecipe(new ItemStack(Items.chainmail_boots), new Object[]{
            "Y Y",
            "X X", 'X', itemChain, 'Y', Items.leather
     });

	}

	public void registerItem(Item item){
    	GameRegistry.registerItem(item, modid + item.getUnlocalizedName().substring(5));
	}
	
	public void registerBlock(Block block){
    	GameRegistry.registerBlock(block, modid + block.getUnlocalizedName().substring(5));
	}
}
