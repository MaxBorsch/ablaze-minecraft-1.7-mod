package Ablaze;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class CreativeTabAblazeToolsets extends CreativeTabs {

	public CreativeTabAblazeToolsets(int id, String label) {
		super(id, label);
	}
	
	@Override
	public String getTranslatedTabLabel(){
		return "Ablaze Tools & Armor";
	}
	
	@Override
	public Item getTabIconItem()
	{
		return Init.itemFirebrandSword;
	}
}
