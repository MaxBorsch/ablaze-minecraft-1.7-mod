package Ablaze.item;

import net.minecraft.block.Block;
import net.minecraft.block.IGrowable;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.util.FakePlayerFactory;
import net.minecraftforge.event.entity.player.BonemealEvent;
import Ablaze.Init;
import cpw.mods.fml.common.eventhandler.Event.Result;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemBioWand extends ItemPowered {

    public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10)
    {
		if (!par3World.isRemote){
			applyBonemeal(par1ItemStack, par3World, par4, par5, par6, par2EntityPlayer);
			par1ItemStack.damageItem(1, par2EntityPlayer);
		}
    	return true;
    }
	
    public static boolean applyBonemeal(ItemStack par0ItemStack, World par1World, int par2, int par3, int par4, EntityPlayer player)
    {
        Block l = par1World.getBlock(par2, par3, par4);

        BonemealEvent event = new BonemealEvent(player, par1World, l, par2, par3, par4);

        if (l instanceof IGrowable)
        {
            IGrowable igrowable = (IGrowable)l;

            if (igrowable.func_149851_a(par1World, par2, par3, par4, par1World.isRemote))
            {
                if (!par1World.isRemote)
                {
                    if (igrowable.func_149852_a(par1World, par1World.rand, par2, par3, par4))
                    {
                        igrowable.func_149853_b(par1World, par1World.rand, par2, par3, par4);
                    }
                }

                return true;
            }


        }
		return false;
    }

public static boolean func_150919_a(ItemStack par0ItemStack, World par1World, int p_150919_2_, int p_150919_3_, int p_150919_4_)
{
    if (par1World instanceof WorldServer)
        return applyBonemeal(par0ItemStack, par1World, p_150919_2_, p_150919_3_, p_150919_4_, FakePlayerFactory.getMinecraft((WorldServer)par1World));
    return false;
}

	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister par1IconRegister)
	{
	    this.itemIcon = par1IconRegister.registerIcon(Init.modid + ":" + (this.getUnlocalizedName().substring(5)));

	}
	
}
