package Ablaze.renderer;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import Ablaze.model.ModelBacteriaFarm;
import Ablaze.tileentity.TileEntityBacteriaFarm;

public class ItemRendererBacteriaFarm implements IItemRenderer {

	private ModelBacteriaFarm model;
	private TileEntitySpecialRenderer render;
	
	public ItemRendererBacteriaFarm(TileEntitySpecialRenderer renderer){
		this.model = new ModelBacteriaFarm();
		this.render = renderer;
	}
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		this.render.renderTileEntityAt(new TileEntityBacteriaFarm(), 0.0D, 0.0D, 0.0D, 0.0F);
	}

}
