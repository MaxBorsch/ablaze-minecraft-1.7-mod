package Ablaze.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import Ablaze.Init;
import Ablaze.tileentity.TileEntityBioGenerator;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockBioGenerator extends BlockMachine {
	
	@Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
    {
        if (player.isSneaking())
        {
            return false;
        }
        else
        {
            if (!world.isRemote)
            {
                if (world.getTileEntity(x, y, z) instanceof TileEntityBioGenerator)
                {
                    player.openGui(Init.instance, Init.guiIdBioGenerator, world, x, y, z);
                }
            }

            return true;
        }
    }
	
	@Override
    public int getLightValue(IBlockAccess world, int x, int y, int z)
    {
        if ((world.getTileEntity(x, y, z) instanceof TileEntityBioGenerator) && (((TileEntityBioGenerator) world.getTileEntity(x, y, z)).getState() == 1))
        {
            return 1;
        }

        return super.getLightValue(world, x, y, z);
    }
	
	@Override
	public TileEntity createNewTileEntity(World world, int par2) {
		return new TileEntityBioGenerator();
	}

	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
	    this.blockIcon = par1IconRegister.registerIcon(Init.modid + ":" + "stone");

	}
	
	@Override
    public Item getItemDropped(int par1, Random rand, int par2){
		return Init.itemBioModule;
	}
	
	@Override
    public void breakBlock(World world, int x, int y, int z, Block block, int meta)
    {
        super.breakBlock(world, x, y, z, block, meta);
        world.setBlock(x, y, z, Init.blockEnergeticPlate);
    }
	
}
