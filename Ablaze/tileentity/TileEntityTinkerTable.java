package Ablaze.tileentity;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import Ablaze.Init;

public class TileEntityTinkerTable extends TileEntity implements ISidedInventory {

	private String localizedName;
	private ItemStack[] slots = new ItemStack[12];
	
	public EntityPlayer player = null;
	public boolean hasItem = false;
	public boolean useMenuLeft = false;
	public boolean useMenuRight = false;
	private ItemStack lastItem;
	
	public int rotation = 0;
	
	public int getSizeInventory(){
		return this.slots.length;
	}
	
	public String getInvName(){
		return this.isInvNameLocalized() ? this.localizedName : "container.TinkerTable";
	}
	
	public boolean isInvNameLocalized(){
		return this.localizedName!=null && this.localizedName.length()>0;
	}
	
	public void setGuiDisplayName(String displayName) {
		this.localizedName = displayName;
	}

	@Override
	public ItemStack getStackInSlot(int i) {
		return this.slots[i];
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt){
		super.readFromNBT(nbt);
		
		NBTTagList list = nbt.getTagList("Items", 10);
		this.slots = new ItemStack[this.getSizeInventory()];
		
		for (int i=0; i<list.tagCount(); i++){
			NBTTagCompound compound = (NBTTagCompound) list.getCompoundTagAt(i);
			byte b = compound.getByte("Slot");
			
			if (b>=0 && b<this.slots.length){
				this.slots[b] = ItemStack.loadItemStackFromNBT(compound);
			}
		}

		if (nbt.hasKey("Name")){
			this.localizedName = nbt.getString("Name");
		}
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt){
		super.writeToNBT(nbt);
		
		
		NBTTagList list = new NBTTagList();
		
		for (int i=0; i<this.slots.length; i++){
			if (this.slots[i]!=null){
				NBTTagCompound compound = new NBTTagCompound();
				compound.setByte("Slot", (byte) i);
				this.slots[i].writeToNBT(compound);
				list.appendTag(compound);
			}
		}
		nbt.setTag("Items", list);
		
		if (this.isInvNameLocalized()){
			nbt.setString("Name", this.localizedName);
		}
	}
	
	@Override
	public ItemStack decrStackSize(int i, int j) {
		if (this.slots[i]!=null){
			ItemStack itemStack;
			
			if (this.slots[i].stackSize<=j){
				itemStack = this.slots[i];
				this.slots[i]=null;
				return itemStack;
			}else{
				itemStack = this.slots[i].splitStack(j);
				if (this.slots[i].stackSize==0){
					this.slots[i]=null;
				}
				return itemStack;
			}
		}
		return null;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int i) {
		if (this.slots[i]!=null){
			ItemStack itemStack;
			itemStack = this.slots[i];
			this.slots[i]=null;
			return itemStack;
		}
		return null;
	}

	@Override
	public void setInventorySlotContents(int i, ItemStack itemstack) {
		this.slots[i] = itemstack;
		if (itemstack!=null && itemstack.stackSize>this.getInventoryStackLimit()){
			itemstack.stackSize = this.getInventoryStackLimit();
		}
	}

	@Override
	public int getInventoryStackLimit() {
		return 1;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer) {
		this.player = entityplayer;
		return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord)!=this ? false : entityplayer.getDistanceSq((double) this.xCoord + 0.2D, (double) this.yCoord + 0.2D, (double) this.zCoord + 0.2D)<=100;
	}

	public void updateEntity(){
		boolean invChanged = false;

		if (this.slots[0]!=null && isTinkerable(this.slots[0]) && this.slots[1]!=null){
			this.hasItem = true;
		}else{
			this.hasItem = false;
		}
		
		this.useMenuLeft = ShouldUseMenuLeft();
		this.useMenuRight = ShouldUseMenuRight();
		
		if (!this.worldObj.isRemote){
		if (this.hasItem){
			if (lastItem!=this.slots[0]){
				LoadItemData();
			}else{
				SaveItemData();
			}
		}else{
			if (this.slots[0]!=null){
				EntityItem ite = new EntityItem(this.worldObj,this.xCoord,this.yCoord+1,this.zCoord, this.slots[0]);
				this.worldObj.spawnEntityInWorld(ite);
				this.slots[0] = null;
			}
			for (int i=0; i<6; i++){
				if (lastItem != null){
					this.slots[6 + i] = null;
				}else if (this.slots[6 + i]!=null){
					EntityItem ite = new EntityItem(this.worldObj,this.xCoord,this.yCoord+1,this.zCoord, this.slots[6 + i]);
					this.worldObj.spawnEntityInWorld(ite);
					this.slots[6 + i] = null;
				}
			}
		}
		}
		
		this.lastItem = this.slots[0];
		
		this.rotation = this.worldObj.getBlockMetadata(this.xCoord, this.yCoord, this.zCoord);
	}

	public void SaveItemData(){
		int sCount = 0;
		if (this.slots[0].getItem() == Init.itemGolemFrame || this.slots[0].getItem() == Init.itemIncompleteMechanicalGolem || this.slots[0].getItem() == Init.itemBareMechanicalGolem){
			if (this.slots[0].hasTagCompound() == false){
				NBTTagCompound com = new NBTTagCompound();
				this.slots[0].setTagCompound(com);
			}
			NBTTagCompound ntag = this.slots[0].getTagCompound();
			for (int i=0; i<6; i++){
				if (this.slots[6 + i]!= null && GetItemID(this.slots[6 + i])!=-1){
					if (isItemAccepted(this.slots[6 + i], 6 + i)){
					ntag.setInteger("Item" + i, GetItemID(this.slots[6 + i]));
					sCount++;
					}else{
						EntityItem ite = new EntityItem(this.worldObj,this.xCoord,this.yCoord+1,this.zCoord, this.slots[6 + i]);
						this.worldObj.spawnEntityInWorld(ite);
						this.slots[6 + i] = null;
					}
				}else{
					if (this.slots[6 + i]!= null && GetItemID(this.slots[6 + i])==-1){
						EntityItem ite = new EntityItem(this.worldObj,this.xCoord,this.yCoord+1,this.zCoord, this.slots[6 + i]);
						this.worldObj.spawnEntityInWorld(ite);
						this.slots[6 + i] = null;
					}
					ntag.setInteger("Item" + i, -1);
				}
			}
			
			if (sCount>0){
				if (this.slots[0].getItem() == Init.itemGolemFrame){
					this.slots[0] = new ItemStack(Init.itemIncompleteMechanicalGolem, 1);
				}
				if (this.slots[0].getItem() == Init.itemIncompleteMechanicalGolem && sCount==6){
					this.slots[0] = new ItemStack(Init.itemBareMechanicalGolem, 1);
				}else if (this.slots[0].getItem() == Init.itemBareMechanicalGolem && sCount<=5){
					this.slots[0] = new ItemStack(Init.itemIncompleteMechanicalGolem, 1);
				}
			}else{
				if (this.slots[0].getItem() == Init.itemIncompleteMechanicalGolem){
					this.slots[0] = new ItemStack(Init.itemGolemFrame, 1);
				}
			}
			
			this.slots[0].setTagCompound(ntag);
		}
	}
	
	private boolean isItemAccepted(ItemStack itemStack, int slot) {
		if (this.hasItem){
			if (this.slots[0].getItem() == Init.itemGolemFrame || this.slots[0].getItem() == Init.itemIncompleteMechanicalGolem || this.slots[0].getItem() == Init.itemBareMechanicalGolem){
				if (slot == 6 && itemStack.getItem() == Init.itemGolemHead){
					return true;
				}
				if (slot == 7 && itemStack.getItem() == Init.itemGolemLeftArm){
					return true;
				}
				if (slot == 8 && itemStack.getItem() == Init.itemGolemLeftLeg){
					return true;
				}
				if (slot == 9 && itemStack.getItem() == Init.itemGolemTorso){
					return true;
				}
				if (slot == 10 && itemStack.getItem() == Init.itemGolemRightArm){
					return true;
				}
				if (slot == 11 && itemStack.getItem() == Init.itemGolemRightLeg){
					return true;
				}
			}
		}
		return false;
	}

	public void LoadItemData(){
		if (this.slots[0].getItem() == Init.itemGolemFrame || this.slots[0].getItem() == Init.itemIncompleteMechanicalGolem || this.slots[0].getItem() == Init.itemBareMechanicalGolem){
			if (this.slots[0].hasTagCompound()){
				for (int i=0; i<6; i++){
					NBTTagCompound ntag = this.slots[0].getTagCompound();
					if (ntag.hasKey("Item" + i)){
						if (ntag.getInteger("Item" + i)!=-1){
							this.slots[6 + i] = new ItemStack(GetItemFromID(ntag.getInteger("Item" + i)), 1);
						}else{
							this.slots[6 + i] = null;
						}
					}
				}
			}
		}
	}
	
	public boolean hasItems(ItemStack item){
		if (item.hasTagCompound()){
		for (int i=0; i<6; i++){
			NBTTagCompound ntag = item.getTagCompound();
			if (ntag.hasKey("Item" + i) && ntag.getInteger("Item" + i)!=-1){
				return true;
			}
		}
		}
		return false;
	}
	
	public boolean hasResearch(String research, ItemStack item){
		if (item!=null && item.getItem() == Init.itemNotebook){
			if (item.hasTagCompound() == false){
				NBTTagCompound com = new NBTTagCompound();
				item.setTagCompound(com);
			}
			
			NBTTagCompound tag = item.getTagCompound();
			if (tag.hasKey("owner") && tag.hasKey(research) == true){
				if (tag.getBoolean(research) == true){
					return true;
				}
			}
		}else if (item!=null && item.getItem() == Init.itemCompletedNotebook){
			if (item.hasTagCompound() == false){
				NBTTagCompound com = new NBTTagCompound();
				item.setTagCompound(com);
			}
			NBTTagCompound tag = item.getTagCompound();
			if (tag.hasKey("owner")){
				return true;
			}
		}
		
		return false;
	}
	
	public int GetItemID(ItemStack item){
		if (item.getItem() == Init.itemBlazingCrystal){
			return 0;
		}else if (item.getItem() == Init.itemChillingCrystal){
			return 1;
		}else if (item.getItem() == Init.itemBioCrystal){
			return 2;
		}else if (item.getItem() == Init.itemGolemHead){
			return 3;
		}else if (item.getItem() == Init.itemGolemTorso){
			return 4;
		}else if (item.getItem() == Init.itemGolemRightArm){
			return 5;
		}else if (item.getItem() == Init.itemGolemLeftArm){
			return 6;
		}else if (item.getItem() == Init.itemGolemRightLeg){
			return 7;
		}else if (item.getItem() == Init.itemGolemLeftLeg){
			return 8;
		}
		
		return -1;
	}
	
	public Item GetItemFromID(int ID){
		if (ID == 0){
			return Init.itemBlazingCrystal;
		}else if (ID == 1){
			return Init.itemChillingCrystal;
		}else if (ID == 2){
			return Init.itemBioCrystal;
		}else if (ID == 3){
			return Init.itemGolemHead;
		}else if (ID == 4){
			return Init.itemGolemTorso;
		}else if (ID == 5){
			return Init.itemGolemRightArm;
		}else if (ID == 6){
			return Init.itemGolemLeftArm;
		}else if (ID == 7){
			return Init.itemGolemRightLeg;
		}else if (ID == 8){
			return Init.itemGolemLeftLeg;
		}
		
		return Init.itemAsh;
	}
	
	private boolean ShouldUseMenuLeft() {
		boolean t = false;
		if (this.hasItem && (this.slots[0].getItem() == Init.itemAugmentedWrench)){
			t = true;
		}
		return t;
	}

	private boolean ShouldUseMenuRight() {
		boolean t = false;
		if (this.hasItem && (this.slots[0].getItem() == Init.itemBareMechanicalGolem || this.slots[0].getItem() == Init.itemGolemFrame || this.slots[0].getItem() == Init.itemIncompleteMechanicalGolem)){
			t = true;
		}
		return t;
	}
	
	private boolean isTinkerable(ItemStack itemStack) {
		boolean t = false;
		if (itemStack.getItem() == Init.itemBareMechanicalGolem || this.slots[0].getItem() == Init.itemGolemFrame || this.slots[0].getItem() == Init.itemIncompleteMechanicalGolem || itemStack.getItem() == Init.itemAugmentedWrench){
			if (hasResearch("research" + Item.getIdFromItem(itemStack.getItem()), this.slots[1]) == true){
				t = true;
			}
		}
		return t;
	}

	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack) {
			return false;
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int var1) {
		return null;
	}

	@Override
	public boolean canInsertItem(int i, ItemStack itemstack, int j) {
		return false;
	}

	@Override
	public boolean canExtractItem(int i, ItemStack itemstack, int j) {
		return false;
	}

	@Override
	public String getInventoryName() {
		// TODO Auto-generated method stub
		return "Tinkering Table";
	}

	@Override
	public boolean hasCustomInventoryName() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void openInventory() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void closeInventory() {
		// TODO Auto-generated method stub
		
	}
}
