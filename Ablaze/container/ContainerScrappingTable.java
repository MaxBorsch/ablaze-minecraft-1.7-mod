package Ablaze.container;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import Ablaze.tileentity.TileEntityScrappingTable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.ItemStack;

public class ContainerScrappingTable extends Container {

	private TileEntityScrappingTable ScrappingTable;
	public int lastBurnTime;
	public int lastItemBurnTime;
	
	public ContainerScrappingTable(InventoryPlayer inventory, TileEntityScrappingTable tileentity){
		this.ScrappingTable = tileentity;
		
		this.addSlotToContainer(new Slot(tileentity, 0, 57, 35));
		this.addSlotToContainer(new SlotFurnace(inventory.player, tileentity, 1, 116, 35));

		for (int i=0; i<3; i++){
			for (int j=0; j<9; j++){
				this.addSlotToContainer(new Slot(inventory, j + i*9 + 9, 8 + j*18, 84 + i*18));
			}
		}
		
		for (int i=0; i<9; i++){
			this.addSlotToContainer(new Slot(inventory, i, 8 + i*18, 142));
		}
		
	}
	
	@Override
	public void addCraftingToCrafters(ICrafting icrafting){
		super.addCraftingToCrafters(icrafting);
		icrafting.sendProgressBarUpdate(this, 1, this.ScrappingTable.currentItemBurnTime);
	}
	
	@Override
	public void detectAndSendChanges(){
		super.detectAndSendChanges();
		for (int i=0; i<this.crafters.size(); i++){
			ICrafting icrafting = (ICrafting) this.crafters.get(i);

			
			if (this.lastItemBurnTime != this.ScrappingTable.currentItemBurnTime){
				icrafting.sendProgressBarUpdate(this, 0, this.ScrappingTable.currentItemBurnTime);
			}
			
			this.lastItemBurnTime = this.ScrappingTable.currentItemBurnTime;
			
		}
	}
	
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int slot, int newValue){
		if(slot==0) this.ScrappingTable.currentItemBurnTime = newValue;
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int islot){
		return null;
    }
	
	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return this.ScrappingTable.isUseableByPlayer(entityplayer);
	}

}
