package Ablaze.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelAutomatedSpawner extends ModelBase
{
	
	//fields
    ModelRenderer Base;
    ModelRenderer Shape1;
    ModelRenderer Shape2;
    ModelRenderer Shape3;
    ModelRenderer Shape4;
    ModelRenderer Shape5;
    ModelRenderer Base2;
    ModelRenderer Shape6;
    ModelRenderer Base3;
  
  public ModelAutomatedSpawner()
  {
    textureWidth = 128;
    textureHeight = 64;
    
      Base = new ModelRenderer(this, 98, 8);
      Base.addBox(-2F, 0F, -1F, 4, 5, 2);
      Base.setRotationPoint(0F, 11F, 5F);
      Base.setTextureSize(128, 64);
      Base.mirror = true;
      setRotation(Base, 0F, 0F, 0F);
      Shape1 = new ModelRenderer(this, 17, 38);
      Shape1.addBox(4F, -8F, -8F, 4, 7, 4);
      Shape1.setRotationPoint(0F, 21F, 0F);
      Shape1.setTextureSize(128, 64);
      Shape1.mirror = true;
      setRotation(Shape1, 0F, 0F, 0F);
      Shape2 = new ModelRenderer(this, 98, 0);
      Shape2.addBox(-3F, 0F, -3F, 6, 1, 6);
      Shape2.setRotationPoint(0F, 8F, 0F);
      Shape2.setTextureSize(128, 64);
      Shape2.mirror = true;
      setRotation(Shape2, 0F, 0F, 0F);
      Shape4 = new ModelRenderer(this, 34, 38);
      Shape4.addBox(-8F, -8F, 4F, 4, 6, 4);
      Shape4.setRotationPoint(0F, 22F, 0F);
      Shape4.setTextureSize(128, 64);
      Shape4.mirror = true;
      setRotation(Shape4, 0F, 0F, 0F);
      Shape5 = new ModelRenderer(this, 51, 38);
      Shape5.addBox(4F, -8F, 4F, 4, 5, 4);
      Shape5.setRotationPoint(0F, 23F, 0F);
      Shape5.setTextureSize(128, 64);
      Shape5.mirror = true;
      setRotation(Shape5, 0F, 0F, 0F);
      Shape6 = new ModelRenderer(this, 0, 38);
      Shape6.addBox(-8F, -8F, -8F, 4, 8, 4);
      Shape6.setRotationPoint(0F, 20F, 0F);
      Shape6.setTextureSize(128, 64);
      Shape6.mirror = true;
      setRotation(Shape6, 0F, 0F, 0F);
      Base2 = new ModelRenderer(this, 0, 0);
      Base2.addBox(-8F, -2F, -8F, 16, 4, 16);
      Base2.setRotationPoint(0F, 22F, 0F);
      Base2.setTextureSize(128, 64);
      Base2.mirror = true;
      setRotation(Base2, 0F, 0F, 0F);
      Shape3 = new ModelRenderer(this, 65, 0);
      Shape3.addBox(-4F, -7F, -4F, 8, 11, 8);
      Shape3.setRotationPoint(0F, 16F, 0F);
      Shape3.setTextureSize(128, 64);
      Shape3.mirror = true;
      setRotation(Shape3, 0F, 0F, 0F);
      Base3 = new ModelRenderer(this, 0, 21);
      Base3.addBox(-7F, 0F, -7F, 14, 2, 14);
      Base3.setRotationPoint(0F, 16F, 0F);
      Base3.setTextureSize(128, 64);
      Base3.mirror = true;
      setRotation(Base3, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    Base.render(f5);
    Shape1.render(f5);
    Shape2.render(f5);
    Shape4.render(f5);
    Shape5.render(f5);
    Shape6.render(f5);
    Base2.render(f5);
    Shape3.render(f5);
    Base3.render(f5);
  }
  
  public void renderModel(float f5){
	  Base.render(f5);
	    Shape1.render(f5);
	    Shape2.render(f5);
	    Shape4.render(f5);
	    Shape5.render(f5);
	    Shape6.render(f5);
	    Base2.render(f5);
	    Shape3.render(f5);
	    Base3.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
  }

}
