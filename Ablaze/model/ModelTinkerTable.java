package Ablaze.model;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelTinkerTable extends ModelBase
{
	 //fields
	 ModelRenderer leg2;
	    ModelRenderer base;
	    ModelRenderer base2;
	    ModelRenderer leg1;
	    ModelRenderer arm1;
	    ModelRenderer thingy1;
	    ModelRenderer arm2;
	    ModelRenderer thingy2;
  
  public ModelTinkerTable()
  {
	  textureWidth = 128;
	    textureHeight = 32;
	    
	      leg2 = new ModelRenderer(this, 49, 0);
	      leg2.addBox(5.999F, 0F, -4F, 2, 2, 11);
	      leg2.setRotationPoint(0F, 17F, -3F);
	      leg2.setTextureSize(128, 32);
	      leg2.mirror = true;
	      setRotation(leg2, -0.7853982F, 0F, 0F);
	      base = new ModelRenderer(this, 0, 0);
	      base.addBox(-8F, 0F, -4F, 16, 10, 8);
	      base.setRotationPoint(0F, 14F, 4F);
	      base.setTextureSize(128, 32);
	      base.mirror = true;
	      setRotation(base, 0F, 0F, 0F);
	      base2 = new ModelRenderer(this, 0, 18);
	      base2.addBox(-8F, 0F, -4F, 16, 2, 8);
	      base2.setRotationPoint(0F, 14F, -4F);
	      base2.setTextureSize(128, 32);
	      base2.mirror = true;
	      setRotation(base2, 0F, 0F, 0F);
	      leg1 = new ModelRenderer(this, 48, 0);
	      leg1.addBox(-7.999F, 0F, -4F, 2, 2, 11);
	      leg1.setRotationPoint(0F, 17F, -3F);
	      leg1.setTextureSize(128, 32);
	      leg1.mirror = true;
	      setRotation(leg1, -0.7853982F, 0F, 0F);
	      arm1 = new ModelRenderer(this, 56, 13);
	      arm1.addBox(-1F, 0F, -1F, 2, 8, 2);
	      arm1.setRotationPoint(-6F, 6F, 6F);
	      arm1.setTextureSize(128, 32);
	      arm1.mirror = true;
	      setRotation(arm1, 0F, 0.7853982F, 0F);
	      thingy1 = new ModelRenderer(this, 48, 19);
	      thingy1.addBox(-1.005F, 0F, -1F, 2, 2, 2);
	      thingy1.setRotationPoint(-2F, 3F, 2F);
	      thingy1.setTextureSize(128, 32);
	      thingy1.mirror = true;
	      setRotation(thingy1, 0.7853982F, -0.7853982F, 0F);
	      arm2 = new ModelRenderer(this, 56, 13);
	      arm2.addBox(-1F, 0F, -1F, 2, 8, 2);
	      arm2.setRotationPoint(-2F, 1F, 2F);
	      arm2.setTextureSize(128, 32);
	      arm2.mirror = true;
	      setRotation(arm2, 0.7853982F, -0.7853982F, 0F);
	      thingy2 = new ModelRenderer(this, 48, 13);
	      thingy2.addBox(-1F, 0F, -1.005F, 2, 4, 2);
	      thingy2.setRotationPoint(-5F, 8F, 5F);
	      thingy2.setTextureSize(128, 32);
	      thingy2.mirror = true;
	      setRotation(thingy2, 0F, 0.7853982F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    leg2.render(f5);
    base.render(f5);
    base2.render(f5);
    leg1.render(f5);
    arm1.render(f5);
    thingy1.render(f5);
    arm2.render(f5);
    thingy2.render(f5);
  }
  
  public void renderModel(float f5, int rotation){
	  if (rotation == 5){
		  rotation = 90;
	  }else if (rotation == 4){
		  rotation = -90;
	  }else if (rotation == 3){
		  rotation = 180;
	  }
	  GL11.glPushMatrix();
		GL11.glRotatef(rotation, 0, 1, 0);
		leg2.render(f5);
	    base.render(f5);
	    base2.render(f5);
	    leg1.render(f5);
	    arm1.render(f5);
	    thingy1.render(f5);
	    arm2.render(f5);
	    thingy2.render(f5);
		GL11.glPopMatrix();
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
  }

}
