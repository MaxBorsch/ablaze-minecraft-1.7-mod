package Ablaze.model;

import java.util.Random;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.MathHelper;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelHungry extends ModelBase
{
    private ModelRenderer[] BodyParts = new ModelRenderer[6];
    private static final int[][] BoxLength = new int[][] {{6, 6, 3}, {6, 5, 3}, {6, 4, 3}, {3, 3, 3}, {2, 2, 3}, {2, 1, 2}, {1, 1, 2}};
    private static final int[][] TexturePositions = new int[][] {{0, 0}, {0, 8}, {0, 9}, {0, 16}, {0, 22}, {11, 0}, {13, 4}};

    public ModelHungry()
    {
        float f = -3.5F;

        for (int i = 0; i < this.BodyParts.length; ++i)
        {
            this.BodyParts[i] = new ModelRenderer(this, TexturePositions[i][0], TexturePositions[i][1]);
            this.BodyParts[i].addBox((float)BoxLength[i][0] * -0.5F, 0.0F, (float)BoxLength[i][2] * -0.5F, BoxLength[i][0], BoxLength[i][1], BoxLength[i][2]);
            this.BodyParts[i].setRotationPoint(0.0F, (float)(24 - BoxLength[i][1]), f);

            if (i < this.BodyParts.length - 1)
            {
                f += (float)(BoxLength[i][2] + BoxLength[i + 1][2]) * 0.5F;
            }
        }
    }

    /**
     * Sets the models various rotation angles then renders the model.
     */
    public void render(Entity par1Entity, float par2, float par3, float par4, float par5, float par6, float par7)
    {
        this.setRotationAngles(par2, par3, par4, par5, par6, par7, par1Entity);
        int i;

        for (i = 0; i < this.BodyParts.length; ++i)
        {
            this.BodyParts[i].render(par7);
        }
    }

    public void setRotationAngles(float par1, float par2, float par3, float par4, float par5, float par6, Entity par7Entity)
    {
        for (int i = 0; i < this.BodyParts.length; ++i)
        {
            this.BodyParts[i].rotateAngleY = MathHelper.cos(par3 * 0.2F + (float)i * 0.15F * (float)Math.PI) * (float)Math.PI * 0.05F * (float)(1 + Math.abs(i - 2));
            this.BodyParts[i].rotationPointX = MathHelper.sin(par3 * 0.2F + (float)i * 0.15F * (float)Math.PI) * (float)Math.PI * 0.2F * (float)Math.abs(i - 2);
        }
    }
}