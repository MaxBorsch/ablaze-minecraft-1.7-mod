package Ablaze.tileentity;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import Ablaze.Init;
import Ablaze.block.BlockGlowGenerator;

public class TileEntityGlowGenerator extends TileEntity implements ISidedInventory {

	private String localizedName;
	private ItemStack[] slots = new ItemStack[1];
	private static final int[] slots_top = new int[]{0};
	private static final int[] slots_bottom = new int[]{0, 0};
	private static final int[] slots_side = new int[]{0};
	
	public static boolean generating = false;
	public EntityPlayer player = null;
	public int lastCount = 0;

	public int getSizeInventory(){
		return this.slots.length;
	}
	
	public String getInvName(){
		return this.isInvNameLocalized() ? this.localizedName : "container.GlowGen";
	}
	
	public boolean isInvNameLocalized(){
		return this.localizedName!=null && this.localizedName.length()>0;
	}
	
	public void setGuiDisplayName(String displayName) {
		this.localizedName = displayName;
	}

	@Override
	public ItemStack getStackInSlot(int i) {
		return this.slots[i];
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt){
		super.readFromNBT(nbt);
		
		NBTTagList list = nbt.getTagList("Items", 10);
		this.slots = new ItemStack[this.getSizeInventory()];
		
		for (int i=0; i<list.tagCount(); i++){
			NBTTagCompound compound = (NBTTagCompound) list.getCompoundTagAt(i);
			byte b = compound.getByte("Slot");
			
			if (b>=0 && b<this.slots.length){
				this.slots[b] = ItemStack.loadItemStackFromNBT(compound);
			}
		}

		if (nbt.hasKey("Name")){
			this.localizedName = nbt.getString("Name");
		}
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt){
		super.writeToNBT(nbt);
		
		
		NBTTagList list = new NBTTagList();
		
		for (int i=0; i<this.slots.length; i++){
			if (this.slots[i]!=null){
				NBTTagCompound compound = new NBTTagCompound();
				compound.setByte("Slot", (byte) i);
				this.slots[i].writeToNBT(compound);
				list.appendTag(compound);
			}
		}
		nbt.setTag("Items", list);
		
		if (this.isInvNameLocalized()){
			nbt.setString("Name", this.localizedName);
		}
	}
	
	@Override
	public ItemStack decrStackSize(int i, int j) {
		if (this.slots[i]!=null){
			ItemStack itemStack;
			
			if (this.slots[i].stackSize<=j){
				itemStack = this.slots[i];
				this.slots[i]=null;
				return itemStack;
			}else{
				itemStack = this.slots[i].splitStack(j);
				if (this.slots[i].stackSize==0){
					this.slots[i]=null;
				}
				return itemStack;
			}
		}
		return null;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int i) {
		if (this.slots[i]!=null){
			ItemStack itemStack;
			itemStack = this.slots[i];
			this.slots[i]=null;
			return itemStack;
		}
		return null;
	}

	@Override
	public void setInventorySlotContents(int i, ItemStack itemstack) {
		this.slots[i] = itemstack;
		if (itemstack!=null && itemstack.stackSize>this.getInventoryStackLimit()){
			itemstack.stackSize = this.getInventoryStackLimit();
		}
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer) {
		this.player = entityplayer;
		return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord)!=this ? false : entityplayer.getDistanceSq((double) this.xCoord + 0.2D, (double) this.yCoord + 0.2D, (double) this.zCoord + 0.2D)<=100;
	}

	public void updateEntity(){
		boolean invChanged = false;
		boolean updateBlock = false;
		int count = canGenerate();
		
		if (count>0){
		Random r = new Random();

		if (r.nextInt(64-count)==3){
		if (!(this.worldObj).isRemote){
			if (this.worldObj.getBlock(this.xCoord, this.yCoord-1, this.zCoord)==Init.blockEnergyBlockIdle){
				generating = true;
				TileEntityEnergyBlock te = (TileEntityEnergyBlock) this.worldObj.getTileEntity(this.xCoord, this.yCoord-1, this.zCoord);
				if (te!=null){
					//te.currentEnergy++;
				}
			}else if (this.slots[0]!=null && (this.slots[0].getItem()==Init.itemEncasedCrystal || this.slots[0].getItem()==Init.itemLighter || this.slots[0].getItem()==Init.itemFreezer)){
				generating = true;
				if (this.slots[0].getItemDamage()>0){
					this.slots[0].damageItem(-1, player);
					invChanged=true;
				}
			}else{
				generating=false;
			}
		}
		}
		}else{
			generating=false;
		}

		if (count>lastCount && lastCount==0){
			updateBlock=true;
		}else if (count==0 && lastCount>0){
			updateBlock=true;
		}
		lastCount = count;
		if (updateBlock){
			BlockGlowGenerator.updateState(count>0, this.worldObj, this.xCoord, this.yCoord, this.zCoord);
		}
		
	}

	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack) {
		return i==2 ? false : i==1 ? isItemFuel(itemstack) : true;
	}

	public boolean isItemFuel(ItemStack itemstack) {
		return getItemBurnTime(itemstack)>0;
	}

	private int getItemBurnTime(ItemStack itemstack) {
		return itemstack.getItem()==Items.lava_bucket ? 200 : itemstack.getItem()==Items.coal ? 220 : 0;
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int var1) {
		return var1==0 ? slots_bottom : var1==1 ? slots_top : slots_side;
	}

	@Override
	public boolean canInsertItem(int i, ItemStack itemstack, int j) {
		return this.isItemValidForSlot(i, itemstack);
	}

	@Override
	public boolean canExtractItem(int i, ItemStack itemstack, int j) {
		return j!=0 || i!=1 || itemstack.getItem()==Items.bucket;
	}

	public int canGenerate(){
		int count = 0;
		for (int z=0; z<2; z++){
			for (int x=0; x<2; x++){
				if (isPlant(this.worldObj.getBlock(this.xCoord+x, this.yCoord, this.zCoord+z))){
					count++;
				}
			}
		}
		for (int z=0; z<2; z++){
			for (int x=0; x<2; x++){
				if (isPlant(this.worldObj.getBlock(this.xCoord-x, this.yCoord, this.zCoord+z))){
					count++;
				}
			}
		}
		for (int z=0; z<2; z++){
			for (int x=0; x<2; x++){
				if (isPlant(this.worldObj.getBlock(this.xCoord-x, this.yCoord, this.zCoord-z))){
					count++;
				}
			}
		}
		for (int z=0; z<2; z++){
			for (int x=0; x<2; x++){
				if (isPlant(this.worldObj.getBlock(this.xCoord+x, this.yCoord, this.zCoord-z))){
					count++;
				}
			}
		}
		for (int z=0; z<2; z++){
			for (int x=0; x<2; x++){
				if (isPlant(this.worldObj.getBlock(this.xCoord+x, this.yCoord-1, this.zCoord+z))){
					count++;
				}
			}
		}
		for (int z=0; z<2; z++){
			for (int x=0; x<2; x++){
				if (isPlant(this.worldObj.getBlock(this.xCoord-x, this.yCoord-1, this.zCoord+z))){
					count++;
				}
			}
		}
		for (int z=0; z<2; z++){
			for (int x=0; x<2; x++){
				if (isPlant(this.worldObj.getBlock(this.xCoord-x, this.yCoord-1, this.zCoord-z))){
					count++;
				}
			}
		}
		for (int z=0; z<2; z++){
			for (int x=0; x<2; x++){
				if (isPlant(this.worldObj.getBlock(this.xCoord+x, this.yCoord-1, this.zCoord-z))){
					count++;
				}
			}
		}
		for (int z=0; z<2; z++){
			for (int x=0; x<2; x++){
				if (isPlant(this.worldObj.getBlock(this.xCoord+x, this.yCoord+1, this.zCoord+z))){
					count++;
				}
			}
		}
		for (int z=0; z<2; z++){
			for (int x=0; x<2; x++){
				if (isPlant(this.worldObj.getBlock(this.xCoord-x, this.yCoord+1, this.zCoord+z))){
					count++;
				}
			}
		}
		for (int z=0; z<2; z++){
			for (int x=0; x<2; x++){
				if (isPlant(this.worldObj.getBlock(this.xCoord-x, this.yCoord+1, this.zCoord-z))){
					count++;
				}
			}
		}
		for (int z=0; z<2; z++){
			for (int x=0; x<2; x++){
				if (isPlant(this.worldObj.getBlock(this.xCoord+x, this.yCoord+1, this.zCoord-z))){
					count++;
				}
			}
		}
		return count;
	}
	
	public boolean isPlant(Block id){
		if (id==Blocks.glowstone){
			return true;
		}
		return false;
	}

	@Override
	public String getInventoryName() {
		// TODO Auto-generated method stub
		return "Glow Generator";
	}

	@Override
	public boolean hasCustomInventoryName() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void openInventory() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void closeInventory() {
		// TODO Auto-generated method stub
		
	}
	
}
