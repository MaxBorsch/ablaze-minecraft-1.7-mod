package Ablaze.renderer;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import Ablaze.model.ModelWeldingPanel;
import Ablaze.tileentity.TileEntityWeldingPanel;

public class ItemRendererWeldingPanel implements IItemRenderer {

	private ModelWeldingPanel model;
	private TileEntitySpecialRenderer render;
	
	public ItemRendererWeldingPanel(TileEntitySpecialRenderer renderer){
		this.model = new ModelWeldingPanel();
		this.render = renderer;
	}
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		this.render.renderTileEntityAt(new TileEntityWeldingPanel(), 0.0D, 0.0D, 0.0D, 0.0F);
	}

}
