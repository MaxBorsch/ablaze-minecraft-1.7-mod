package Ablaze.container;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import Ablaze.tileentity.TileEntityRefinery;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.ItemStack;

public class ContainerRefinery extends Container {

	private TileEntityRefinery Refinery;
	public int lastBurnTime;
	public int lastItemBurnTime;
	
	public ContainerRefinery(InventoryPlayer inventory, TileEntityRefinery tileentity){
		this.Refinery = tileentity;
		
		this.addSlotToContainer(new Slot(tileentity, 0, 56, 17));
		this.addSlotToContainer(new Slot(tileentity, 1, 56, 53));
		this.addSlotToContainer(new SlotFurnace(inventory.player, tileentity, 2, 116, 35));
		this.addSlotToContainer(new SlotFurnace(inventory.player, tileentity, 3, 148, 35));

		for (int i=0; i<3; i++){
			for (int j=0; j<9; j++){
				this.addSlotToContainer(new Slot(inventory, j + i*9 + 9, 8 + j*18, 84 + i*18));
			}
		}
		
		for (int i=0; i<9; i++){
			this.addSlotToContainer(new Slot(inventory, i, 8 + i*18, 142));
		}
		
	}
	
	@Override
	public void addCraftingToCrafters(ICrafting icrafting){
		super.addCraftingToCrafters(icrafting);
		icrafting.sendProgressBarUpdate(this, 0, this.Refinery.burnTime);
		icrafting.sendProgressBarUpdate(this, 1, this.Refinery.currentItemBurnTime);
	}
	
	@Override
	public void detectAndSendChanges(){
		super.detectAndSendChanges();
		for (int i=0; i<this.crafters.size(); i++){
			ICrafting icrafting = (ICrafting) this.crafters.get(i);
			
			if (this.lastBurnTime != this.Refinery.burnTime){
				icrafting.sendProgressBarUpdate(this, 0, this.Refinery.burnTime);
			}
			
			if (this.lastItemBurnTime != this.Refinery.currentItemBurnTime){
				icrafting.sendProgressBarUpdate(this, 1, this.Refinery.currentItemBurnTime);
			}
			
			this.lastBurnTime = this.Refinery.burnTime;
			this.lastItemBurnTime = this.Refinery.currentItemBurnTime;
			
		}
	}
	
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int slot, int newValue){
		if(slot==0) this.Refinery.burnTime = newValue;
		if(slot==1) this.Refinery.currentItemBurnTime = newValue;
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int islot){
		return null;
    }
	
	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return this.Refinery.isUseableByPlayer(entityplayer);
	}

}
