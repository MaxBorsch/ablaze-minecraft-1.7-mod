package Ablaze.item;

import java.util.List;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import Ablaze.Init;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemNotebook extends Item {

	public ItemNotebook() {
		this.setMaxStackSize(1);
	}

	 public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
	    {
	    	if (!par2World.isRemote){
	    		if (par1ItemStack.hasTagCompound() == false){
					NBTTagCompound coms = new NBTTagCompound();
					par1ItemStack.setTagCompound(coms);
				}
				NBTTagCompound tag1 = par1ItemStack.getTagCompound();
				if (tag1.hasKey("owner") == false){
				tag1.setString("owner", par3EntityPlayer.getDisplayName());
				par1ItemStack.setTagCompound(tag1);
	    		}
	    	}
	    	return par1ItemStack;
	    }
    
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
    {
        if (par1ItemStack.hasTagCompound())
        {
            NBTTagCompound nbttagcompound = par1ItemStack.getTagCompound();
            String nbttagstring = nbttagcompound.getString("owner");

            if (nbttagstring != null)
            {
                par3List.add(EnumChatFormatting.GRAY + nbttagstring);
            }
        }
    }
	
    /**
     * If this function returns true (or the item is damageable), the ItemStack's NBT tag will be sent to the client.
     */
    public boolean getShareTag()
    {
        return true;
    }
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister par1IconRegister)
	{
	    this.itemIcon = par1IconRegister.registerIcon(Init.modid + ":" + (this.getUnlocalizedName().substring(5)));

	}
	
}
