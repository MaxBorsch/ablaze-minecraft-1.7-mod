package Ablaze.item;

import net.minecraft.block.Block;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import Ablaze.Init;
import Ablaze.block.BlockMachine;
import Ablaze.tileentity.TileEntityMachine;

public class ItemBasicWrench extends ItemBasicItem {

	public ItemBasicWrench() {
		this.setMaxDamage(8);
		this.setMaxStackSize(1);
	}

    public boolean onItemUse(ItemStack itemstack, EntityPlayer player, World world, int x, int y, int z, int par7, float par8, float par9, float par10)
    {
    	if (!world.isRemote){
    		boolean used = false;
    		
	    	if (player.isSneaking()){
		    	if (detachModule(world, x, y, z)) {
		    		used = true;
		    		
		    	} else if (breakMachine(world, x, y, z)) {
		    		used = true;
		    		
		    	}
		    	
	    	}else{
	        	
	    	}
	    	
	    	if (used){
	    		itemstack.damageItem(1, player);
	    	}
    	}
    	
    	return true;
    }
    
    public boolean detachModule(World world, int x, int y, int z) {
    	Block block = world.getBlock(x, y, z);
    	
    	if (block == Init.blockBioGenerator){
    		world.setBlock(x, y, z, Init.blockEnergeticPlate);
    		world.spawnEntityInWorld(new EntityItem(world, x, y, z, new ItemStack(Init.itemBioModule, 1)));
    		
    		return true;
    	}

    	return false;
    }
    
    public boolean breakMachine(World world, int x, int y, int z) {
    	Block block = world.getBlock(x, y, z);
    	TileEntity tileentity = world.getTileEntity(x, y, z);
    	
    	if (block != null && tileentity != null && block instanceof BlockMachine && tileentity instanceof TileEntityMachine){
    		world.setBlockToAir(x, y, z);
    		world.spawnEntityInWorld(new EntityItem(world, x, y, z, new ItemStack(Item.getItemFromBlock(block), 1)));
    		
    		return true;
    	}

    	return false;
    }
	
}
