package Ablaze.container;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import Ablaze.Init;

public class SlotNotebook extends Slot {
   public SlotNotebook(IInventory inventory, int par2, int par3, int par4) {
      super(inventory, par2, par3, par4);
   }

   @Override
   public boolean isItemValid(ItemStack itemstack) {
	   boolean t = false;
	   if (itemstack.getItem() == Init.itemNotebook || itemstack.getItem() == Init.itemCompletedNotebook){
		   t = true;
	   }
      return t;
   }

   @Override
   public int getSlotStackLimit() {
      return 1;
   }
}