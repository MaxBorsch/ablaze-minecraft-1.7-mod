package Ablaze.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackOnCollide;
import net.minecraft.entity.ai.EntityAIMoveTowardsRestriction;
import net.minecraft.entity.ai.EntityAIMoveTowardsTarget;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityGolem;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import Ablaze.Init;
import Ablaze.util.Vector3;

public class EntityHungry extends EntityGolem
{
    private int hunger;
    private Vector3 homeArea;
    
    public EntityHungry(World par1World)
    {
        super(par1World);
        this.setSize(.5F, .9F);
        this.homeArea = new Vector3((int)this.posX, (int)this.posY, (int)this.posZ);
        this.setHomeArea((int)this.posX, (int)this.posY, (int)this.posZ, 5);
        this.getNavigator().setAvoidsWater(true);
        this.tasks.addTask(1, new EntityAIAttackOnCollide(this, 1.0D, true));
        this.tasks.addTask(2, new EntityAIMoveTowardsTarget(this, 0.9D, 32.0F));
        this.tasks.addTask(3, new EntityAIMoveTowardsRestriction(this, 1.0D));
        this.targetTasks.addTask(1, new EntityAINearestAttackableTarget(this, EntityHungry.class, 0, false, true, null));
    }

    protected void entityInit()
    {
        super.entityInit();
        this.dataWatcher.addObject(16, Byte.valueOf((byte)0));
    }
    
    public int getHunger(){
    	return this.dataWatcher.getWatchableObjectByte(16);
    }
    
    /**
     * Returns true if the newer Entity AI code should be run
     */
    public boolean isAIEnabled()
    {
        return true;
    }

    /**
     * main AI tick function, replaces updateEntityActionState
     */
    protected void updateAITick()
    {
        super.updateAITick();
    }

    protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(4.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.15D);

    }

    public void onDeath(DamageSource par1DamageSource)
    {
        super.onDeath(par1DamageSource);
        if (!worldObj.isRemote){
	        if (this.hunger > 1) {
	        	ItemStack itemDrop = new ItemStack(Items.rotten_flesh, 2);
	            EntityItem drop = new EntityItem(worldObj, this.posX, this.posY+1, this.posZ, itemDrop);
	            drop.setPosition(this.posX, this.posY+1, this.posZ);
	            worldObj.spawnEntityInWorld(drop);
	        }
	        if (this.hunger > 2) {
	        	ItemStack itemDrop = new ItemStack(Items.sugar, 1);
	            EntityItem drop = new EntityItem(worldObj, this.posX, this.posY+1, this.posZ, itemDrop);
	            drop.setPosition(this.posX, this.posY+1, this.posZ);
	            worldObj.spawnEntityInWorld(drop);
	        }
	        if (this.hunger > 3) {
	        	ItemStack itemDrop = new ItemStack(Items.leather, 1);
	            EntityItem drop = new EntityItem(worldObj, this.posX, this.posY+1, this.posZ, itemDrop);
	            drop.setPosition(this.posX, this.posY+1, this.posZ);
	            worldObj.spawnEntityInWorld(drop);
	        }
        }
    }
    
    public int isFood(ItemStack itemstack) {
    	if (itemstack == null) { return 0; }
    	
    	if (itemstack.getItem() == Items.apple 
    			|| itemstack.getItem() == Items.potato 
    			|| itemstack.getItem() == Items.beef 
    			|| itemstack.getItem() == Items.bread 
    			|| itemstack.getItem() == Items.carrot 
    			|| itemstack.getItem() == Items.chicken 
    			|| itemstack.getItem() == Items.poisonous_potato 
    			|| itemstack.getItem() == Items.pumpkin_pie 
    			|| itemstack.getItem() == Items.fish 
    			|| itemstack.getItem() == Items.porkchop 
    			|| itemstack.getItem() == Items.melon 
    			|| itemstack.getItem() == Items.fermented_spider_eye) {
    		return 1;
    	}
    	
    	if (itemstack.getItem() == Items.sugar || itemstack.getItem() == Items.cookie) {
    		return 2;
    	}
    	
    	if (itemstack.getItem() == Init.itemVileSugar) {
    		return 11;
    	}
    	
    	return 0;
    }
    
    public boolean interact(EntityPlayer par1EntityPlayer)
    {
    	if (par1EntityPlayer.worldObj.isRemote) { return true; }
    	
        ItemStack itemstack = par1EntityPlayer.inventory.getCurrentItem();
        int foodValue = isFood(itemstack);
        
        if (foodValue == 11) {
    		this.setDead();
    		
    		EntityEvolvedHungry e = new EntityEvolvedHungry(this.worldObj);
    		e.setPosition(this.posX, this.posY, this.posZ);
    		this.worldObj.spawnEntityInWorld(e);
    		
    		return true;
    	}
        
        if (this.hunger < 10 && foodValue > 0)
        {
        	if (!worldObj.isRemote){
        		itemstack.stackSize--;
        		if (itemstack.stackSize <= 0) {
        			itemstack = null;
        		}
        	}
        	
        	this.hunger = Math.min(10, this.hunger + foodValue);
        	this.dataWatcher.updateObject(16, Byte.valueOf((byte)this.hunger));
            return true;
        }
        else
        {
            return super.interact(par1EntityPlayer);
        }
    }
    
    public void setHunger(int hunger) {
    	this.hunger = Math.min(10, hunger);
    	this.dataWatcher.updateObject(16, Byte.valueOf((byte)this.hunger));
    }
    
    /**
     * Decrements the entity's air supply when underwater
     */
    protected int decreaseAirSupply(int par1)
    {
        return par1;
    }

    public boolean attackEntityAsMob(Entity par1Entity)
    {
        boolean flag = par1Entity.attackEntityFrom(DamageSource.causeMobDamage(this), (float)(7 + this.rand.nextInt(15)));
        return flag;
    }
    
    /**
     * Called frequently so the entity can update its state every tick as required. For example, zombies and skeletons
     * use this to react to sunlight and start to burn.
     */
    public void onLivingUpdate()
    {
        super.onLivingUpdate();
    }

    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    public void writeEntityToNBT(NBTTagCompound par1NBTTagCompound)
    {
        super.writeEntityToNBT(par1NBTTagCompound);
        par1NBTTagCompound.setInteger("hunger", this.hunger);
        par1NBTTagCompound.setInteger("hx", (int) this.homeArea.x);
        par1NBTTagCompound.setInteger("hy", (int) this.homeArea.y);
        par1NBTTagCompound.setInteger("hz", (int) this.homeArea.z);
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    public void readEntityFromNBT(NBTTagCompound par1NBTTagCompound)
    {
        super.readEntityFromNBT(par1NBTTagCompound);
        this.hunger = par1NBTTagCompound.getInteger("hunger");
        this.homeArea = new Vector3(par1NBTTagCompound.getInteger("hx"), par1NBTTagCompound.getInteger("hy"), par1NBTTagCompound.getInteger("hz"));
        this.setHomeArea((int) this.homeArea.x, (int) this.homeArea.y, (int) this.homeArea.z, 5);
        this.dataWatcher.updateObject(16, Byte.valueOf((byte)this.hunger));
    }
    
    public void setHome(double par1, double par3, double par5){
    	this.homeArea = new Vector3((int)par1, (int)par3, (int)par5);
        this.setHomeArea((int)par1, (int)par3, (int)par5, 5);
    }
    
    /**
     * Returns the volume for the sounds this mob makes.
     */
    protected float getSoundVolume()
    {
        return 0.2F;
    }
    
    /**
     * Returns the sound this mob makes while it's alive.
     */
    protected String getLivingSound()
    {
        return "mob.silverfish.say";
    }

    /**
     * Returns the sound this mob makes when it is hurt.
     */
    protected String getHurtSound()
    {
        return "mob.slime.small";
    }

    /**
     * Returns the sound this mob makes on death.
     */
    protected String getDeathSound()
    {
        return "mob.silverfish.hurt";
    }

    /**
     * Plays step sound at given x, y, z for the entity
     */
    protected void playStepSound(int par1, int par2, int par3, int par4)
    {
        this.playSound("mob.silverfish.step", 1.0F, 1.0F);
    }

}
