package Ablaze.worldgen;
 
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;
import Ablaze.Init;
 
public class WorldGenPit extends WorldGenerator
{
   public WorldGenPit()
   {
     
   }
   
   public void dig(World world, int x, int y, int z, Block block, Random rand) {
	   boolean flag = false;
	   
	   for (int d=0; d<5; d++) {
		   for (int c=0; c<5; c++) {
			   if (flag) {
				   world.setBlock(x - 2 + d, y, z - 2 + c, (c == 0 || d == 0 || c == 4 || d == 4) ? (rand.nextInt(18) == 0 ? Init.blockFungiGlow : block) : Blocks.air);
			   }
			   flag = true;
		   }
	   }
	   world.setBlock(x, y, z, Blocks.air);
   }
   
   public boolean generate(World world, Random rand, int x, int y, int z)
   {
	  if(world.getBlock(x, y, z)== Blocks.water || world.getBlock(x, y, z)== Blocks.ice || world.getBlock(x, y, z)== Blocks.leaves || world.getBlock(x, y, z)== Blocks.sand || world.getBlock(x, y + 1, z)!= Blocks.air || world.getBlock(x, y + 1, z)== Blocks.log)
	  {
		  return false;
	  }
	  
	  boolean flag = false;
	  for (int i=0; i<35; i++) {
		  if (y > 25) {
			  dig(world, x, y, z, flag==true ? Init.blockFungi : Blocks.air, rand);
			  flag = true;
			  y--;
			  x += (-1 + rand.nextInt(3));
			  z += (-1 + rand.nextInt(3));
		  }
	  }
	  
	  dig(world, x, y, z, Init.blockFungiBlock, rand);
	  
      return true;
   }

}