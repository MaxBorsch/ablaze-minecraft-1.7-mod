package Ablaze.container;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import Ablaze.tileentity.TileEntityEnergyBlock;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.ItemStack;

public class ContainerEnergyBlock extends Container {

	private TileEntityEnergyBlock EnergyBlock;
	public int lastenergy;
	
	public ContainerEnergyBlock(InventoryPlayer inventory, TileEntityEnergyBlock tileentity){
		this.EnergyBlock = tileentity;
		
		this.addSlotToContainer(new Slot(tileentity, 0, 56, 17));
		this.addSlotToContainer(new Slot(tileentity, 1, 56, 45));		

		for (int i=0; i<3; i++){
			for (int j=0; j<9; j++){
				this.addSlotToContainer(new Slot(inventory, j + i*9 + 9, 8 + j*18, 84 + i*18));
			}
		}
		
		for (int i=0; i<9; i++){
			this.addSlotToContainer(new Slot(inventory, i, 8 + i*18, 142));
		}
		
	}
	
	@Override
	public void addCraftingToCrafters(ICrafting icrafting){
		super.addCraftingToCrafters(icrafting);
		icrafting.sendProgressBarUpdate(this, 0, this.EnergyBlock.energy);
	}
	
	@Override
	public void detectAndSendChanges(){
		super.detectAndSendChanges();
		for (int i=0; i<this.crafters.size(); i++){
			ICrafting icrafting = (ICrafting) this.crafters.get(i);
			
			if (this.lastenergy != this.EnergyBlock.energy){
				icrafting.sendProgressBarUpdate(this, 0, this.EnergyBlock.energy);
			}
			
			this.lastenergy = this.EnergyBlock.energy;
			
		}
	}
	
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int slot, int newValue){
		if(slot==0) this.EnergyBlock.energy = newValue;
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int islot){
		return null;
    }
	
	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return this.EnergyBlock.isUseableByPlayer(entityplayer);
	}

}
