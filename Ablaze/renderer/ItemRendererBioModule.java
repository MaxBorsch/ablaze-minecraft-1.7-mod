package Ablaze.renderer;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import Ablaze.model.ModelBioModule;
import Ablaze.tileentity.TileEntityBioModule;

public class ItemRendererBioModule implements IItemRenderer {

	private ModelBioModule model;
	private TileEntitySpecialRenderer render;
	
	public ItemRendererBioModule(TileEntitySpecialRenderer renderer){
		this.model = new ModelBioModule();
		this.render = renderer;
	}
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		this.render.renderTileEntityAt(new TileEntityBioModule(), 0.0D, 0.0D, 0.0D, 0.0F);
	}

}
