package Ablaze.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import Ablaze.Init;
import Ablaze.tileentity.TileEntityResearchDesk;
import cpw.mods.fml.common.network.internal.FMLNetworkHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockResearchDesk extends BlockContainer
{
    private Random random = new Random();
	
    private IIcon workbenchIconTop;
	private IIcon workbenchIconFront;
	
	private static boolean keepInventory;
	
    public BlockResearchDesk()
    {
        super(Material.rock);
    }
	
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ){
		if (!world.isRemote){
			FMLNetworkHandler.openGui(player, Init.instance, Init.guiIdResearchDesk, world, x, y, z);
		}
		return true;
	}
		
	
	@Override
    public void breakBlock(World world, int x, int y, int z, Block var3, int var4){
		if (!keepInventory){
			TileEntityResearchDesk tileentity = (TileEntityResearchDesk) world.getTileEntity(x, y, z);
			if (tileentity!=null){
				for (int i=0; i<tileentity.getSizeInventory(); i++){
					ItemStack itemstack = tileentity.getStackInSlot(i);
					
					if (itemstack!=null){
						float f = random.nextFloat() * 0.8F + 0.1F;
						float f2 = random.nextFloat() * 0.8F + 0.1F;
						float f3 = random.nextFloat() * 0.8F + 0.1F;
						
						while (itemstack.stackSize > 0){
							int j = random.nextInt(21) + 10;
							
							if (j > itemstack.stackSize){
								j = itemstack.stackSize;
							}
							itemstack.stackSize-= j;
							
							EntityItem item = new EntityItem(world, (double)((float)x + f), (double)((float)y + f2), (double)((float)z + f3), new ItemStack(itemstack.getItem(), j, itemstack.getItemDamage()));
						
						if (itemstack.hasTagCompound()){
							item.getEntityItem().setTagCompound((NBTTagCompound) itemstack.getTagCompound().copy());
						}
						
						float f4 = 0.5F;
						item.motionX = (double)((float)this.random.nextGaussian() * f4);
						item.motionY = (double)((float)this.random.nextGaussian() * f4);
						item.motionZ = (double)((float)this.random.nextGaussian() * f4 + 0.2F);
						
						world.spawnEntityInWorld(item);
						}
					}
				}
			}
		}
		super.breakBlock(world, x, y, z, var3, var4);
	}
	
	public void onBlockAdded(World world, int x, int y, int z){
	super.onBlockAdded(world, x, y, z);
	this.setDefaultDirection(world, x, y, z);
	}
	
	private void setDefaultDirection(World world, int x, int y, int z){
		if (!world.isRemote){
		byte b0 = 3;
		Block l = world.getBlock(x, y, z - 1);
		Block il = world.getBlock(x, y, z + 1);
		Block jl = world.getBlock(x - 1, y, z);
		Block kl = world.getBlock(x + 1, y, z);
		
		if (l.isOpaqueCube() && !il.isOpaqueCube()){
			b0 = 3;
		}
		if (il.isOpaqueCube() && !l.isOpaqueCube()){
			b0 = 2;
		}
		if (kl.isOpaqueCube() && !jl.isOpaqueCube()){
			b0 = 5;
		}
		if (jl.isOpaqueCube() && !kl.isOpaqueCube()){
			b0 = 4;
		}
		world.setBlockMetadataWithNotify(x, y, z, b0, 2);
		}
	}
	
	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase elb, ItemStack item){
		int l = MathHelper.floor_double((double) (elb.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
		if (l == 0){
			world.setBlockMetadataWithNotify(x, y, z, 2, 2);
		}
		if (l == 1){
			world.setBlockMetadataWithNotify(x, y, z, 5, 2);

		}
		if (l == 2){
			world.setBlockMetadataWithNotify(x, y, z, 3, 2);

		}
		if (l == 3){
			world.setBlockMetadataWithNotify(x, y, z, 4, 2);

		}
		if (item.hasDisplayName()){
			((TileEntityResearchDesk) world.getTileEntity(x, y, z)).setGuiDisplayName(item.getDisplayName());
		}
	}
	
	@Override
	public boolean hasComparatorInputOverride(){
		return true;
	}
	
	@Override
	public int getComparatorInputOverride(World world, int x, int y, int z, int i){
		return Container.calcRedstoneFromInventory((IInventory) world.getTileEntity(x, y, z));
	}
	
	@Override
	public TileEntity createNewTileEntity(World world, int var2) {
		return new TileEntityResearchDesk();
	}
	
	@SideOnly(Side.CLIENT)
    public IIcon getIcon(int par1, int par2)
    {
        return par1 == 1 ? this.workbenchIconTop : (par1 == 0 ? Blocks.stone.getBlockTextureFromSide(par1) : (par1 != 2 && par1 != 4 ? this.workbenchIconFront : this.workbenchIconFront));
    }
	
	@SideOnly(Side.CLIENT)
	 public void registerBlockIcons(IIconRegister par1IconRegister)
	    {
	        this.blockIcon = par1IconRegister.registerIcon(Init.modid + ":" + "researchdesk_front");
	        this.workbenchIconTop = par1IconRegister.registerIcon(Init.modid + ":" + "researchdesk_top");
	        this.workbenchIconFront = par1IconRegister.registerIcon(Init.modid + ":" + "researchdesk_front");
	    }
	
	/*
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
	    this.blockIcon = par1IconRegister.registerIcon(Init.modid + ":" + "stone");

	}
	
	public boolean isOpaqueCube(){
		return false;
	}
	
	public boolean renderAsNormalBlock(){
		return false;
	}
	
    public int getRenderType()
    {
        return -1;
    }*/
}
