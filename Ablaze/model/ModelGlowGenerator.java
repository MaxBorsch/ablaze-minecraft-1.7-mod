package Ablaze.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelGlowGenerator extends ModelBase
{
	
	public int animFrame=0;
	private boolean animUp = true;
	
	//fields
    ModelRenderer Base;
    ModelRenderer Cube;
    ModelRenderer Shape1;
    ModelRenderer Shape2;
    ModelRenderer Shape3;
    ModelRenderer Shape4;
    ModelRenderer Shape5;
  
  public ModelGlowGenerator()
  {
    textureWidth = 64;
    textureHeight = 32;
    
      Base = new ModelRenderer(this, 0, 0);
      Base.addBox(-6F, 0F, -6F, 12, 1, 12);
      Base.setRotationPoint(0F, 23F, 0F);
      Base.setTextureSize(64, 32);
      Base.mirror = true;
      setRotation(Base, 0F, 0F, 0F);
      Cube = new ModelRenderer(this, 0, 13);
      Cube.addBox(-4F, -4F, -4F, 8, 8, 8);
      Cube.setRotationPoint(0F, 18F, 0F);
      Cube.setTextureSize(64, 32);
      Cube.mirror = true;
      setRotation(Cube, 0F, 0F, 0F);
      Shape1 = new ModelRenderer(this, 32, 21);
      Shape1.addBox(0F, 0F, 0F, 4, 4, 1);
      Shape1.setRotationPoint(-2F, 16F, 4F);
      Shape1.setTextureSize(64, 32);
      Shape1.mirror = true;
      setRotation(Shape1, 0F, 0F, 0F);
      Shape2 = new ModelRenderer(this, 32, 21);
      Shape2.addBox(0F, 0F, -1F, 4, 4, 1);
      Shape2.setRotationPoint(-2F, 16F, -4F);
      Shape2.setTextureSize(64, 32);
      Shape2.mirror = true;
      setRotation(Shape2, 0F, 0F, 0F);
      Shape3 = new ModelRenderer(this, 32, 13);
      Shape3.addBox(0F, 0F, 0F, 1, 4, 4);
      Shape3.setRotationPoint(4F, 16F, -2F);
      Shape3.setTextureSize(64, 32);
      Shape3.mirror = true;
      setRotation(Shape3, 0F, 0F, 0F);
      Shape4 = new ModelRenderer(this, 32, 13);
      Shape4.addBox(-1F, 0F, 0F, 1, 4, 4);
      Shape4.setRotationPoint(-4F, 16F, -2F);
      Shape4.setTextureSize(64, 32);
      Shape4.mirror = true;
      setRotation(Shape4, 0F, 0F, 0F);
      Shape5 = new ModelRenderer(this, 32, 26);
      Shape5.addBox(-2F, 1F, -2F, 4, 1, 4);
      Shape5.setRotationPoint(0F, 12F, 0F);
      Shape5.setTextureSize(64, 32);
      Shape5.mirror = true;
      setRotation(Shape5, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    Base.render(f5);
    Cube.render(f5);
    Shape1.render(f5);
    Shape2.render(f5);
    Shape3.render(f5);
    Shape4.render(f5);
    Shape5.render(f5);
  }
  
  public void renderModel(float f5){
	  if (animFrame<200 && animUp){
		  animFrame++;
		  if (animFrame>=200){
			  animUp=false;
		  }
	  }else if (!animUp){
		  animFrame--;
		  if (animFrame<=0){
			  animUp=true;
		  }
	  }
	  Cube.setRotationPoint(0F, 18F-((float) animFrame/120), 0F);
	  Shape1.setRotationPoint(-2F, 16F-((float) animFrame/120), 4F);
	  Shape2.setRotationPoint(-2F, 16F-((float) animFrame/120), -4F);
	  Shape3.setRotationPoint(4F, 16F-((float) animFrame/120), -2F);
	  Shape4.setRotationPoint(-4F, 16F-((float) animFrame/120), -2F);
	  Shape5.setRotationPoint(0F, 12F-((float) animFrame/120), 0F);

	    Base.render(f5);
	    Cube.render(f5);
	    Shape1.render(f5);
	    Shape2.render(f5);
	    Shape3.render(f5);
	    Shape4.render(f5);
	    Shape5.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
  }

}
