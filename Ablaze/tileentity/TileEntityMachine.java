package Ablaze.tileentity;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.ForgeDirection;

public class TileEntityMachine extends TileEntity
{
    protected ForgeDirection orientation;
    protected byte state;
    protected final String NBT_ORIENTATION = "Orientation";
    protected final String NBT_STATE = "State";

    public TileEntityMachine()
    {
        orientation = ForgeDirection.SOUTH;
        state = 0;
    }

    public ForgeDirection getOrientation()
    {
        return orientation;
    }

    public void setOrientation(ForgeDirection orientation)
    {
        this.orientation = orientation;
    }

    public void setOrientation(int orientation)
    {
        this.orientation = ForgeDirection.getOrientation(orientation);
    }

    public short getState()
    {
        return state;
    }

    public void setState(byte state)
    {
        this.state = state;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbtTagCompound)
    {
        super.readFromNBT(nbtTagCompound);

        if (nbtTagCompound.hasKey(NBT_ORIENTATION))
        {
            this.orientation = ForgeDirection.getOrientation(nbtTagCompound.getByte(NBT_ORIENTATION));
        }

        if (nbtTagCompound.hasKey(NBT_STATE))
        {
            this.state = nbtTagCompound.getByte(NBT_STATE);
        }
    }

    @Override
    public void writeToNBT(NBTTagCompound nbtTagCompound)
    {
        super.writeToNBT(nbtTagCompound);

        nbtTagCompound.setByte(NBT_ORIENTATION, (byte) orientation.ordinal());
        nbtTagCompound.setByte(NBT_STATE, state);
    }

}