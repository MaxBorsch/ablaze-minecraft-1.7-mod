package Ablaze.block;

import java.util.Random;

import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import Ablaze.Init;

public class BlockFungiBlock extends BlockBasicBlock
{
    private Random random = new Random();
		
    public BlockFungiBlock()
    {
        super(Material.rock);
        this.setTickRandomly(true);
    }
    
    @Override
    public Item getItemDropped(int par1, Random rand, int par2){
		return Item.getItemFromBlock(this);
	}
    
    public boolean grow(World world, int x, int y, int z) {
    	if (this.canPlaceBlockAt(world, x, y, z)) {
    		world.setBlock(x, y, z, Init.blockFungi);
    		return true;
    	}
    	
    	return false;
    }
    
    public void updateTick(World world, int x, int y, int z, Random r)
    {        
    	if (world.isRemote) { return; }
    	
    	if (this.canBlockStay(world, x, y, z) && r.nextInt(24) == 0) {
        	for (int d=0; d<3; d++) {
        		for (int f=0; f<3; f++) {
        			for (int g=0; g<3; g++) {
                		grow(world, x-1+d, y-1+f, z-1+g);
                	}	
            	}
        	}
        }
    }
	
	public void onEntityCollidedWithBlock(World par1World, int par2, int par3, int par4, Entity par5Entity)
	 {
		if (par5Entity instanceof EntityPlayer) {
			EntityPlayer player = (EntityPlayer) par5Entity;
			
			player.addPotionEffect(new PotionEffect(Potion.confusion.id, 30, 2));
			player.addPotionEffect(new PotionEffect(Potion.poison.id, 30, 1));
		}
	 }
	
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ){
		player.addPotionEffect(new PotionEffect(Potion.confusion.id, 30, 2));
		player.addPotionEffect(new PotionEffect(Potion.poison.id, 30, 1));
		return true;
	}
	
	@Override
	public boolean canBlockStay(World p_149718_1_, int p_149718_2_, int p_149718_3_, int p_149718_4_)
    {
        return  p_149718_1_.getLightBrightness(p_149718_2_, p_149718_3_, p_149718_4_) < 0.2f;
    }
    
    public boolean canPlaceBlockAt(World world, int x, int y, int z)
    {
        return super.canPlaceBlockAt(world, x, y, z);
    }
    
}
