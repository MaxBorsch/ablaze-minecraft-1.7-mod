package Ablaze.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import Ablaze.Init;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockFungi extends BlockBasicBlock
{
    private Random random = new Random();
		
    public BlockFungi()
    {
        super(Material.web);
        this.setTickRandomly(true);
    }
    
    @Override
    public Item getItemDropped(int par1, Random rand, int par2){
		return Init.itemFungiSample;
	}
    
    public boolean grow(World world, int x, int y, int z) {
    	if (this.canPlaceBlockAt(world, x, y, z)) {
    		world.setBlock(x, y, z, this);
    		return true;
    	}
    	
    	return false;
    }
    
    public void updateTick(World world, int x, int y, int z, Random r)
    {
    	if (world.isRemote) { return; }
    	
    	if (!this.canBlockStay(world, x, y, z)) {
    		this.dropBlockAsItem(world, x, y, z, new ItemStack(this.getItemDropped(0, new Random(), 0), 1));
    		world.setBlock(x, y, z, Blocks.air);
    		
    	} else if (world.getLightBrightness(x, y, z) < 0.2f && r.nextInt(15) == 0) {
    		grow(world, x - 1, y, z);
    		grow(world, x + 1, y, z);
    		grow(world, x, y + 1, z);
    		grow(world, x, y - 1, z);
    		grow(world, x, y, z + 1);
    		grow(world, x, y, z - 1);
        }
    }
    
	public void onEntityCollidedWithBlock(World par1World, int par2, int par3, int par4, Entity par5Entity)
	 {
		if (par5Entity instanceof EntityPlayer) {
			EntityPlayer player = (EntityPlayer) par5Entity;
			
			player.addPotionEffect(new PotionEffect(Potion.confusion.id, 30, 2));
			player.addPotionEffect(new PotionEffect(Potion.poison.id, 30, 1));
		}
	 }
	
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4)
    {
        return null;
    }
	
	@Override
	public boolean canBlockStay(World world, int x, int y, int z)
    {
		boolean flag = false;
		if (world.getBlock(x, y-1, z).getMaterial().blocksMovement()) {
			flag = true;
		} else if (world.getBlock(x, y+1, z).getMaterial().blocksMovement()) {
			flag = true;
		} else if (world.getBlock(x-1, y, z).getMaterial().blocksMovement()) {
			flag = true;
		} else if (world.getBlock(x+1, y, z).getMaterial().blocksMovement()) {
			flag = true;
		} else if (world.getBlock(x, y, z-1).getMaterial().blocksMovement()) {
			flag = true;
		} else if (world.getBlock(x, y, z+1).getMaterial().blocksMovement()) {
			flag = true;
		}
		
        return  flag;
    }
    
    public boolean canPlaceBlockAt(World world, int x, int y, int z)
    {
        return super.canPlaceBlockAt(world, x, y, z) && this.canBlockStay(world, x, y, z);
    }
    
    public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
    {
    	if (!world.isRemote && !this.canBlockStay(world, x, y, z)){
    		this.dropBlockAsItem(world, x, y, z, new ItemStack(this.getItemDropped(0, new Random(), 0), 1));
    		world.setBlock(x, y, z, Blocks.air);
    	}
    }
    
    public boolean isOpaqueCube()
    {
        return false;
    }

    public boolean renderAsNormalBlock()
    {
        return false;
    }

    public int getRenderType()
    {
        return 1;
    }
}
