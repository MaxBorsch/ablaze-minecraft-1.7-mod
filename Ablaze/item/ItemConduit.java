package Ablaze.item;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.ItemReed;
import Ablaze.Init;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemConduit extends ItemReed {

	public ItemConduit() {
		super(Init.blockConduit);
		
	}

	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister par1IconRegister)
	{
	    this.itemIcon = par1IconRegister.registerIcon(Init.modid + ":" + (this.getUnlocalizedName().substring(5)));

	}
	
}
