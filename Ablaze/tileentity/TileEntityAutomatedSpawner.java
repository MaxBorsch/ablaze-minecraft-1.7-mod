package Ablaze.tileentity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraftforge.common.util.ForgeDirection;
import Ablaze.item.ItemMobEffigy;

public class TileEntityAutomatedSpawner extends TileEntityEnergyHandler implements ISidedInventory {

	public static final int INVENTORY_SIZE = 2;
    public static final int INPUT_INVENTORY_INDEX = 0;
    public static final int INPUT2_INVENTORY_INDEX = 1;
    	
	public int maxWork = 100;
	public int work = 0;
	public ItemMobEffigy effigy = null;
	public Entity entity = null;
	public float animation = 0;
	
	private int lastWork = 0;
	private boolean hadEffigy;
	private boolean hadWork;
	private Item lastItem;
	
	private ItemStack[] inventory;
	
	
	public TileEntityAutomatedSpawner() {
		this.alwaysRequest = true;
		this.maxEnergy = 100;
		
		inventory = new ItemStack[INVENTORY_SIZE];
	}
	
	@Override
	public String getInventoryName() {
		return "Automated Spawner";
	}
	
	public int getSizeInventory(){
		return this.inventory.length;
	}

	@Override
	public ItemStack getStackInSlot(int i) {
		return this.inventory[i];
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt){
		super.readFromNBT(nbt);
		
		NBTTagList list = nbt.getTagList("Items", 10);
		inventory = new ItemStack[this.getSizeInventory()];
		
		for (int i=0; i<list.tagCount(); i++){
			NBTTagCompound compound = (NBTTagCompound) list.getCompoundTagAt(i);
			byte b = compound.getByte("Slot");
			
			if (b>=0 && b<inventory.length){
				this.inventory[b] = ItemStack.loadItemStackFromNBT(compound);
			}
		}
		
		this.work = nbt.getShort("work");
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt){
		super.writeToNBT(nbt);
				
		NBTTagList list = new NBTTagList();
		
		for (int i=0; i<inventory.length; i++){
			if (inventory[i]!=null){
				NBTTagCompound compound = new NBTTagCompound();
				compound.setByte("Slot", (byte) i);
				inventory[i].writeToNBT(compound);
				list.appendTag(compound);
			}
		}
		nbt.setTag("Items", list);
		
		nbt.setShort("work", (short) this.work);
	}
	
	public void updateEntity(){
		this.animation += 1f;
		if (this.animation >= 360f) {
			this.animation = 0f;
		}
		
		if (!(this.worldObj).isRemote){
			boolean flag = false;
			boolean flag2 = false;
			boolean update = false;
			
			if (inventory[INPUT_INVENTORY_INDEX] != null 
			&& inventory[INPUT_INVENTORY_INDEX].getItem() instanceof ItemMobEffigy 
			&& inventory[INPUT_INVENTORY_INDEX].getItemDamage() < inventory[INPUT_INVENTORY_INDEX].getMaxDamage()) {
				flag = true;
				
					if (this.work < this.maxWork && this.energy > 0) {
						this.work ++;
						this.energy --;
					} else if (this.work >= this.maxWork) {
						this.work = 0;
						
						Entity entity = ((ItemMobEffigy) inventory[INPUT_INVENTORY_INDEX].getItem()).getEntity(this.worldObj);
						if (entity != null) {						
							entity.setPosition(xCoord, yCoord + 1, zCoord);
							this.worldObj.spawnEntityInWorld(entity);
						}
						
						inventory[INPUT_INVENTORY_INDEX].setItemDamage(inventory[INPUT_INVENTORY_INDEX].getItemDamage() + 1);
						
						if (inventory[INPUT_INVENTORY_INDEX].getItemDamage() >= inventory[INPUT_INVENTORY_INDEX].getMaxDamage()) {
							inventory[INPUT_INVENTORY_INDEX] = null;
						}
					}
			}
			
			if (this.work != this.lastWork) {
				flag2 = true;
			}
			
			if (this.hadEffigy && !flag) {
				update = true;
			} else if (!this.hadEffigy && flag) {
				update = true;
			}
			
			if (this.hadWork && !flag2) {
				update = true;
			} else if (!this.hadWork && flag2) {
				update = true;
			}
			
			
			if (update) {
				markDirty();
				this.state = this.work != this.lastWork ? (byte) 1 : (byte) 0;
				this.worldObj.addBlockEvent(this.xCoord, this.yCoord, this.zCoord, this.getBlockType(), 1, this.state);
				worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
			}
			
			this.lastWork = this.work;
			this.hadEffigy = flag;
			this.hadWork = flag2;
		} else {
			//Client
			if (inventory[INPUT_INVENTORY_INDEX] != null 
					&& inventory[INPUT_INVENTORY_INDEX].getItem() instanceof ItemMobEffigy 
					&& inventory[INPUT_INVENTORY_INDEX].getItemDamage() < inventory[INPUT_INVENTORY_INDEX].getMaxDamage()) {
				
				if (this.entity == null || this.effigy == null || inventory[INPUT_INVENTORY_INDEX].getItem() != lastItem) {
					this.effigy = (ItemMobEffigy) inventory[INPUT_INVENTORY_INDEX].getItem();
					this.entity = ((ItemMobEffigy) inventory[INPUT_INVENTORY_INDEX].getItem()).getEntity(this.worldObj);
				}
				
				lastItem = inventory[INPUT_INVENTORY_INDEX].getItem();
			} else {
				this.effigy = null;
				this.entity = null;
			}
		}
	}
	
	@Override
    public boolean receiveClientEvent(int eventId, int eventData)
    {
        if (eventId == 1)
        {
            this.state = (byte) eventData;
            //this.worldObj.updateAllLightTypes
            this.worldObj.func_147451_t(this.xCoord, this.yCoord, this.zCoord);
            return true;
            
        }
        else 
        {
            return super.receiveClientEvent(eventId, eventData);
        }
    }
	
	@Override
	public Packet getDescriptionPacket()
	{
		NBTTagCompound syncData = new NBTTagCompound();
		NBTTagCompound item = new NBTTagCompound();
		
		if (inventory[INPUT_INVENTORY_INDEX] != null) {
			inventory[INPUT_INVENTORY_INDEX].writeToNBT(item);
			syncData.setTag("Item", item);
		}
		
		return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 1, syncData);
	}
	
	@Override
	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
	{
		if (pkt.func_148857_g().hasKey("Item")) {
			inventory[INPUT_INVENTORY_INDEX] = ItemStack.loadItemStackFromNBT(pkt.func_148857_g().getCompoundTag("Item"));
		} else {
			inventory[INPUT_INVENTORY_INDEX] = null;
		}
	}
	
	@Override
    public ItemStack decrStackSize(int slotIndex, int decrementAmount)
    {
        ItemStack itemStack = getStackInSlot(slotIndex);
        if (itemStack != null)
        {
            if (itemStack.stackSize <= decrementAmount)
            {
                setInventorySlotContents(slotIndex, null);
            }
            else
            {
                itemStack = itemStack.splitStack(decrementAmount);
                if (itemStack.stackSize == 0)
                {
                    setInventorySlotContents(slotIndex, null);
                }
            }
        }

        return itemStack;
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int slotIndex)
    {
        ItemStack itemStack = getStackInSlot(slotIndex);
        if (itemStack != null)
        {
            setInventorySlotContents(slotIndex, null);
        }
        return itemStack;
    }


    @Override
    public void setInventorySlotContents(int slotIndex, ItemStack itemStack)
    {
        inventory[slotIndex] = itemStack;
        if (itemStack != null && itemStack.stackSize > getInventoryStackLimit())
        {
            itemStack.stackSize = getInventoryStackLimit();
        }
    }

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer) {
		return true;
	}
	
	@Override
	public boolean allowInput(ForgeDirection side) {
		return true;
	}
	
	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack) {
		return true;
	}

	@Override
    public int[] getAccessibleSlotsFromSide(int side)
    {
        return side == ForgeDirection.DOWN.ordinal() ? new int[]{INPUT_INVENTORY_INDEX} : new int[]{INPUT_INVENTORY_INDEX, INPUT2_INVENTORY_INDEX};
    }

	@Override
	public boolean canInsertItem(int i, ItemStack itemstack, int j) {
		return this.isItemValidForSlot(i, itemstack);
	}

	@Override
    public boolean canExtractItem(int slotIndex, ItemStack itemStack, int side)
    {
        return slotIndex == INPUT_INVENTORY_INDEX;
    }

	public int getEnergyScaled(int i) {		
		return (int) ((((float) this.energy) / ((float) this.maxEnergy)) * (float) i);
	}
	
	public int getWorkScaled(int i) {				
		return (int) ((((float) this.work) / ((float) this.maxWork)) * (float) i);
	}

	@Override
	public boolean hasCustomInventoryName() {
		return true;
	}

	@Override
	public void openInventory() {}

	@Override
	public void closeInventory() {}

}
