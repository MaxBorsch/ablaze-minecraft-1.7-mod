package Ablaze.item;


public class ItemUpgrade extends ItemBasicItem  {

	
	/**
	 * 0 - Speed
	 * 1 - Efficiency
	 * 2 - Range
	 */
	protected int UPGRADE_TYPE;
	
	/**
	 * Tier of the upgrade. 0 has no effect, 1+ boosts the effect.
	 */
	protected int UPGRADE_TIER;
	

	public ItemUpgrade(int UpgradeType, int UpgradeTier) {
		this.UPGRADE_TYPE = UpgradeType;
		this.UPGRADE_TIER = UpgradeTier;
	}
	
	public int getType() {
		return UPGRADE_TYPE;
	}

	public void setType(int type) {
		UPGRADE_TYPE = type;
	}

	public int getTier() {
		return UPGRADE_TIER;
	}

	public void setTier(int tier) {
		UPGRADE_TIER = tier;
	}
}
