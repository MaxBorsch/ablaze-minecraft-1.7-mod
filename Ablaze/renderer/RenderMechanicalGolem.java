package Ablaze.renderer;

import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import Ablaze.Init;
import Ablaze.entity.EntityMechanicalGolem;
import Ablaze.model.ModelMechanicalGolem;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderMechanicalGolem extends RenderLiving
{
    private static final ResourceLocation mechanicalGolemTextures = new ResourceLocation(Init.modid + ":" + "textures/models/entity/MechanicalGolem.png");

    /** Iron Golem's Model. */
    private final ModelMechanicalGolem mechanicalGolemModel;

    public RenderMechanicalGolem()
    {
        super(new ModelMechanicalGolem(), 0.5F);
        this.mechanicalGolemModel = (ModelMechanicalGolem)this.mainModel;
    }

    /**
     * Renders the Iron Golem.
     */
    public void doRenderMechanicalGolem(EntityMechanicalGolem par1EntityMechanicalGolem, double par2, double par4, double par6, float par8, float par9)
    {
        super.doRender(par1EntityMechanicalGolem, par2, par4, par6, par8, par9);
    }
    
    /**
     * Rotates Iron Golem corpse.
     */
    protected void rotateMechanicalGolemCorpse(EntityMechanicalGolem par1EntityMechanicalGolem, float par2, float par3, float par4)
    {
        super.rotateCorpse(par1EntityMechanicalGolem, par2, par3, par4);

        if ((double)par1EntityMechanicalGolem.limbSwingAmount >= 0.01D)
        {
            float f3 = 13.0F;
            float f4 = par1EntityMechanicalGolem.limbSwing - par1EntityMechanicalGolem.limbSwingAmount * (1.0F - par4) + 6.0F;
            float f5 = (Math.abs(f4 % f3 - f3 * 0.5F) - f3 * 0.25F) / (f3 * 0.25F);
            GL11.glRotatef(6.5F * f5, 0.0F, 0.0F, 1.0F);
        }
    }

    /**
     * Renders Iron Golem Equipped items.
     */
    protected void renderMechanicalGolemEquippedItems(EntityMechanicalGolem par1EntityMechanicalGolem, float par2)
    {
        super.renderEquippedItems(par1EntityMechanicalGolem, par2);
    }

    public void doRenderLiving(EntityLiving par1EntityLiving, double par2, double par4, double par6, float par8, float par9)
    {
        this.doRenderMechanicalGolem((EntityMechanicalGolem)par1EntityLiving, par2, par4, par6, par8, par9);
    }

    protected void renderEquippedItems(EntityLivingBase par1EntityLivingBase, float par2)
    {
        this.renderMechanicalGolemEquippedItems((EntityMechanicalGolem)par1EntityLivingBase, par2);
    }

    protected void rotateCorpse(EntityLivingBase par1EntityLivingBase, float par2, float par3, float par4)
    {
        this.rotateMechanicalGolemCorpse((EntityMechanicalGolem)par1EntityLivingBase, par2, par3, par4);
    }

    public void renderPlayer(EntityLivingBase par1EntityLivingBase, double par2, double par4, double par6, float par8, float par9)
    {
        this.doRenderMechanicalGolem((EntityMechanicalGolem)par1EntityLivingBase, par2, par4, par6, par8, par9);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected ResourceLocation getEntityTexture(Entity par1Entity)
    {
        return mechanicalGolemTextures;
    }

    /**
     * Actually renders the given argument. This is a synthetic bridge method, always casting down its argument and then
     * handing it off to a worker function which does the actual work. In all probabilty, the class Render is generic
     * (Render<T extends Entity) and this method has signature public void doRender(T entity, double d, double d1,
     * double d2, float f, float f1). But JAD is pre 1.5 so doesn't do that.
     */
    public void doRender(Entity par1Entity, double par2, double par4, double par6, float par8, float par9)
    {
        this.doRenderMechanicalGolem((EntityMechanicalGolem)par1Entity, par2, par4, par6, par8, par9);
    }
}
