package Ablaze.block;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import net.minecraftforge.fluids.BlockFluidClassic;
import Ablaze.Init;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockAcidFluid extends BlockFluidClassic {

	public BlockAcidFluid(int id) {
		super(Init.fluidAcid, Init.materialAcid);
		this.setHardness(50.0F);
		this.setLightOpacity(1);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister){
		this.blockIcon = iconRegister.registerIcon(Init.modid + ":" + this.getUnlocalizedName().substring(5));
	}
	
	 public void onEntityCollidedWithBlock(World par1World, int par2, int par3, int par4, Entity par5Entity)
	 {
		 boolean safe = false;
		 if (par5Entity instanceof EntityPlayer){
			 EntityPlayer player = (EntityPlayer) par5Entity;
			 ItemStack ca = player.getCurrentArmor(0);
			 ItemStack ca2 = player.getCurrentArmor(1);
			 ItemStack ca3 = player.getCurrentArmor(2);
			 ItemStack ca4 = player.getCurrentArmor(3);
if (ca!=null && ca2!=null && ca3!=null && ca4!=null){
			 if (ca==new ItemStack(Init.itemXeniteBoots, 1) && ca2==new ItemStack(Init.itemXeniteLegs, 1) && ca3==new ItemStack(Init.itemXenitePlate, 1) && ca4==new ItemStack(Init.itemXeniteVisoredHelmet, 1)){
			 safe = true;
			 }
}
		 }
		 
		 if (!safe){
			 par5Entity.attackEntityFrom(DamageSource.lava, 3);
			 par5Entity.setFire(1);
		 }
		 
	 }
}
