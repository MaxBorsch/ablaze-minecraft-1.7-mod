package Ablaze;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class CreativeTabAblaze extends CreativeTabs {

	public CreativeTabAblaze(int id, String label) {
		super(id, label);
	}
	
	@Override
	public String getTranslatedTabLabel(){
		return "Ablaze";
	}
	
	@Override
	public Item getTabIconItem()
	{
		return Item.getItemFromBlock(Init.blockRefineryActive);
	}
}
