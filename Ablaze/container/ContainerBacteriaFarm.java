package Ablaze.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import Ablaze.tileentity.TileEntityBacteriaFarm;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerBacteriaFarm extends Container {

	private TileEntityBacteriaFarm BacteriaFarm;
	public int lastenergy;
	public int lastwork;
	
	public ContainerBacteriaFarm(InventoryPlayer inventory, TileEntityBacteriaFarm tileentity){
		this.BacteriaFarm = tileentity;
		
		this.addSlotToContainer(new Slot(tileentity, 0, 80, 11));
		this.addSlotToContainer(new Slot(tileentity, 1, 80, 51));		

		for (int i=0; i<3; i++){
			for (int j=0; j<9; j++){
				this.addSlotToContainer(new Slot(inventory, j + i*9 + 9, 8 + j*18, 84 + i*18));
			}
		}
		
		for (int i=0; i<9; i++){
			this.addSlotToContainer(new Slot(inventory, i, 8 + i*18, 142));
		}
		
	}
	
	@Override
	public void addCraftingToCrafters(ICrafting icrafting){
		super.addCraftingToCrafters(icrafting);
		icrafting.sendProgressBarUpdate(this, 0, this.BacteriaFarm.energy);
		icrafting.sendProgressBarUpdate(this, 1, this.BacteriaFarm.work);
	}
	
	@Override
	public void detectAndSendChanges(){
		super.detectAndSendChanges();
				
		for (int i=0; i<this.crafters.size(); i++){
			ICrafting icrafting = (ICrafting) this.crafters.get(i);
			
			if (this.lastenergy != this.BacteriaFarm.energy){
				icrafting.sendProgressBarUpdate(this, 0, this.BacteriaFarm.energy);
			}
			
			this.lastenergy = this.BacteriaFarm.energy;
			
			if (this.lastwork != this.BacteriaFarm.work){
				icrafting.sendProgressBarUpdate(this, 1, this.BacteriaFarm.work);
			}
			
			this.lastwork = this.BacteriaFarm.work;
			
		}
	}
	
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int slot, int newValue){
		if(slot==0) this.BacteriaFarm.energy = newValue;
		if(slot==1) this.BacteriaFarm.work = newValue;
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int islot){
		return null;
    }
	
	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return this.BacteriaFarm.isUseableByPlayer(entityplayer);
	}

}
