package Ablaze.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import Ablaze.Init;
import Ablaze.container.ContainerBacteriaFarm;
import Ablaze.tileentity.TileEntityBacteriaFarm;

public class GuiBacteriaFarm extends GuiContainer {

	public TileEntityBacteriaFarm BacteriaFarm;
	public static final ResourceLocation Bkg = new ResourceLocation(Init.modid, "textures/gui/bacteriafarm.png");
	
	public GuiBacteriaFarm(InventoryPlayer inventory, TileEntityBacteriaFarm entity) {
		super(new ContainerBacteriaFarm(inventory, entity));
		
		this.BacteriaFarm = entity;
		this.xSize = 176;
		this.ySize = 166;
	}

	public void drawGuiContainerForegroundLayer(int par1, int par2){		
	}
	
	public void drawGuiContainerBackgroundLayer(float f, int i, int j) {
		GL11.glColor4f(1F, 1F, 1F, 1F);
		
		Minecraft.getMinecraft().getTextureManager().bindTexture(Bkg);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
	
		int k2 = BacteriaFarm.getEnergyScaled(42);
		drawTexturedModalRect(guiLeft + 116, guiTop + 18, 176, 0, 5, 42 - k2);
		
		int k3 = BacteriaFarm.getWorkScaled(17);
		drawTexturedModalRect(guiLeft + 85, guiTop + 30, 176, 42, 6, 17 - k3);
	
	}

}
