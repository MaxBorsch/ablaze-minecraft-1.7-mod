package Ablaze.item;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelCow;
import net.minecraft.entity.Entity;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

public class ItemMobEffigy extends ItemBasicItem {

	public String message;
	public ModelBase model;
	
	public ItemMobEffigy() {
		this.setMaxDamage(32);
		this.setMaxStackSize(1);
		
		this.message = "Mooo?";
		this.model = new ModelCow();
	}

    public boolean onItemUse(ItemStack itemstack, EntityPlayer player, World world, int par4, int par5, int par6, int par7, float par8, float par9, float par10)
    {
    	player.addChatMessage(new ChatComponentText(this.message));
    	return true;
    }

	public Entity getEntity(World world) {
		return new EntityCow(world);
	}
	
	public void renderModel(Entity entity, double x, double y, double z, float animation) {
		
	}
	
}
