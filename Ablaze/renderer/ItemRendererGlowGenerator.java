package Ablaze.renderer;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import Ablaze.model.ModelGlowGenerator;
import Ablaze.tileentity.TileEntityGlowGenerator;

public class ItemRendererGlowGenerator implements IItemRenderer {

	private ModelGlowGenerator model;
	private TileEntitySpecialRenderer render;
	
	public ItemRendererGlowGenerator(TileEntitySpecialRenderer renderer){
		this.model = new ModelGlowGenerator();
		this.render = renderer;
	}
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		this.render.renderTileEntityAt(new TileEntityGlowGenerator(), 0.0D, 0.0D, 0.0D, 0.0F);
	}

}
