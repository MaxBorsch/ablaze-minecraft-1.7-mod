package Ablaze.tileentity;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.util.ForgeDirection;
import Ablaze.Init;
import Ablaze.block.BlockEnergyBlock;
import Ablaze.item.ItemPowered;

public class TileEntityEnergyBlock extends TileEntityEnergyHandler implements ISidedInventory {

	private String localizedName;
	private ItemStack[] slots = new ItemStack[2];
	private static final int[] slots_top = new int[]{0};
	private static final int[] slots_bottom = new int[]{2, 1};
	private static final int[] slots_side = new int[]{1};
	
	public int CookProgress = 0;
	public int FlameProgress = 0;
		
	public TileEntityEnergyBlock() {
		this.alwaysRequest = true;
		this.maxEnergy = 400;
	}
	
	public int getSizeInventory(){
		return this.slots.length;
	}
	
	public String getInvName(){
		return this.isInvNameLocalized() ? this.localizedName : "container.EnergyBlock";
	}
	
	public boolean isInvNameLocalized(){
		return this.localizedName!=null && this.localizedName.length()>0;
	}
	
	public void setGuiDisplayName(String displayName) {
		this.localizedName = displayName;
	}

	@Override
	public ItemStack getStackInSlot(int i) {
		return this.slots[i];
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt){
		super.readFromNBT(nbt);
		
		NBTTagList list = nbt.getTagList("Items", 10);
		this.slots = new ItemStack[this.getSizeInventory()];
		
		for (int i=0; i<list.tagCount(); i++){
			NBTTagCompound compound = (NBTTagCompound) list.getCompoundTagAt(i);
			byte b = compound.getByte("Slot");
			
			if (b>=0 && b<this.slots.length){
				this.slots[b] = ItemStack.loadItemStackFromNBT(compound);
			}
		}
				
		if (nbt.hasKey("Name")){
			this.localizedName = nbt.getString("Name");
		}
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt){
		super.writeToNBT(nbt);
				
		NBTTagList list = new NBTTagList();
		
		for (int i=0; i<this.slots.length; i++){
			if (this.slots[i]!=null){
				NBTTagCompound compound = new NBTTagCompound();
				compound.setByte("Slot", (byte) i);
				this.slots[i].writeToNBT(compound);
				list.appendTag(compound);
			}
		}
		nbt.setTag("Items", list);
		
		if (this.isInvNameLocalized()){
			nbt.setString("Name", this.localizedName);
		}
	}
	
	@Override
	public ItemStack decrStackSize(int i, int j) {
		if (this.slots[i]!=null){
			ItemStack itemStack;
			
			if (this.slots[i].stackSize<=j){
				itemStack = this.slots[i];
				this.slots[i]=null;
				return itemStack;
			}else{
				itemStack = this.slots[i].splitStack(j);
				if (this.slots[i].stackSize==0){
					this.slots[i]=null;
				}
				return itemStack;
			}
		}
		return null;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int i) {
		if (this.slots[i]!=null){
			ItemStack itemStack;
			itemStack = this.slots[i];
			this.slots[i]=null;
			return itemStack;
		}
		return null;
	}

	@Override
	public void setInventorySlotContents(int i, ItemStack itemstack) {
		this.slots[i] = itemstack;
		if (itemstack!=null && itemstack.stackSize>this.getInventoryStackLimit()){
			itemstack.stackSize = this.getInventoryStackLimit();
		}
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer) {
		return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord)!=this ? false : entityplayer.getDistanceSq((double) this.xCoord + 0.5D, (double) this.yCoord + 0.5D, (double) this.zCoord + 0.5D)<=100;
	}
	
	@Override
	public boolean allowInput(ForgeDirection side) {
		return side == ForgeDirection.UP;
	}
	
	public void updateEntity(){
		boolean updateBlock = false;

		if (!this.worldObj.isRemote) {
			if (this.slots[0]!=null && (this.slots[0].getItem() instanceof ItemPowered || this.slots[0].getItem() == Init.itemUnattainableEnergy) && this.energy<this.maxEnergy){
				if (this.slots[0].getItemDamage()<this.slots[0].getMaxDamage()){
					this.energy++;
					this.slots[0].setItemDamage(this.slots[0].getItemDamage() + 1);
				} else if (this.slots[0].getItem() == Init.itemUnattainableEnergy) {
					this.energy++;
				}
			}
			if (this.slots[1]!=null && (this.slots[1].getItem() instanceof ItemPowered || this.slots[0].getItem() == Init.itemUnattainableEnergy) && this.energy>0){
				if (this.slots[1].getItemDamage()>0){
					this.energy--;
					this.slots[0].setItemDamage(this.slots[0].getItemDamage() - 1);
				} else if (this.slots[0].getItem() == Init.itemUnattainableEnergy) {
					this.energy--;
				}
			}
			
			if (this.random.nextInt(10) == 0) {
				this.updateConnections();
			}

			if (this.energy > 0) {
				if (this.isProviding()) {
					this.sendEnergy(this.getRequesting());
				}
			}
			
			if (this.energy >= this.maxEnergy) {
				this.alwaysRequest = false;
			} else {
				this.alwaysRequest = true;
			}
			
			if(this.energy>this.maxEnergy){
				this.energy = this.maxEnergy;
			}else if(this.energy<0){
				this.energy = 0;
			}
		
			if (updateBlock){
				BlockEnergyBlock.updateState(false, this.worldObj, this.xCoord, this.yCoord, this.zCoord);
			}
		}
	}
	
	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack) {
		return true;
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int var1) {
		return var1==0 ? slots_bottom : var1==1 ? slots_top : slots_side;
	}

	@Override
	public boolean canInsertItem(int i, ItemStack itemstack, int j) {
		return this.isItemValidForSlot(i, itemstack);
	}

	@Override
	public boolean canExtractItem(int i, ItemStack itemstack, int j) {
		return j!=0 || i!=1 || itemstack.getItem()==Items.bucket;
	}

	public int getBurnTimeRemainingScaled(int i) {
		return FlameProgress;
	}

	public int getCookProgressScaled(int i) {
		int One = maxEnergy/i;
		
		if (energy<=0){
			CookProgress = 0;
		}
		if (energy>0 && energy<=One){
			CookProgress = 1;
		}
		if (energy>One && energy<=One*2){
			CookProgress = 2;
		}
		if (energy>One*2 && energy<=One*3){
			CookProgress = 3;
		}
		if (energy>One*3 && energy<=One*4){
			CookProgress = 4;
		}
		if (energy>One*4 && energy<=One*5){
			CookProgress = 5;
		}
		if (energy>One*5 && energy<=One*6){
			CookProgress = 6;
		}
		if (energy>One*6 && energy<=One*7){
			CookProgress = 7;
		}
		if (energy>One*7 && energy<=One*8){
			CookProgress = 8;
		}
		if (energy>One*8 && energy<=One*9){
			CookProgress = 9;
		}
		if (energy>One*9 && energy<=One*10){
			CookProgress = 10;
		}
		if (energy>One*10 && energy<=One*11){
			CookProgress = 11;
		}
		if (energy>One*11 && energy<=One*12){
			CookProgress = 12;
		}
		if (energy>One*12 && energy<=One*13){
			CookProgress = 13;
		}
		if (energy>One*12 && energy<=One*14){
			CookProgress = 14;
		}
		if (energy>One*13 && energy<=One*15){
			CookProgress = 15;
		}
		if (energy>One*14 && energy<=One*16){
			CookProgress = 16;
		}
		if (energy>One*15 && energy<=One*17){
			CookProgress = 17;
		}
		if (energy>One*16 && energy<=One*18){
			CookProgress = 18;
		}
		if (energy>One*17 && energy<=One*19){
			CookProgress = 19;
		}
		if (energy>One*18 && energy<=One*20){
			CookProgress = 20;
		}
		if (energy>One*19 && energy<=One*21){
			CookProgress = 21;
		}
		if (energy>One*20 && energy<=One*22){
			CookProgress = 22;
		}
		if (energy>One*21 && energy<=One*23){
			CookProgress = 23;
		}
		if (energy>One*22 && energy<=One*24){
			CookProgress = 24;
		}
		if (energy>One*23 && energy<=One*25){
			CookProgress = 25;
		}
		if (energy>One*24 && energy<=One*26){
			CookProgress = 26;
		}
		if (energy>One*25 && energy<=One*27){
			CookProgress = 27;
		}
		if (energy>One*26 && energy<=One*28){
			CookProgress = 28;
		}
		if (energy>One*27 && energy<=One*29){
			CookProgress = 29;
		}
		if (energy>One*28 && energy<=One*30){
			CookProgress = 30;
		}
		if (energy>One*29 && energy<=One*31){
			CookProgress = 31;
		}
		if (energy>One*30 && energy<=One*32){
			CookProgress = 32;
		}
		if (energy>One*31 && energy<=One*33){
			CookProgress = 33;
		}
		if (energy>One*32 && energy<=One*34){
			CookProgress = 34;
		}
		if (energy>One*33 && energy<=One*35){
			CookProgress = 35;
		}
		if (energy>One*34 && energy<=One*36){
			CookProgress = 36;
		}
		if (energy>One*35 && energy<=One*37){
			CookProgress = 37;
		}
		if (energy>One*36 && energy<=One*38){
			CookProgress = 38;
		}
		if (energy>One*37 && energy<=One*39){
			CookProgress = 39;
		}
		if (energy>One*38 && energy<=One*40){
			CookProgress = 40;
		}
		if (energy>One*39 && energy<=One*41){
			CookProgress = 41;
		}
		if (energy>One*40){
			CookProgress = 42;
		}
		return CookProgress;
	}

	@Override
	public String getInventoryName() {
		// TODO Auto-generated method stub
		return "Energy Container";
	}

	@Override
	public boolean hasCustomInventoryName() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void openInventory() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void closeInventory() {
		// TODO Auto-generated method stub
		
	}

}
