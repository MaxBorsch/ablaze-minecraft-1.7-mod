package Ablaze.renderer;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import Ablaze.model.ModelTinkerTable;
import Ablaze.tileentity.TileEntityTinkerTable;

public class ItemRendererTinkerTable implements IItemRenderer {

	private ModelTinkerTable model;
	private TileEntitySpecialRenderer render;
	
	public ItemRendererTinkerTable(TileEntitySpecialRenderer renderer){
		this.model = new ModelTinkerTable();
		this.render = renderer;
	}
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		this.render.renderTileEntityAt(new TileEntityTinkerTable(), 0.0D, 0.0D, 0.0D, 0.0F);
	}

}
