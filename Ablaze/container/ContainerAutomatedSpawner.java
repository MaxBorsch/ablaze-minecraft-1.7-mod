package Ablaze.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import Ablaze.tileentity.TileEntityAutomatedSpawner;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerAutomatedSpawner extends Container {

	private TileEntityAutomatedSpawner AutomatedSpawner;
	public int lastenergy;
	public int lastwork;
	
	public ContainerAutomatedSpawner(InventoryPlayer inventory, TileEntityAutomatedSpawner tileentity){
		this.AutomatedSpawner = tileentity;
		
		this.addSlotToContainer(new SlotEffigy(tileentity, 0, 79, 26));
		this.addSlotToContainer(new Slot(tileentity, 1, 79, 55));		

		for (int i=0; i<3; i++){
			for (int j=0; j<9; j++){
				this.addSlotToContainer(new Slot(inventory, j + i*9 + 9, 8 + j*18, 84 + i*18));
			}
		}
		
		for (int i=0; i<9; i++){
			this.addSlotToContainer(new Slot(inventory, i, 8 + i*18, 142));
		}
		
	}
	
	@Override
	public void addCraftingToCrafters(ICrafting icrafting){
		super.addCraftingToCrafters(icrafting);
		icrafting.sendProgressBarUpdate(this, 0, this.AutomatedSpawner.energy);
		icrafting.sendProgressBarUpdate(this, 1, this.AutomatedSpawner.work);
	}
	
	@Override
	public void detectAndSendChanges(){
		super.detectAndSendChanges();
				
		for (int i=0; i<this.crafters.size(); i++){
			ICrafting icrafting = (ICrafting) this.crafters.get(i);
			
			if (this.lastenergy != this.AutomatedSpawner.energy){
				icrafting.sendProgressBarUpdate(this, 0, this.AutomatedSpawner.energy);
			}
			
			this.lastenergy = this.AutomatedSpawner.energy;
			
			if (this.lastwork != this.AutomatedSpawner.work){
				icrafting.sendProgressBarUpdate(this, 1, this.AutomatedSpawner.work);
			}
			
			this.lastwork = this.AutomatedSpawner.work;
			
		}
	}
	
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int slot, int newValue){
		if(slot==0) this.AutomatedSpawner.energy = newValue;
		if(slot==1) this.AutomatedSpawner.work = newValue;
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int islot){
		return null;
    }
	
	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return this.AutomatedSpawner.isUseableByPlayer(entityplayer);
	}

}
