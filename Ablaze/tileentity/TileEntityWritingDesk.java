package Ablaze.tileentity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import Ablaze.Init;

public class TileEntityWritingDesk extends TileEntity implements ISidedInventory {

	private String localizedName;
	private ItemStack[] slots = new ItemStack[3];
	
	public int furnaceSpeed = 300;
	public int currentItemBurnTime = 0; //The currently burning items progress
	
	public int CookProgress = 0;
	public int rotation = 0;
	
	public int getSizeInventory(){
		return this.slots.length;
	}
	
	public String getInvName(){
		return this.isInvNameLocalized() ? this.localizedName : "container.WritingDesk";
	}
	
	public boolean isInvNameLocalized(){
		return this.localizedName!=null && this.localizedName.length()>0;
	}
	
	public void setGuiDisplayName(String displayName) {
		this.localizedName = displayName;
	}

	@Override
	public ItemStack getStackInSlot(int i) {
		return this.slots[i];
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt){
		super.readFromNBT(nbt);
		
		NBTTagList list = nbt.getTagList("Items", 10);
		this.slots = new ItemStack[this.getSizeInventory()];
		
		for (int i=0; i<list.tagCount(); i++){
			NBTTagCompound compound = (NBTTagCompound) list.getCompoundTagAt(i);
			byte b = compound.getByte("Slot");
			
			if (b>=0 && b<this.slots.length){
				this.slots[b] = ItemStack.loadItemStackFromNBT(compound);
			}
		}
		
		this.currentItemBurnTime = nbt.getShort("currentItemBurnTime");
		
		if (nbt.hasKey("Name")){
			this.localizedName = nbt.getString("Name");
		}
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt){
		super.writeToNBT(nbt);
		
		nbt.setShort("currentItemBurnTime", (short) currentItemBurnTime);
		
		NBTTagList list = new NBTTagList();
		
		for (int i=0; i<this.slots.length; i++){
			if (this.slots[i]!=null){
				NBTTagCompound compound = new NBTTagCompound();
				compound.setByte("Slot", (byte) i);
				this.slots[i].writeToNBT(compound);
				list.appendTag(compound);
			}
		}
		nbt.setTag("Items", list);
		
		if (this.isInvNameLocalized()){
			nbt.setString("Name", this.localizedName);
		}
	}
	
	@Override
	public ItemStack decrStackSize(int i, int j) {
		if (this.slots[i]!=null){
			ItemStack itemStack;
			
			if (this.slots[i].stackSize<=j){
				itemStack = this.slots[i];
				this.slots[i]=null;
				return itemStack;
			}else{
				itemStack = this.slots[i].splitStack(j);
				if (this.slots[i].stackSize==0){
					this.slots[i]=null;
				}
				return itemStack;
			}
		}
		return null;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int i) {
		if (this.slots[i]!=null){
			ItemStack itemStack;
			itemStack = this.slots[i];
			this.slots[i]=null;
			return itemStack;
		}
		return null;
	}

	@Override
	public void setInventorySlotContents(int i, ItemStack itemstack) {
		this.slots[i] = itemstack;
		if (itemstack!=null && itemstack.stackSize>this.getInventoryStackLimit()){
			itemstack.stackSize = this.getInventoryStackLimit();
		}
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer) {
		return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord)!=this ? false : entityplayer.getDistanceSq((double) this.xCoord + 0.5D, (double) this.yCoord + 0.5D, (double) this.zCoord + 0.5D)<=100;
	}

	public void updateEntity(){
		boolean invChanged = false;
		
		if (!(this.worldObj).isRemote){
			if (this.slots[0]!=null && this.slots[1]!=null && isWritable(this.slots[1]) && this.slots[2]==null){
				this.currentItemBurnTime++;
				if (this.currentItemBurnTime>=this.furnaceSpeed){
					WriteItem();
					this.currentItemBurnTime = 0;
				}
			}else{
				this.currentItemBurnTime = 0;
			}
		}
		
		this.rotation = this.worldObj.getBlockMetadata(this.xCoord, this.yCoord, this.zCoord);
		
	}

	private boolean isWritable(ItemStack itemStack) {
		if (this.slots[0]!=null && this.slots[0].getItem() == Init.itemNotebook && this.slots[1].getItem() == Init.itemResearchNotes && this.slots[1].hasTagCompound()){
			if (this.slots[0].hasTagCompound() == false){
				NBTTagCompound com = new NBTTagCompound();
				this.slots[0].setTagCompound(com);
			}
			
			NBTTagCompound tag = this.slots[0].getTagCompound();
			if (tag.hasKey("owner") && !tag.hasKey(itemStack.getTagCompound().getString("research"))){
					return true;
			}
		}
		return false;
	}

	private void WriteItem() {
		NBTTagCompound tag = this.slots[0].getTagCompound();
		tag.setBoolean(this.slots[1].getTagCompound().getString("research"), true);
		this.slots[1] = null;
		this.slots[0] = null;
		ItemStack newItem = new ItemStack(Init.itemNotebook, 1);
		newItem.setTagCompound(tag);
		this.slots[2] = newItem;
	}

	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack) {
		return false;
	}

	@Override
	public boolean canInsertItem(int i, ItemStack itemstack, int j) {
		return false;
	}

	@Override
	public boolean canExtractItem(int i, ItemStack itemstack, int j) {
		return false;
	}

	public int getCookProgressScaled(int i) {
		int One = furnaceSpeed/i;
		
		if (currentItemBurnTime<=0){
			CookProgress = 0;
		}
		if (currentItemBurnTime>0 && currentItemBurnTime<=One){
			CookProgress = 1;
		}
		if (currentItemBurnTime>One && currentItemBurnTime<=One*2){
			CookProgress = 2;
		}
		if (currentItemBurnTime>One*2 && currentItemBurnTime<=One*3){
			CookProgress = 3;
		}
		if (currentItemBurnTime>One*3 && currentItemBurnTime<=One*4){
			CookProgress = 4;
		}
		if (currentItemBurnTime>One*4 && currentItemBurnTime<=One*5){
			CookProgress = 5;
		}
		if (currentItemBurnTime>One*5 && currentItemBurnTime<=One*6){
			CookProgress = 6;
		}
		if (currentItemBurnTime>One*6 && currentItemBurnTime<=One*7){
			CookProgress = 7;
		}
		if (currentItemBurnTime>One*7 && currentItemBurnTime<=One*8){
			CookProgress = 8;
		}
		if (currentItemBurnTime>One*8 && currentItemBurnTime<=One*9){
			CookProgress = 9;
		}
		if (currentItemBurnTime>One*9 && currentItemBurnTime<=One*10){
			CookProgress = 10;
		}
		if (currentItemBurnTime>One*10 && currentItemBurnTime<=One*11){
			CookProgress = 11;
		}
		if (currentItemBurnTime>One*11 && currentItemBurnTime<=One*12){
			CookProgress = 12;
		}
		if (currentItemBurnTime>One*12 && currentItemBurnTime<=One*13){
			CookProgress = 13;
		}
		if (currentItemBurnTime>One*12 && currentItemBurnTime<=One*14){
			CookProgress = 14;
		}
		if (currentItemBurnTime>One*13 && currentItemBurnTime<=One*15){
			CookProgress = 15;
		}
		if (currentItemBurnTime>One*14 && currentItemBurnTime<=One*16){
			CookProgress = 16;
		}
		if (currentItemBurnTime>One*15 && currentItemBurnTime<=One*17){
			CookProgress = 17;
		}
		if (currentItemBurnTime>One*16 && currentItemBurnTime<=One*18){
			CookProgress = 18;
		}
		if (currentItemBurnTime>One*17 && currentItemBurnTime<=One*19){
			CookProgress = 19;
		}
		if (currentItemBurnTime>One*18 && currentItemBurnTime<=One*20){
			CookProgress = 20;
		}
		if (currentItemBurnTime>One*19 && currentItemBurnTime<=One*21){
			CookProgress = 21;
		}
		if (currentItemBurnTime>One*20 && currentItemBurnTime<=One*22){
			CookProgress = 22;
		}
		if (currentItemBurnTime>One*21 && currentItemBurnTime<=One*23){
			CookProgress = 23;
		}
		if (currentItemBurnTime>One*22 && currentItemBurnTime<=One*24){
			CookProgress = 24;
		}
		return CookProgress;
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int var1) {
		return null;
	}

	@Override
	public String getInventoryName() {
		return "Writing Desk";
	}

	@Override
	public boolean hasCustomInventoryName() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void openInventory() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void closeInventory() {
		// TODO Auto-generated method stub
		
	}

}
