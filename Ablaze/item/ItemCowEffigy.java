package Ablaze.item;

import net.minecraft.client.model.ModelCow;
import net.minecraft.entity.Entity;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.world.World;

import org.lwjgl.opengl.GL11;

public class ItemCowEffigy extends ItemMobEffigy {
		
	public ItemCowEffigy() {
		this.message = "Moooo?";
		this.model = new ModelCow();
	}

	public Entity getEntity(World world) {
		return new EntityCow(world);
	}
	
	@Override
	public void renderModel(Entity entity, double x, double y, double z, float animation) {
		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glTranslatef((float) x + 0.5F, (float)y + 2F, (float)z + 0.5F);
		GL11.glRotatef(180, 1, 0, 0);
		GL11.glRotatef(animation, 0, 1, 0);
		GL11.glScalef(0.5f, 0.5f, 0.5f);
		this.model.render(entity, 2f, 1f, 1f, 1f, 1f, 0.0625F);
		GL11.glPopMatrix();
	}
	
}
