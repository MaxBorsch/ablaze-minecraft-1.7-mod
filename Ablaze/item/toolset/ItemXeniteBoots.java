package Ablaze.item.toolset;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import Ablaze.Init;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemXeniteBoots extends ItemArmor {

	public ItemXeniteBoots(ArmorMaterial par2EnumArmorMaterial, int par3, int par4) {
		super(par2EnumArmorMaterial, par3, par4);
	}

	public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
	{
	if(stack.getItem() == Init.itemXeniteHelmet || stack.getItem() == Init.itemXenitePlate || stack.getItem() == Init.itemXeniteBoots)
	{
	return Init.modid + ":textures/models/armor/xenite_layer_1.png";
	}
	if(stack.getItem() == Init.itemXeniteLegs)
	{
	return Init.modid + ":textures/models/armor/xenite_layer_2.png";
	}
	else return null;
	}
	
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister par1IconRegister)
	{
	    this.itemIcon = par1IconRegister.registerIcon(Init.modid + ":" + (this.getUnlocalizedName().substring(5)));

	}
	
}
