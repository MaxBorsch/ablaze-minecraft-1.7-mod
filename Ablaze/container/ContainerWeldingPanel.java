package Ablaze.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.ItemStack;
import Ablaze.tileentity.TileEntityWeldingPanel;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerWeldingPanel extends Container {

	private TileEntityWeldingPanel WeldingPanel;
	public int lastBurnTime;
	public int lastItemBurnTime;
	
	public ContainerWeldingPanel(InventoryPlayer inventory, TileEntityWeldingPanel tileentity){
		this.WeldingPanel = tileentity;
		
		this.addSlotToContainer(new Slot(tileentity, 0, 8, 5));
		this.addSlotToContainer(new Slot(tileentity, 1, 26, 5));
		this.addSlotToContainer(new Slot(tileentity, 2, 44, 5));
		this.addSlotToContainer(new Slot(tileentity, 3, 62, 5));
		
		this.addSlotToContainer(new Slot(tileentity, 4, 8, 23));
		this.addSlotToContainer(new Slot(tileentity, 5, 26, 23));
		this.addSlotToContainer(new Slot(tileentity, 6, 44, 23));
		this.addSlotToContainer(new Slot(tileentity, 7, 62, 23));
		
		this.addSlotToContainer(new Slot(tileentity, 8, 8, 41));
		this.addSlotToContainer(new Slot(tileentity, 9, 26, 41));
		this.addSlotToContainer(new Slot(tileentity, 10, 44, 41));
		this.addSlotToContainer(new Slot(tileentity, 11, 62, 41));
		
		this.addSlotToContainer(new Slot(tileentity, 12, 8, 59));
		this.addSlotToContainer(new Slot(tileentity, 13, 26, 59));
		this.addSlotToContainer(new Slot(tileentity, 14, 44, 59));
		this.addSlotToContainer(new Slot(tileentity, 15, 62, 59));
		
		this.addSlotToContainer(new SlotFurnace(inventory.player, tileentity, 16, 143, 33));

		for (int i=0; i<3; i++){
			for (int j=0; j<9; j++){
				this.addSlotToContainer(new Slot(inventory, j + i*9 + 9, 8 + j*18, 84 + i*18));
			}
		}
		
		for (int i=0; i<9; i++){
			this.addSlotToContainer(new Slot(inventory, i, 8 + i*18, 142));
		}
		
	}
	
	@Override
	public void addCraftingToCrafters(ICrafting icrafting){
		super.addCraftingToCrafters(icrafting);
		icrafting.sendProgressBarUpdate(this, 1, this.WeldingPanel.currentItemBurnTime);
	}
	
	@Override
	public void detectAndSendChanges(){
		super.detectAndSendChanges();
		for (int i=0; i<this.crafters.size(); i++){
			ICrafting icrafting = (ICrafting) this.crafters.get(i);

			
			if (this.lastItemBurnTime != this.WeldingPanel.currentItemBurnTime){
				icrafting.sendProgressBarUpdate(this, 0, this.WeldingPanel.currentItemBurnTime);
			}
			
			this.lastItemBurnTime = this.WeldingPanel.currentItemBurnTime;
			
		}
	}
	
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int slot, int newValue){
		if(slot==0) this.WeldingPanel.currentItemBurnTime = newValue;
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int islot){
		return null;
    }
	
	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return this.WeldingPanel.isUseableByPlayer(entityplayer);
	}

}
