package Ablaze.item;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import Ablaze.Init;
import Ablaze.entity.EntityBareMechanicalGolem;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemBareMechanicalGolem extends Item {

	public ItemBareMechanicalGolem() {
		this.setMaxStackSize(1);
	}

    public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10)
    {
    	if (!par3World.isRemote){
    	if (par2EntityPlayer.isSneaking()){

    	}else{
        		par1ItemStack.stackSize--;
        		NBTTagCompound ntag = par1ItemStack.getTagCompound();
        		EntityBareMechanicalGolem golem = new EntityBareMechanicalGolem(par3World);
        		if (par1ItemStack.hasTagCompound()){
        		golem.setItems(ntag.getInteger("Item0"), ntag.getInteger("Item1"), ntag.getInteger("Item2"), ntag.getInteger("Item3"), ntag.getInteger("Item4"), ntag.getInteger("Item5"));
        		}
        		golem.setPosition(par4+.5, par5+1, par6+.5);
        		par3World.spawnEntityInWorld(golem);
    	}
    	}
    	return true;
    }

    /**
     * If this function returns true (or the item is damageable), the ItemStack's NBT tag will be sent to the client.
     */
    public boolean getShareTag()
    {
        return true;
    }
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister par1IconRegister)
	{
	    this.itemIcon = par1IconRegister.registerIcon(Init.modid + ":" + (this.getUnlocalizedName().substring(5)));

	}
	
}
