package Ablaze.renderer;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import Ablaze.Init;
import Ablaze.model.ModelAutomatedSpawner;
import Ablaze.tileentity.TileEntityAutomatedSpawner;
import cpw.mods.fml.client.FMLClientHandler;

public class RendererAutomatedSpawner extends TileEntitySpecialRenderer {

	private ModelAutomatedSpawner model;
	
	public static final ResourceLocation TEXTURE = new ResourceLocation(Init.modid, "textures/models/entity/Effigy.png");
	public static final ResourceLocation TEXTURE_ON = new ResourceLocation(Init.modid, "textures/models/block/AutomatedSpawner_active.png");
	public static final ResourceLocation TEXTURE_OFF = new ResourceLocation(Init.modid, "textures/models/block/AutomatedSpawner.png");
	
	public RendererAutomatedSpawner(){
		this.model = new ModelAutomatedSpawner();
	}
	
	@Override
	public void renderTileEntityAt(TileEntity tileentity, double x, double y,double z, float f) {
	
		TileEntityAutomatedSpawner te = (TileEntityAutomatedSpawner) tileentity;
		
		FMLClientHandler.instance().getClient().renderEngine.bindTexture(te.getState() == 1 ? TEXTURE_ON : TEXTURE_OFF);
		
		GL11.glPushMatrix();
		GL11.glTranslatef((float) x + 0.5F, (float)y + 1.5F, (float)z + 0.5F);
		GL11.glRotatef(180, 0F, 0F, 1F);
		model.renderModel(0.0625F);
		GL11.glPopMatrix();
		
		if (te.entity != null && te.effigy != null) {
			FMLClientHandler.instance().getClient().renderEngine.bindTexture(TEXTURE);
			te.effigy.renderModel(te.entity, x, y, z, te.animation);
		}
	}

}
