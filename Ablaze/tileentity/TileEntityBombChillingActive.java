package Ablaze.tileentity;

import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class TileEntityBombChillingActive extends TileEntity{
	
	private int stime;
	private int time;
	
	public TileEntityBombChillingActive(){
		this.stime = 0;
		this.time = 0;
	}
	
	public void updateEntity(){
		this.time++;
		if (this.time>=this.stime+100 && !this.worldObj.isRemote){
			Explode();
		}
	}
	
	public void Explode(){
		for (int z=0; z<15; z++){
			for (int x=0; x<15; x++){
				this.worldObj.setBlock(this.xCoord+x, probeTerrain(this.worldObj, this.xCoord+x, this.zCoord+z), this.zCoord+z, Blocks.ice);
			}
		}
		for (int z=0; z<15; z++){
			for (int x=0; x<15; x++){
				this.worldObj.setBlock(this.xCoord-x, probeTerrain(this.worldObj, this.xCoord-x, this.zCoord-z), this.zCoord-z, Blocks.ice);

			}
		}
		for (int z=0; z<15; z++){
			for (int x=0; x<15; x++){
				this.worldObj.setBlock(this.xCoord+x, probeTerrain(this.worldObj, this.xCoord+x, this.zCoord-z), this.zCoord-z, Blocks.ice);

			}
		}
		for (int z=0; z<15; z++){
			for (int x=0; x<15; x++){
				this.worldObj.setBlock(this.xCoord-x, probeTerrain(this.worldObj, this.xCoord-x, this.zCoord+z), this.zCoord+z, Blocks.ice);

			}
		}
		this.worldObj.newExplosion(null, this.xCoord, this.yCoord, this.zCoord, 15, false, false);
		this.worldObj.setBlock(this.xCoord, this.yCoord, this.zCoord, Blocks.air);
		this.worldObj.setTileEntity(this.xCoord, this.yCoord, this.zCoord, null);
	}
	
	  public int probeTerrain(World world, int x, int z){
   	   int y = 0;
   	   boolean done = false;
   	   for (int i=256; i>1; i--){
   		   if (!done){
   		   if (world.getBlock(x, i, z)!=Blocks.air){
   			   y = i;
   			   done = true;
   		   }
   		   }
   	   }
   	   return y+1;
      }
	
}
