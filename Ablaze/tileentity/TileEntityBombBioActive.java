package Ablaze.tileentity;

import java.util.Random;

import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class TileEntityBombBioActive extends TileEntity{
	
	private int stime;
	private int time;
	
	public TileEntityBombBioActive(){
		this.stime = 0;
		this.time = 0;
	}
	
	public void updateEntity(){
		this.time++;
		if (this.time>=this.stime+100 && !this.worldObj.isRemote){
			Explode();
		}
	}
	
	public void plant(int x, int y, int z){
		Random r = new Random();

		int r1 = r.nextInt(6);
		if (r1>=3){
		this.worldObj.setBlock(x, y, z, Blocks.tallgrass, 1, 3);
	}else if (r1==1){
		this.worldObj.setBlock(x, y, z, Blocks.yellow_flower);
	}else if (r1==2){
		this.worldObj.setBlock(x, y, z, Blocks.red_flower);
	}
	}
	
	public void Explode(){
		for (int z=0; z<15; z++){
			for (int x=0; x<15; x++){
				plant(this.xCoord+x, probeTerrain(this.worldObj, this.xCoord+x, this.zCoord+z), this.zCoord+z);
			}
		}
		for (int z=0; z<15; z++){
			for (int x=0; x<15; x++){
				plant(this.xCoord-x, probeTerrain(this.worldObj, this.xCoord-x, this.zCoord+z), this.zCoord+z);
			}
		}
		for (int z=0; z<15; z++){
			for (int x=0; x<15; x++){
				plant(this.xCoord+x, probeTerrain(this.worldObj, this.xCoord+x, this.zCoord-z), this.zCoord-z);
			}
		}
		for (int z=0; z<15; z++){
			for (int x=0; x<15; x++){
				plant(this.xCoord-x, probeTerrain(this.worldObj, this.xCoord-x, this.zCoord-z), this.zCoord-z);
			}
		}
		this.worldObj.newExplosion(null, this.xCoord, this.yCoord, this.zCoord, 15, false, false);
		this.worldObj.setBlock(this.xCoord, this.yCoord, this.zCoord, Blocks.air);
		this.worldObj.setTileEntity(this.xCoord, this.yCoord, this.zCoord, null);
	}
	
	  public int probeTerrain(World world, int x, int z){
   	   int y = 0;
   	   boolean done = false;
   	   for (int i=256; i>1; i--){
   		   if (!done){
   		   if (world.getBlock(x, i, z)!=Blocks.air && world.getBlock(x, i, z)!=Blocks.snow_layer){
   			   y = i;
   			   done = true;
   		   }
   		   }
   	   }
   	   return y+1;
      }
	
}
