package Ablaze.block;

import net.minecraft.block.material.Material;
import Ablaze.Init;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockEnergeticPlate extends BlockBasicBlock
{
    public BlockEnergeticPlate()
    {
        this.setBlockBounds(0.05F, 0.0F, 0.05F, 0.95F, 0.1F, 0.95F);
    }

	public boolean isOpaqueCube(){
		return false;
	}
	
	public boolean renderAsNormalBlock(){
		return false;
	}
}
