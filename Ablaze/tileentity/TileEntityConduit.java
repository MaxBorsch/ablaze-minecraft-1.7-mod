package Ablaze.tileentity;

import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraftforge.common.util.ForgeDirection;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TileEntityConduit extends TileEntityEnergyHandler {

	
	private TileEntity[] adjacentConnections;
	
	public void updateEntity(){
		if (!this.worldObj.isRemote){
			
			if (this.random.nextInt(10) == 0) {
				this.updateConnections();
			}
			
			if (this.energy > 0) {
				if (this.isRequesting()) {
					this.sendEnergy(this.getRequesting());
				}
			}
		}
	}
	
	public boolean allowInput(ForgeDirection side) {
		return side != this.getRequesting();
	}
	
    public TileEntity[] getAdjacentConnections()
    {

            this.adjacentConnections = new TileEntity[6];
            TileEntity a = this.worldObj.getTileEntity(this.xCoord+1, this.yCoord, this.zCoord);
            TileEntity a2 = this.worldObj.getTileEntity(this.xCoord-1, this.yCoord, this.zCoord);
            TileEntity a3 = this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord+1);
            TileEntity a4 = this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord-1);
            TileEntity a5 = this.worldObj.getTileEntity(this.xCoord, this.yCoord+1, this.zCoord);
            TileEntity a6 = this.worldObj.getTileEntity(this.xCoord, this.yCoord-1, this.zCoord);
            if (a!=null && shouldConnect(a)){
            	this.adjacentConnections[5] = a;
            }
            if (a2!=null && shouldConnect(a2)){
            	this.adjacentConnections[4] = a2;
            }
            if (a3!=null && shouldConnect(a3)){
            	this.adjacentConnections[3] = a3;
            }
            if (a4!=null && shouldConnect(a4)){
            	this.adjacentConnections[2] = a4;
            }
            if (a5!=null && shouldConnect(a5)){
            	this.adjacentConnections[1] = a5;
            }
            if (a6!=null && shouldConnect(a6)){
            	this.adjacentConnections[0] = a6;
            }

            return this.adjacentConnections;

}

	@Override
    @SideOnly(Side.CLIENT)
    public AxisAlignedBB getRenderBoundingBox()
    {
            return AxisAlignedBB.getAABBPool().getAABB(this.xCoord, this.yCoord, this.zCoord, this.xCoord + 1, this.yCoord + 1, this.zCoord + 1);
    }

}