package Ablaze.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import Ablaze.entity.EntityMechanicalGolem;

public class ModelMechanicalGolem extends ModelBase
{
	  //fields
    ModelRenderer head;
    ModelRenderer body;
    public static ModelRenderer rightarm;
    ModelRenderer leftarm;
    ModelRenderer rightleg;
    ModelRenderer leftleg;
  
  public ModelMechanicalGolem()
  {
	  textureWidth = 64;
	    textureHeight = 32;
	    
	      head = new ModelRenderer(this, 0, 0);
	      head.addBox(-2F, -4F, -2F, 4, 4, 4);
	      head.setRotationPoint(0F, 12F, 1F);
	      head.setTextureSize(64, 32);
	      head.mirror = true;
	      setRotation(head, 0F, 0F, 0F);
	      body = new ModelRenderer(this, 16, 0);
	      body.addBox(-2F, 0F, -1F, 4, 6, 2);
	      body.setRotationPoint(0F, 12F, 1F);
	      body.setTextureSize(64, 32);
	      body.mirror = true;
	      setRotation(body, 0F, 0F, 0F);
	      rightarm = new ModelRenderer(this, 28, 0);
	      rightarm.addBox(-2F, -1F, -2F, 2, 6, 2);
	      rightarm.setRotationPoint(-2F, 13F, 2F);
	      rightarm.setTextureSize(64, 32);
	      rightarm.mirror = true;
	      setRotation(rightarm, 0F, 0F, 0F);
	      leftarm = new ModelRenderer(this, 28, 0);
	      leftarm.addBox(-1F, -1F, -2F, 2, 6, 2);
	      leftarm.setRotationPoint(3F, 13F, 2F);
	      leftarm.setTextureSize(64, 32);
	      leftarm.mirror = true;
	      setRotation(leftarm, 0F, 0F, 0F);
	      rightleg = new ModelRenderer(this, 36, 0);
	      rightleg.addBox(0F, 0F, 0F, 2, 6, 2);
	      rightleg.setRotationPoint(-2F, 18F, 0F);
	      rightleg.setTextureSize(64, 32);
	      rightleg.mirror = true;
	      setRotation(rightleg, 0F, 0F, 0F);
	      leftleg = new ModelRenderer(this, 36, 0);
	      leftleg.addBox(-2F, 0F, 0F, 2, 6, 2);
	      leftleg.setRotationPoint(2F, 18F, 0F);
	      leftleg.setTextureSize(64, 32);
	      leftleg.mirror = true;
	      setRotation(leftleg, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    head.render(f5);
    body.render(f5);
    rightarm.render(f5);
    leftarm.render(f5);
    rightleg.render(f5);
    leftleg.render(f5);
  }
  
  public void renderModel(float f5){
	  head.render(f5);
	    body.render(f5);
	    rightarm.render(f5);
	    leftarm.render(f5);
	    rightleg.render(f5);
	    leftleg.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    this.head.rotateAngleY = f3 / (180F / (float)Math.PI);
    this.head.rotateAngleX = f4 / (180F / (float)Math.PI);
    this.leftleg.rotateAngleX = -1.5F * this.func_78172_a(f, 13.0F) * f1;
    this.rightleg.rotateAngleX = 1.5F * this.func_78172_a(f, 13.0F) * f1;
    this.leftleg.rotateAngleY = 0.0F;
    this.rightleg.rotateAngleY = 0.0F;
  }

  public void setLivingAnimations(EntityLivingBase par1EntityLivingBase, float par2, float par3, float par4)
  {
              this.rightarm.rotateAngleX = (-0.2F + 1.5F * this.func_78172_a(par2, 13.0F)) * par3;
              this.leftarm.rotateAngleX = (-0.2F - 1.5F * this.func_78172_a(par2, 13.0F)) * par3;
  }

  private float func_78172_a(float par1, float par2)
  {
      return (Math.abs(par1 % par2 - par2 * 0.5F) - par2 * 0.25F) / (par2 * 0.25F);
  }
  
}
