package Ablaze.item;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import Ablaze.Init;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemIncompleteMechanicalGolem extends Item {

	public ItemIncompleteMechanicalGolem() {
		this.setMaxStackSize(1);
	}

    /**
     * If this function returns true (or the item is damageable), the ItemStack's NBT tag will be sent to the client.
     */
    public boolean getShareTag()
    {
        return true;
    }
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister par1IconRegister)
	{
	    this.itemIcon = par1IconRegister.registerIcon(Init.modid + ":" + (this.getUnlocalizedName().substring(5)));

	}
	
}
