package Ablaze.block;

import net.minecraft.client.renderer.texture.IIconRegister;
import Ablaze.Init;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockHardenedStoneCompound extends BlockBasicBlock
{
    public BlockHardenedStoneCompound()
    {
        this.setHardness(0F);
        this.setResistance(0F);
    }

	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
	    this.blockIcon = par1IconRegister.registerIcon(Init.modid + ":" + (this.getUnlocalizedName().substring(5)));

	}
}
