package Ablaze.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import Ablaze.Init;
import Ablaze.tileentity.TileEntityBombBlazing;
import Ablaze.tileentity.TileEntityBombBlazingActive;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockBombBlazing extends BlockContainer
{
    private Random random = new Random();

    public BlockBombBlazing()
    {
        super(Material.rock);
    }

    public void onBlockAdded(World par1World, int par2, int par3, int par4)
    {
    	par1World.scheduleBlockUpdate(par2, par3, par4, this, 4);
    }
    
    @Override
    public void onNeighborBlockChange(World world, int par2, int par3, int par4, Block block) {
    {
        if (!world.isRemote)
            if (world.isBlockIndirectlyGettingPowered(par2, par3, par4))
            {
            	world.setBlock(par2, par3, par4, Init.blockBombBlazingActive);

            }
    	}
    }
    
    public void updateTick(World par1World, int par2, int par3, int par4, Random par5Random)
    {
        if (!par1World.isRemote)
            if (par1World.isBlockIndirectlyGettingPowered(par2, par3, par4))
            {
                par1World.setBlock(par2, par3, par4, Init.blockBombBlazingActive);
            }
    }
    
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ){
		if (!world.isRemote){
		EntityItem item = new EntityItem(world, (double)((float)x), (double)((float)y+2), (double)((float)z), new ItemStack(Init.itemBlazingSingularity));
	
	float f4 = 0.2F;
	item.motionY = (double)((float)this.random.nextGaussian() * f4);
	
	world.spawnEntityInWorld(item);
	world.setBlock(x, y, z, Init.blockBombFrame);
	}
	return true;
	}
    
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
	    this.blockIcon = par1IconRegister.registerIcon(Init.modid + ":" + "stone");

	}

	@Override
	public TileEntity createNewTileEntity(World world, int par2) {
		return new TileEntityBombBlazing();
	}
	
	public boolean isOpaqueCube(){
		return false;
	}
	
	public boolean renderAsNormalBlock(){
		return false;
	}
	
    public int getRenderType()
    {
        return -1;
    }
    
}
