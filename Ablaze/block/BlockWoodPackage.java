package Ablaze.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import Ablaze.Init;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockWoodPackage extends Block
{
	
    public BlockWoodPackage()
    {
        super(Material.wood);
        this.setHardness(10F);
        this.setResistance(10F);
        this.setStepSound(soundTypeWood);
        this.setBlockBounds(0.1F, 0.0F, 0.1F, 0.9F, 0.7F, 0.9F);
    }

	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
	    this.blockIcon = par1IconRegister.registerIcon(Init.modid + ":" + (this.getUnlocalizedName().substring(5)));

	}
	
	@Override
	public boolean isOpaqueCube(){
		return false;
	}
	
	@Override
	public boolean renderAsNormalBlock(){
		return false;
	}
	
    /**
     * Returns the quantity of items to drop on block destruction.
     */
    public int quantityDropped(Random par1Random)
    {
        return 4;
    }
    
    @Override
	public Item getItemDropped(int par1, Random r, int par2){
    	Item[] itemDrops = new Item[]{Init.itemAsh, Items.apple, Items.arrow, Items.book, Items.bucket, Items.bone};
    	int r1 = r.nextInt(6);
    	
		return itemDrops[r1];
	}
	
}
