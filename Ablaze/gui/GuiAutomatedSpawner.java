package Ablaze.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import Ablaze.Init;
import Ablaze.container.ContainerAutomatedSpawner;
import Ablaze.tileentity.TileEntityAutomatedSpawner;

public class GuiAutomatedSpawner extends GuiContainer {

	public TileEntityAutomatedSpawner AutomatedSpawner;
	public static final ResourceLocation Bkg = new ResourceLocation(Init.modid, "textures/gui/automatedspawner.png");
	
	public GuiAutomatedSpawner(InventoryPlayer inventory, TileEntityAutomatedSpawner entity) {
		super(new ContainerAutomatedSpawner(inventory, entity));
		
		this.AutomatedSpawner = entity;
		this.xSize = 176;
		this.ySize = 166;
	}

	public void drawGuiContainerForegroundLayer(int par1, int par2){
		String name = this.AutomatedSpawner.getInventoryName();
		
		this.fontRendererObj.drawString(name, this.xSize/2 - this.fontRendererObj.getStringWidth(name)/2, 6, 4210752);
	}
	
	public void drawGuiContainerBackgroundLayer(float f, int i, int j) {
		GL11.glColor4f(1F, 1F, 1F, 1F);
		
		Minecraft.getMinecraft().getTextureManager().bindTexture(Bkg);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
	
		int k2 = AutomatedSpawner.getEnergyScaled(42);
		drawTexturedModalRect(guiLeft + 119, guiTop + 24, 176, 0, 5, 42 - k2);
		
		int k3 = AutomatedSpawner.getWorkScaled(42);
		drawTexturedModalRect(guiLeft + 50, guiTop + 24, 176, 0, 5, 42 - k3);
	
	}

}
