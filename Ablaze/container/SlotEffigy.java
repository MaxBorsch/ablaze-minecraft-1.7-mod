package Ablaze.container;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import Ablaze.item.ItemMobEffigy;

public class SlotEffigy extends Slot {
   public SlotEffigy(IInventory inventory, int par2, int par3, int par4) {
      super(inventory, par2, par3, par4);
   }

   @Override
   public boolean isItemValid(ItemStack itemstack) {
      return itemstack.getItem() instanceof ItemMobEffigy;
   }

   @Override
   public int getSlotStackLimit() {
      return 1;
   }
}