package Ablaze.container;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import Ablaze.tileentity.TileEntityExtractor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.ItemStack;

public class ContainerExtractor extends Container {

	private TileEntityExtractor Extractor;
	public int lastBurnTime;
	public int lastItemBurnTime;
	
	public ContainerExtractor(InventoryPlayer inventory, TileEntityExtractor tileentity){
		this.Extractor = tileentity;
		
		this.addSlotToContainer(new Slot(tileentity, 0, 56, 17));
		this.addSlotToContainer(new Slot(tileentity, 1, 56, 53));
		this.addSlotToContainer(new Slot(tileentity, 3, 30, 17));
		this.addSlotToContainer(new SlotFurnace(inventory.player, tileentity, 2, 116, 35));
		

		for (int i=0; i<3; i++){
			for (int j=0; j<9; j++){
				this.addSlotToContainer(new Slot(inventory, j + i*9 + 9, 8 + j*18, 84 + i*18));
			}
		}
		
		for (int i=0; i<9; i++){
			this.addSlotToContainer(new Slot(inventory, i, 8 + i*18, 142));
		}
		
	}
	
	@Override
	public void addCraftingToCrafters(ICrafting icrafting){
		super.addCraftingToCrafters(icrafting);
		icrafting.sendProgressBarUpdate(this, 0, this.Extractor.burnTime);
		icrafting.sendProgressBarUpdate(this, 1, this.Extractor.currentItemBurnTime);
	}
	
	@Override
	public void detectAndSendChanges(){
		super.detectAndSendChanges();
		for (int i=0; i<this.crafters.size(); i++){
			ICrafting icrafting = (ICrafting) this.crafters.get(i);
			
			if (this.lastBurnTime != this.Extractor.burnTime){
				icrafting.sendProgressBarUpdate(this, 0, this.Extractor.burnTime);
			}
			
			if (this.lastItemBurnTime != this.Extractor.currentItemBurnTime){
				icrafting.sendProgressBarUpdate(this, 1, this.Extractor.currentItemBurnTime);
			}
			
			this.lastBurnTime = this.Extractor.burnTime;
			this.lastItemBurnTime = this.Extractor.currentItemBurnTime;
			
		}
	}
	
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int slot, int newValue){
		if(slot==0) this.Extractor.burnTime = newValue;
		if(slot==1) this.Extractor.currentItemBurnTime = newValue;
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int islot){
		return null;
    }
	
	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return this.Extractor.isUseableByPlayer(entityplayer);
	}

}
