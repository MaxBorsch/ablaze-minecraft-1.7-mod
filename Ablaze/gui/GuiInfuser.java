package Ablaze.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import Ablaze.Init;
import Ablaze.container.ContainerInfuser;
import Ablaze.tileentity.TileEntityInfuser;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class GuiInfuser extends GuiContainer {

	public TileEntityInfuser Infuser;
	public static final ResourceLocation Bkg = new ResourceLocation(Init.modid, "textures/gui/infuser.png");
	
	public GuiInfuser(InventoryPlayer inventory, TileEntityInfuser entity) {
		super(new ContainerInfuser(inventory, entity));
		
		this.Infuser = entity;
		this.xSize = 176;
		this.ySize = 166;
	}

	public void drawGuiContainerForegroundLayer(int par1, int par2){
		String name = this.Infuser.getInventoryName();
		
		this.fontRendererObj.drawString(name, this.xSize/2 - this.fontRendererObj.getStringWidth(name)/2, 6, 4210752);
	}
	
	public void drawGuiContainerBackgroundLayer(float f, int i, int j) {
		GL11.glColor4f(1F, 1F, 1F, 1F);
		
		Minecraft.getMinecraft().getTextureManager().bindTexture(Bkg);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
	
	if (Infuser.burnTime>0){
		int k = this.Infuser.getBurnTimeRemainingScaled(12);
		drawTexturedModalRect(guiLeft+56, guiTop+38-k, 176, k, 14, 12);
		//------------------------------------------------------------------------
		int k2 = Infuser.getCookProgressScaled(24);
		drawTexturedModalRect(guiLeft+79, guiTop+34, 176, 14, k2, 16);
	}
	
	}

}
