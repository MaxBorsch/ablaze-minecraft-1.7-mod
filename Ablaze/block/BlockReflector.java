package Ablaze.block;

import java.util.Random;

import javax.swing.Icon;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import Ablaze.Init;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockReflector extends Block {
	
	@SideOnly(Side.CLIENT)
	private IIcon iconFront;
	@SideOnly(Side.CLIENT)
	private IIcon iconSide;
	
	private Random random = new Random();
	
	public BlockReflector() {
		super(Material.rock);
	}


	public void onBlockAdded(World world, int x, int y, int z){
	super.onBlockAdded(world, x, y, z);
	this.setDefaultDirection(world, x, y, z);
	}
	private void setDefaultDirection(World world, int x, int y, int z){
		if (!world.isRemote){
		byte b0 = 3;
		Block l = world.getBlock(x, y, z - 1);
		Block il = world.getBlock(x, y, z + 1);
		Block jl = world.getBlock(x - 1, y, z);
		Block kl = world.getBlock(x + 1, y, z);
		
		if (l.isOpaqueCube() && !il.isOpaqueCube()){
			b0 = 3;
		}
		if (il.isOpaqueCube() && !l.isOpaqueCube()){
			b0 = 2;
		}
		if (kl.isOpaqueCube() && !jl.isOpaqueCube()){
			b0 = 5;
		}
		if (jl.isOpaqueCube() && !kl.isOpaqueCube()){
			b0 = 4;
		}
		world.setBlockMetadataWithNotify(x, y, z, b0, 2);
		}
	}
	
	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase elb, ItemStack item){
		int l = MathHelper.floor_double((double) (elb.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
		if (l == 0){
			world.setBlockMetadataWithNotify(x, y, z, 2, 2);
		}
		if (l == 1){
			world.setBlockMetadataWithNotify(x, y, z, 5, 2);

		}
		if (l == 2){
			world.setBlockMetadataWithNotify(x, y, z, 3, 2);

		}
		if (l == 3){
			world.setBlockMetadataWithNotify(x, y, z, 4, 2);

		}
	}
	
	
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
	    this.blockIcon = par1IconRegister.registerIcon(Init.modid + ":" + "scorchedbrickBlock");
	    this.iconFront = par1IconRegister.registerIcon(Init.modid + ":" + "reflectorBlock");

	}
	
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int metadata){
		return metadata == 0 && side == 3 ? this.iconFront : (side==metadata ? this.iconFront : side==2 ? this.blockIcon : (this.blockIcon));
	}


	
}
