package Ablaze.block;

import java.util.Random;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import Ablaze.Init;
import Ablaze.tileentity.TileEntityBombChillingActive;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockBombChillingActive extends BlockContainer
{
    public BlockBombChillingActive()
    {
        super(Material.rock);
    }

    @Override
    public Item getItemDropped(int par1, Random rand, int par2){
		return Item.getItemFromBlock(Init.blockBombChilling);
	}
    
    @SideOnly(Side.CLIENT)
	public void randomDisplayTick(World world, int x, int y, int z, Random r){
		
		boolean r1=r.nextBoolean();
		boolean r2=r.nextBoolean();
		float x1 = (float) x + 0.5F;
		float y1 = (float) y + r.nextFloat();
		float z1 = (float) z + 0.5F;
		
		float f = 0.52F;
		float f1 = r.nextFloat() * 0.6F - 0.3F;
		world.spawnParticle("smoke", (double)(r1==true?x1+f1:x1-f1), (double)y1+f/2, (double)(r2==true?z1+f1:z1-f1), 0D, 0D, 0D);
		world.spawnParticle("largesmoke", (double)(r1==true?x1+f1:x1-f1), (double)y1+f/2, (double)(r2==true?z1+f1:z1-f1), 0D, 0D, 0D);

	}
    
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
	    this.blockIcon = par1IconRegister.registerIcon(Init.modid + ":" + "stone");

	}

	@Override
	public TileEntity createNewTileEntity(World world, int par2) {
		return new TileEntityBombChillingActive();
	}
	
	public boolean isOpaqueCube(){
		return false;
	}
	
	public boolean renderAsNormalBlock(){
		return false;
	}
	
    public int getRenderType()
    {
        return -1;
    }
    
}
