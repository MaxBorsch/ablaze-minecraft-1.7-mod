package Ablaze.tileentity;

import java.util.Random;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.ForgeDirection;

public class TileEntityEnergyHandler extends TileEntityMachine {
	
	public int energy = 0;
	public int maxEnergy = 1;
	public boolean alwaysRequest;
	private ForgeDirection requesting = null;
	public Random random = new Random();
	
	public TileEntityEnergyHandler(){
		this.maxEnergy = 1;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt){
		super.readFromNBT(nbt);
		this.energy = nbt.getShort("energy");	
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt){
		super.writeToNBT(nbt);
		nbt.setShort("energy", (short) this.energy);
	}
	
	public boolean isRequesting() {
		return (this.requesting != null) || (this.alwaysRequest && this.energy < this.maxEnergy);
	}
	
	public boolean isProviding() {
		return (this.requesting != null);
	}
	
	public void setRequesting(ForgeDirection side) {
		this.requesting = side;
	}
	
	public ForgeDirection getRequesting() {
		return this.requesting;
	}
	
	public boolean allowInput(ForgeDirection side) {
		return false;
	}
		
	public int receiveEnergy(int amount) {
		if (this.energy < this.maxEnergy) {
			this.energy++;
			return 1;
		} else {
			return 0;
		}
	}
	
	public void sendEnergy(ForgeDirection side) {
		if (side == null) { return; }
		
		TileEntity t = this.worldObj.getTileEntity(this.xCoord + side.offsetX, this.yCoord + side.offsetY, this.zCoord + side.offsetZ);
		if (t != null && t instanceof TileEntityEnergyHandler) {
			int e = ((TileEntityEnergyHandler) t).receiveEnergy(this.energy);
			this.energy -= e;
		}
	}
	
	public ForgeDirection shouldRequest() {
		for (ForgeDirection s : ForgeDirection.VALID_DIRECTIONS) {
			TileEntity tile = this.worldObj.getTileEntity(this.xCoord + s.offsetX, this.yCoord + s.offsetY, this.zCoord + s.offsetZ);
			if (tile != null && shouldConnect(tile)) {
				if (((TileEntityEnergyHandler) tile).isRequesting() && ((TileEntityEnergyHandler) tile).allowInput(s.getOpposite())) {
					return s;
				}
			}
		}
		
		return null;
	}
	
	public boolean shouldConnect(TileEntity a) {
    	if (a instanceof TileEntityEnergyHandler){
    		return true;
    	}
    	
		return false;
	}
	
	public void updateConnections() {
		this.setRequesting(this.shouldRequest());	
	}
	
}
