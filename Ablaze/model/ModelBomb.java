package Ablaze.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelBomb extends ModelBase
{
	
	public static boolean hasCharge;
	 //fields
    ModelRenderer Main;
    ModelRenderer P8;
    ModelRenderer P7;
    ModelRenderer P5;
    ModelRenderer P4;
    ModelRenderer P6;
    ModelRenderer P3;
    ModelRenderer P2;
    ModelRenderer P1;
    ModelRenderer Charge;

  public ModelBomb()
  {
	this.hasCharge = false;
    textureWidth = 64;
    textureHeight = 32;
    
      Main = new ModelRenderer(this, 0, 0);
      Main.addBox(-2F, -2F, -2F, 6, 6, 6);
      Main.setRotationPoint(-1F, 18F, -1F);
      Main.setTextureSize(64, 32);
      Main.mirror = true;
      setRotation(Main, 0F, 0F, 0F);
      P8 = new ModelRenderer(this, 0, 16);
      P8.addBox(-1F, 0F, -2F, 2, 2, 12);
      P8.setRotationPoint(-6F, 22F, -4F);
      P8.setTextureSize(64, 32);
      P8.mirror = true;
      setRotation(P8, 0F, 0F, 0F);
      P7 = new ModelRenderer(this, 0, 16);
      P7.addBox(0F, 0F, 0F, 2, 2, 12);
      P7.setRotationPoint(5F, 22F, -6F);
      P7.setTextureSize(64, 32);
      P7.mirror = true;
      setRotation(P7, 0F, 0F, 0F);
      P5 = new ModelRenderer(this, 0, 12);
      P5.addBox(0F, 0F, 0F, 14, 2, 2);
      P5.setRotationPoint(-7F, 22F, -7F);
      P5.setTextureSize(64, 32);
      P5.mirror = true;
      setRotation(P5, 0F, 0F, 0F);
      P4 = new ModelRenderer(this, 34, 0);
      P4.addBox(-2F, -4F, 0F, 4, 8, 1);
      P4.setRotationPoint(0F, 19.4F, -4F);
      P4.setTextureSize(64, 32);
      P4.mirror = true;
      setRotation(P4, -0.7853982F, 0F, 0F);
      P6 = new ModelRenderer(this, 0, 12);
      P6.addBox(-6F, 0F, 0F, 14, 2, 2);
      P6.setRotationPoint(-1F, 22F, 5F);
      P6.setTextureSize(64, 32);
      P6.mirror = true;
      setRotation(P6, 0F, 0F, 0F);
      P3 = new ModelRenderer(this, 34, 0);
      P3.addBox(-2F, -4F, 0F, 4, 8, 1);
      P3.setRotationPoint(0F, 19.8F, 3F);
      P3.setTextureSize(64, 32);
      P3.mirror = true;
      setRotation(P3, 0.7853982F, 0F, 0F);
      P2 = new ModelRenderer(this, 24, 0);
      P2.addBox(0F, -4F, -2F, 1, 8, 4);
      P2.setRotationPoint(-4F, 19.4F, 0F);
      P2.setTextureSize(64, 32);
      P2.mirror = true;
      setRotation(P2, 0F, 0F, 0.7853982F);
      P1 = new ModelRenderer(this, 24, 0);
      P1.addBox(0F, -4F, -2F, 1, 8, 4);
      P1.setRotationPoint(3F, 19.9F, 0F);
      P1.setTextureSize(64, 32);
      P1.mirror = true;
      setRotation(P1, 0F, 0F, -0.7853982F);
      Charge = new ModelRenderer(this, 28, 16);
      Charge.addBox(-2F, 0F, 0F, 4, 1, 4);
      Charge.setRotationPoint(0F, 15F, -2F);
      Charge.setTextureSize(64, 32);
      Charge.mirror = true;
      setRotation(Charge, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    Main.render(f5);
    P8.render(f5);
    P7.render(f5);
    P5.render(f5);
    P4.render(f5);
    P6.render(f5);
    P3.render(f5);
    P2.render(f5);
    P1.render(f5);
    if (this.hasCharge){
    Charge.render(f5);
    }
  }
  
  public void renderModel(float f5){
	    Main.render(f5);
	    P8.render(f5);
	    P7.render(f5);
	    P6.render(f5);
	    P5.render(f5);
	    P4.render(f5);
	    P3.render(f5);
	    P2.render(f5);
	    P1.render(f5);
	    if (this.hasCharge){
	    Charge.render(f5);
	    }
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
  }

}
