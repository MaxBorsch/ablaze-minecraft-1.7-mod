package Ablaze.proxy;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.Item;
import net.minecraftforge.client.MinecraftForgeClient;
import Ablaze.Init;
import Ablaze.entity.EntityBareMechanicalGolem;
import Ablaze.entity.EntityEvolvedHungry;
import Ablaze.entity.EntityHungry;
import Ablaze.entity.EntityMechanicalGolem;
import Ablaze.renderer.ItemRendererAutomatedSpawner;
import Ablaze.renderer.ItemRendererBacteriaFarm;
import Ablaze.renderer.ItemRendererBioGenerator;
import Ablaze.renderer.ItemRendererBioModule;
import Ablaze.renderer.ItemRendererBombBlazing;
import Ablaze.renderer.ItemRendererBombBlazingActive;
import Ablaze.renderer.ItemRendererBombChilling;
import Ablaze.renderer.ItemRendererBombChillingActive;
import Ablaze.renderer.ItemRendererBombFrame;
import Ablaze.renderer.ItemRendererGlowGenerator;
import Ablaze.renderer.ItemRendererIncendiaryModule;
import Ablaze.renderer.ItemRendererTinkerTable;
import Ablaze.renderer.ItemRendererWeldingFrame;
import Ablaze.renderer.ItemRendererWeldingPanel;
import Ablaze.renderer.ItemRendererWritingDesk;
import Ablaze.renderer.RenderBareMechanicalGolem;
import Ablaze.renderer.RenderEvolvedHungry;
import Ablaze.renderer.RenderHungry;
import Ablaze.renderer.RenderMechanicalGolem;
import Ablaze.renderer.RendererAutomatedSpawner;
import Ablaze.renderer.RendererBacteriaFarm;
import Ablaze.renderer.RendererBioGenerator;
import Ablaze.renderer.RendererBioModule;
import Ablaze.renderer.RendererBombBio;
import Ablaze.renderer.RendererBombBioActive;
import Ablaze.renderer.RendererBombBlazing;
import Ablaze.renderer.RendererBombBlazingActive;
import Ablaze.renderer.RendererBombChilling;
import Ablaze.renderer.RendererBombChillingActive;
import Ablaze.renderer.RendererBombFrame;
import Ablaze.renderer.RendererGlowGenerator;
import Ablaze.renderer.RendererIncendiaryModule;
import Ablaze.renderer.RendererTinkerTable;
import Ablaze.renderer.RendererWeldingFrame;
import Ablaze.renderer.RendererWeldingPanel;
import Ablaze.renderer.RendererWritingDesk;
import Ablaze.tileentity.TileEntityAutomatedSpawner;
import Ablaze.tileentity.TileEntityBacteriaFarm;
import Ablaze.tileentity.TileEntityBioGenerator;
import Ablaze.tileentity.TileEntityBioModule;
import Ablaze.tileentity.TileEntityBombBio;
import Ablaze.tileentity.TileEntityBombBioActive;
import Ablaze.tileentity.TileEntityBombBlazing;
import Ablaze.tileentity.TileEntityBombBlazingActive;
import Ablaze.tileentity.TileEntityBombChilling;
import Ablaze.tileentity.TileEntityBombChillingActive;
import Ablaze.tileentity.TileEntityBombFrame;
import Ablaze.tileentity.TileEntityGlowGenerator;
import Ablaze.tileentity.TileEntityIncendiaryModule;
import Ablaze.tileentity.TileEntityTinkerTable;
import Ablaze.tileentity.TileEntityWeldingFrame;
import Ablaze.tileentity.TileEntityWeldingPanel;
import Ablaze.tileentity.TileEntityWritingDesk;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy {

	@Override
	public void registerEVERYTHING() {
		
		TileEntitySpecialRenderer render = new RendererWritingDesk();
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntityWritingDesk.class, render);
    	MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(Init.blockWritingDesk), new ItemRendererWritingDesk(render));

    	TileEntitySpecialRenderer render2 = new RendererTinkerTable();
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntityTinkerTable.class, render2);
    	MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(Init.blockTinkerTable), new ItemRendererTinkerTable(render2));
    	
    	TileEntitySpecialRenderer render3 = new RendererWeldingFrame();
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntityWeldingFrame.class, render3);
    	MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(Init.blockWeldingFrame), new ItemRendererWeldingFrame(render3));
    	
    	TileEntitySpecialRenderer render4 = new RendererWeldingPanel();
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntityWeldingPanel.class, render4);
    	MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(Init.blockWeldingPanel), new ItemRendererWeldingPanel(render4));
    	
    	TileEntitySpecialRenderer render5 = new RendererBioModule();
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBioModule.class, render5);
    	MinecraftForgeClient.registerItemRenderer(Init.itemBioModule, new ItemRendererBioModule(render5));
    	
    	TileEntitySpecialRenderer render6 = new RendererIncendiaryModule();
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntityIncendiaryModule.class, render6);
    	MinecraftForgeClient.registerItemRenderer(Init.itemIncendiaryModule, new ItemRendererIncendiaryModule(render6));
    	
    	TileEntitySpecialRenderer render9 = new RendererGlowGenerator();
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntityGlowGenerator.class, render9);
    	MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(Init.blockGlowGenIdle), new ItemRendererGlowGenerator(render9));
    	
    	TileEntitySpecialRenderer render10 = new RendererBombFrame();
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBombFrame.class, render10);
    	MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(Init.blockBombFrame), new ItemRendererBombFrame(render10));
    	
    	TileEntitySpecialRenderer render11 = new RendererBombBioActive();
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBombBioActive.class, render11);
    	MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(Init.blockBombBioActive), new ItemRendererBombChillingActive(render11));
    	
    	TileEntitySpecialRenderer render12 = new RendererBombBio();
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBombBio.class, render12);
    	MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(Init.blockBombBio), new ItemRendererBombChilling(render12));
    	
    	TileEntitySpecialRenderer render13 = new RendererBombBlazingActive();
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBombBlazingActive.class, render13);
    	MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(Init.blockBombBlazingActive), new ItemRendererBombBlazingActive(render13));
    	
    	TileEntitySpecialRenderer render14 = new RendererBombBlazing();
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBombBlazing.class, render14);
    	MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(Init.blockBombBlazing), new ItemRendererBombBlazing(render14));
    	
    	TileEntitySpecialRenderer render15 = new RendererBombChillingActive();
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBombChillingActive.class, render15);
    	MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(Init.blockBombChillingActive), new ItemRendererBombChillingActive(render15));
    	
    	TileEntitySpecialRenderer render16 = new RendererBombChilling();
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBombChilling.class, render16);
    	MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(Init.blockBombChilling), new ItemRendererBombChilling(render16));
    	
    	TileEntitySpecialRenderer render17 = new RendererBioGenerator();
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBioGenerator.class, render17);
    	MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(Init.blockBioGenerator), new ItemRendererBioGenerator(render17));
    	
    	TileEntitySpecialRenderer render18 = new RendererAutomatedSpawner();
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntityAutomatedSpawner.class, render18);
    	MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(Init.blockAutomatedSpawner), new ItemRendererAutomatedSpawner(render18));

    	TileEntitySpecialRenderer render19 = new RendererBacteriaFarm();
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBacteriaFarm.class, render19);
    	MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(Init.blockBacteriaFarm), new ItemRendererBacteriaFarm(render19));

    	
    	RenderingRegistry.registerEntityRenderingHandler(EntityMechanicalGolem.class, new RenderMechanicalGolem());
    	RenderingRegistry.registerEntityRenderingHandler(EntityBareMechanicalGolem.class, new RenderBareMechanicalGolem());
    	RenderingRegistry.registerEntityRenderingHandler(EntityHungry.class, new RenderHungry());
    	RenderingRegistry.registerEntityRenderingHandler(EntityEvolvedHungry.class, new RenderEvolvedHungry());

	}
	
}
