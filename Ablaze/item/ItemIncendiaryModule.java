package Ablaze.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import Ablaze.Init;

public class ItemIncendiaryModule extends Item {

    public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10)
    {
    	if (par2EntityPlayer.isSneaking()){

    	}else{
        	if (par3World.getBlock(par4, par5, par6)==Init.blockEnergeticPlate){
        		par3World.setBlock(par4, par5, par6, Init.blockGlowGenIdle);
        		par1ItemStack.stackSize--;
        	}
    	}
    	return true;
    }
    
    @SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IIconRegister par1IconRegister)
	{
	    this.itemIcon = par1IconRegister.registerIcon(Init.modid + ":penItem");

	}
}
